import React from "react";
import {Alert, Image, StyleSheet, Platform, View} from "react-native";
import {
    createAppContainer,
    createBottomTabNavigator,
    createStackNavigator,
    createSwitchNavigator
} from "react-navigation";
import TabBar from "./app/Components/TabBar"

import {Home} from './app/screens/Home';
import {Loginn} from './app/screens/Login';
import {ForgotPassword} from './app/screens/ForgotPassword';
import {CustomHeader} from './app/Components/CustomHeader';
import {TripDetails} from "./app/screens/TripDetails";
import {Flight} from "./app/screens/Flight";
import Colors from "./app/Styles/Colors";
import {Settings} from "./app/screens/Settings";
import {Guide} from "./app/screens/Guide";
import {Hotel} from "./app/screens/Hotel";
import TextFontStyles from "./app/Styles/TextFontStyles";
import {
    errorAlert,
    getData,
    getSavedObject,
    getUserObject,
    isSignedIn,
    onSignOutUser,
    saveMainObject
} from "./app/Classes/auth";
import AuthLoadingScreen from "./app/screens/AuthLoadingScreen";
import * as Font from 'expo-font'
import {AlertTrip} from "./app/screens/AlertTrip";
import {Message} from "./app/screens/Message";
import {CheckList} from "./app/screens/CheckList";
import {ImportantInformation} from "./app/screens/ImportantInformation";
import {AppLoading, Notifications} from 'expo';
import {GuideData} from "./app/screens/GuideData";
import {get_current_trip_url, notification_key, trip_key} from "./app/Classes/UrlConstant";
import {Asset} from 'expo-asset';
import {AdminHome} from "./app/screens/AdminHome";
import {Provider, Consumer, MyContext} from "./app/Components/Consumer";
import Compass from "./app/screens/Compass";
import {ImageScreen} from "./app/screens/ImageScreen";


export default class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            fontLoaded: false,
            isReady: false,
            signedIn: false,
            value_message: '',
            value_alert: '',
            value_trip_id: "0",
            value_current_trip_id: "0",
        };
    }

    static contextType = MyContext;

    componentDidMount() {
        this._notificationSubscription = Notifications.addListener(this._handleNotification);
        getSavedObject(notification_key)
            .then((res) => {
                if (!res)
                    return;
                else {
                    this._handleNotificationSaved(JSON.parse(res))
                }
            })
            .catch(() => {

            })
    }

    async _loadAssetsAsync() {
        const ss = await Font.loadAsync({
            'open-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf'),
            'Roboto': require('./assets/fonts/Roboto-Regular.ttf'),
            'roboto-light': require('./assets/fonts/Roboto-Light.ttf'),
            'roboto-bolditalic': require('./assets/fonts/Roboto-BoldItalic.ttf'),
            'yellowtail-regular': require('./assets/fonts/Yellowtail-Regular.ttf'),
            'roboto-bold': require('./assets/fonts/Roboto-Bold.ttf'),
        })

        const images = [
            require('./app/Asset/header_logo.png'),
            require('./app/Asset/qibla.png'),
            require('./app/Asset/home_active.png'),
            require('./app/Asset/home_rest.png'),
            require('./app/Asset/guide.png'),
            require('./app/Asset/guide_rest.png'),
            require('./app/Asset/hotel_active.png'),
            require('./app/Asset/hotel_rest.png'),
            require('./app/Asset/flight_active.png'),
            require('./app/Asset/flight_rest.png'),
            require('./app/Asset/alert_active.png'),
            require('./app/Asset/alert_rest.png'),
            require('./app/Asset/message_active.png'),
            require('./app/Asset/message_rest.png'),
            require('./app/Asset/setting_active.png'),
            require('./app/Asset/setting_rest.png'),
            require('./app/Asset/bg.png'),
            require('./app/Asset/sendmsg.png'),
            require('./app/Asset/user.png'),
            require('./app/Asset/signout.png'),
            require('./app/Asset/all_trips.png'),
            require('./app/Asset/arrow_left_rest.png'),
            require('./app/Asset/arrow_left.png'),
            require('./app/Asset/arrow_right.png'),
            require('./app/Asset/arrow_right_rest.png'),
            require('./app/Asset/setting_dark.png'),
            require('./app/Asset/tick.png'),
            require('./app/Asset/back_arrow.png'),
            require('./app/Asset/drop_down.png'),
            require('./app/Asset/compass.png'),
            require('./app/Asset/qibla_compass.png'),

        ];
        const cacheImages = images.map(image => {
            return Asset.fromModule(image).downloadAsync();
        });

        var s = new Promise((resolve, reject) => {
            // onSignOutUser();
            isSignedIn()
                .then((r) => {
                    if (r) {// if it is logged in
                        getUserObject().then((user) => {
                            // console.log(user);
                            if (JSON.parse(user).user_type == 'agent' || JSON.parse(user).user_type == 'staff') {
                                resolve(true)
                            } else {
                                getData(get_current_trip_url, 'GET', '')
                                    .then((res) => {
                                        saveMainObject(trip_key, JSON.stringify(res))
                                            .then((res) => {
                                                resolve(true);
                                            })
                                            .catch((res) => {
                                                reject(false);
                                            })
                                    })
                                    .catch(err => {
                                        reject(false)
                                    });
                            }
                        })
                    } else { // if it is logged out
                        resolve(true)
                    }
                })
                .catch((e) => {
                    // console.log('error');
                    resolve(true)
                })
        });
        return Promise.all([ss, cacheImages, s]).catch((err) => {
            console.log(err)
        })
    }

    _handleNotificationSaved = (notification) => {
        this.setState({notification: notification});
        if (this.state.notification.data.type === 'message') {
            this.setState({
                value_message: 'message',
                value_trip_id: this.state.notification.data.data.trip_id,
            })
        } else if (this.state.notification.data.type === 'agent') {
        } else {
            this.setState({
                value_alert: 'emergency_alert',
                value_trip_id: this.state.notification.data.data.trip_id,
            })
        }
        if (this.state.notification.origin === 'received' && Platform.OS === 'ios') {
            // Alert.alert(
            //     'Notification',
            //     message
            //     ,
            //     [
            //         // {
            //         //     text: 'Cancel',
            //         //     onPress: () => console.log('Cancel Pressed'),
            //         //     style: 'cancel',
            //         // },
            //         {
            //             text: 'Okay',
            //             onPress: () => {
            //                 console.log('Open pressed')
            //             }
            //         },
            //     ],
            //     {cancelable: false},)
        }
    };
    _handleNotification = (notification) => {
        this.setState({notification: notification});
        saveMainObject(notification_key, JSON.stringify(notification)).then((res) => {
        }).catch((res) => {
        });

        var message = ''
        if (this.state.notification.data.type === 'message') {
            message = 'New message received.';
            this.setState({
                value_message: 'message',
                value_alert: '',
                value_trip_id: this.state.notification.data.data.trip_id,
            })
        } else if (this.state.notification.data.type === 'agent') {

        } else {
            message = 'New notification received.';
            this.setState({
                value_message: '',
                value_alert: 'emergency_alert',
                value_trip_id: this.state.notification.data.data.trip_id,
            })
        }
        if (this.state.notification.origin === 'received' && Platform.OS === 'ios') {
            Alert.alert(
                'Notification',
                message
                ,
                [
                    // {
                    //     text: 'Cancel',
                    //     onPress: () => console.log('Cancel Pressed'),
                    //     style: 'cancel',
                    // },
                    {
                        text: 'Okay',
                        onPress: () => {
                            console.log('Open pressed')
                        }
                    },
                ],
                {cancelable: false},)

        }
    };

    render() {
        if (!this.state.isReady) {
            return (
                <AppLoading
                    startAsync={this._loadAssetsAsync}
                    onFinish={() => {
                        this.setState({isReady: true})
                    }}
                    onError={(err) => {
                        errorAlert('Error fetching new data.')
                        // return;
                    }}
                />
            );
        } else {
            return (
                <Provider value={{
                    val_message: this.state.value_message,
                    val_alert: this.state.value_alert,
                    val_trip_id: this.state.value_trip_id,
                }}>
                    <SwitchNav/>
                </Provider>
            )
        }
    }
}

const UserStack = createStackNavigator({

        Loginn: {
            screen: Loginn,
            navigationOptions: {
                title: 'Login',
                header: null,
            },
        },
        ForgotPassword: {
            screen: ForgotPassword,
            navigationOptions: {
                title: 'Forgot Password',
                header: null,
            },
        },
    },
    {
        headerMode: 'float',
        defaultNavigationOptions: {
            headerStyle: {
                backgroundColor: Colors.primary_color,
            },
            headerTintColor: Colors.header_text_color,
            headerTitleStyle: {
                fontWeight: TextFontStyles.header_font_weight,
            },
        }
    }
);
const HomeStack = createStackNavigator({
        TripDetails: {
            screen: TripDetails,
        },
        CheckList: {
            screen: CheckList,

        },
        ImportantInformation: {
            screen: ImportantInformation,
        },
        ImageScreen: {
            screen: ImageScreen,
        },
    },
    {
        headerMode: 'screen',
        headerLayoutPreset: 'left',

        // headerTintColor: "#000",
        // initialRouteName: 'LoginScreen',
    }
);
const AllTripsStack = createStackNavigator({
        Home: {
            screen: Home,
        },
        ImageScreen: {
            screen: ImageScreen,
        },
    },
    {
        headerMode: 'screen',
        headerLayoutPreset: 'left',
    }
);
const CurrentTripsStack = createStackNavigator({
        TripDetails: TripDetails,
        CheckList: {
            screen: CheckList,

        },
        ImportantInformation: {
            screen: ImportantInformation,
        },
        ImageScreen: {
            screen: ImageScreen,
        },
    },
    {
        headerMode: 'screen',
        headerLayoutPreset: 'left',
    }
);

const GuideStack = createStackNavigator({
        Guide: {
            screen: Guide,
        },
        GuideData: {
            screen: GuideData,
        },
    },
    {
        headerMode: 'screen',
        headerLayoutPreset: 'left'
    }
);
const AdminStack = createStackNavigator({
        AdminStack: {
            screen: AdminHome,
        },
    },
    {
        headerMode: 'screen',
        headerLayoutPreset: 'left'
    }
);
const HotelStack = createStackNavigator({
        Hotel: {
            screen: Hotel,
        },
        ImageScreen: {
            screen: ImageScreen,
        },
    },
    {
        headerMode: 'screen',
        headerLayoutPreset: 'left'
    }
);

const FlightStack = createStackNavigator({
        FlightStack: {
            screen: Flight,
        },

    },
    {
        headerMode: 'screen',
        headerLayoutPreset: 'left'
    }
);
const AlertStack = createStackNavigator({
        AlertTrip: {
            screen: AlertTrip,
        },

    },
    {
        headerMode: 'screen',
        headerLayoutPreset: 'left'
    }
);
const MessageStack = createStackNavigator({
        Message: {
            screen: Message,
        },

    },
    {
        headerMode: 'screen',
        headerLayoutPreset: 'left'
    }
);
const CheckListStack = createStackNavigator({
        CheckList: {
            screen: CheckList,
        },
    },
    {
        headerMode: 'screen',
        headerLayoutPreset: 'left',
        navigationOptions: {
            tabBarVisible: false
        }

    }
);
const QiblaDirectionStack = createStackNavigator({
        QiblaDirection: {
            screen: Compass,
        },
    },
    {
        headerMode: 'screen',
        headerLayoutPreset: 'left',
        navigationOptions: {
            tabBarVisible: false
        }

    }
);
const SettingsStack = createStackNavigator({
        Settings: {
            screen: Settings,
            navigationOptions: {
                title: 'Settings',
            }
        },
    },
    {
        headerMode: 'float',
    }
);


const TabNav = createBottomTabNavigator(
    {
        Home: {
            screen: HomeStack,
        },
        GuideStack: {
            screen: GuideStack,
        },
        HotelStack: {
            screen: HotelStack,
        },
        FlightStack: {
            screen: FlightStack,
        },
        AlertTrip: {
            screen: AlertStack,
        },
        MessageStack: {
            screen: MessageStack,
        },
        Settings: {
            screen: SettingsStack,
        },
    },
    {
        tabBarComponent: TabBar,
        // tabBarOptions: {
        //     activeTintColor: Colors.primary_color,
        //     inactiveTintColor: Colors.primary_dark_color,
        //     keyboardHidesTabBar:true,
        // },

        lazy: true,

        tabBarOptions:{
            keyboardHidesTabBar:false
        }

    }
);

const SwitchNavigator = createSwitchNavigator(
    {
        AuthLoading: AuthLoadingScreen,
        Auth: UserStack,
        AppHome: TabNav,
        AllTripsStack: AllTripsStack,
        CurrentTripsStack: CurrentTripsStack,
        SettingsStack: SettingsStack,
        QiblaDirection: QiblaDirectionStack,
        AdminStack: AdminStack,

    },
    {
        initialRouteName: 'AuthLoading',
    }
)

const SwitchNav = createAppContainer(SwitchNavigator);


