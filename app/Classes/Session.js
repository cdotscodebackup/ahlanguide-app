
import { 
    AsyncStorage ,ToastAndroid
  } 
from 'react-native';

class Session {


    static _storeData = async (key,value) => {
        try {
          await AsyncStorage.setItem(key, value);
          // ToastAndroid.show("value", ToastAndroid.SHORT);  
        } catch (error) {
          // Error saving data
          ToastAndroid.show("error storing", ToastAndroid.SHORT);  
        }
      }

      static getdata= (key) => {

        return AsyncStorage.getItem(key).then((value) => {
          ToastAndroid.show("value "+value, ToastAndroid.SHORT);
            return (value);
        });

      }
    
      static _retrieveData = async (key) => {
        try {
          const value = await AsyncStorage.getItem(key);
          // ToastAndroid.show("value "+value, ToastAndroid.SHORT);
          if (value !== null) {
            return value;
          }
         } catch (error) {
           // Error retrieving data
           ToastAndroid.show("error retrieving", ToastAndroid.SHORT);
         }
      }


    //   static _retrieveUserDetails = async () => {
    //       const fname;// = await AsyncStorage.getItem(SessionKeys.user_fname);
    //       this._retrieveData(SessionKeys.user_fname).then((value)=>{
    //         fname= value;  
    //     });

    //     const lname;// = await AsyncStorage.getItem(SessionKeys.user_fname);
    //       this._retrieveData(SessionKeys.user_lname).then((value)=>{
    //         lname= value;  
    //     });
    //     const email;// = await AsyncStorage.getItem(SessionKeys.user_fname);
    //       this._retrieveData(SessionKeys.user_email).then((value)=>{
    //         email= value;  
    //     });
    //     const phone;// = await AsyncStorage.getItem(SessionKeys.user_fname);
    //       this._retrieveData(SessionKeys.user_phone).then((value)=>{
    //         phone= value;  
    //     });
          
    //     return [fname,lname,email,phone];

    // }
  }

export default Session;
export class SessionKeys {

    static user_email="user_email";
    static user_fname="user_fname";
    static user_lname="user_lname";
    static user_phone="user_phone";
    static is_user_login="is_user_login";
    static user_login_true="true";
    static user_login_false="false";
    static customer_id="customer_id";
    


    }