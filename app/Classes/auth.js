import {Alert, AsyncStorage, NetInfo, ToastAndroid} from "react-native";
import CartConstants from "./CartConstants";
import {all_trips_key, internetError, notification_key, single_trip_url, trip_key} from "./UrlConstant";

export const USER_KEY = "auth-key";
export const USER = "user-key";

export const onSignIn = () => AsyncStorage.setItem(USER_KEY, "true");
export const onSignOut = () => AsyncStorage.removeItem(USER_KEY);

export const onSignInUser = (user) => {
    AsyncStorage.setItem(USER_KEY, "true");
    AsyncStorage.setItem(USER, user);
}

export const onSignOutUser = () => {
    AsyncStorage.removeItem(USER_KEY)
    AsyncStorage.removeItem(USER);
    AsyncStorage.removeItem(trip_key);
    AsyncStorage.removeItem(all_trips_key);
}

export const isSignedIn = () => {
    return new Promise((resolve, reject) => {
        AsyncStorage.getItem(USER_KEY)
            .then(res => {
                if (res !== null) {
                    resolve(true);
                } else {
                    resolve(false);
                }
            })
            .catch(err => reject(err));
    });
};
export const isNetConnected = () => {
    return new Promise((resolve, reject) => {
        NetInfo.getConnectionInfo().then(state => {
            if (state.isConnected) {
                resolve(true);
            }
            else {
                resolve(false);
            }
        }).catch(() => {
            reject(false);
        })

    });
};
export const successAlert = (message) => {
    Alert.alert('Success', message)
}
export const errorAlert = (message) => {
    Alert.alert('Error', message)
}
export const simpleAlert = (message) => {
    Alert.alert('Alert', message)
}

export const getUser = () => {
    return new Promise((resolve, reject) => {
        AsyncStorage.getItem(USER)
            .then(res => {
                if (res !== null) {
                    // console.log("user found")
                    resolve("Bearer " + JSON.parse(res).token);
                }
                // else {
                //     resolve(false);
                // }
            })
            .catch(err => {
                // console.log("User not found. Session expired")
                reject(err)
            });
    });
};
export const getUserObject = () => {
    return new Promise((resolve, reject) => {
        AsyncStorage.getItem(USER)
            .then(res => {
                if (res !== null) {
                    // console.log("user found")
                    resolve(res);
                }
                // else {
                //     resolve(false);
                // }
            })
            .catch(err => {
                // console.log("User not found. Session expired")
                reject(err)
            });
    });
};

export const getLocalizedDate = (str) => {
    // var str = "2019-08-02 05:50:00";

    var year = str.substring(0, 4);
    var month = str.substring(5, 7);
    var day = str.substring(8, 10);
    var hour = str.substring(11, 13);
    var minute = str.substring(14, 16);
    var second = str.substring(17, 19);

    // 2019-01-01T00:00:00
    // var ns = year + '-' + month + '-' + day + 'T' + hour + ':' + minute + ':' + second

    var date = Date.UTC(year, month - 1, day, hour, minute, second);


    // console.log('Date String: ' + ns)
    // console.log('Date utc seconds: ' + date)
    // console.log('Date from seconds: ' + new Date(date).toString())

    return new Date(date);

}
export const getLocalizedDate2 = (str) => {
    // var str = "2019-08-02 05:50:00";
    // 2019-08-03T14:24:06.000000Z

    var year = str.substring(0, 4);
    var month = str.substring(5, 7);
    var day = str.substring(8, 10);
    var hour = str.substring(11, 13);
    var minute = str.substring(14, 16);
    var second = str.substring(17, 19);

    var date = Date.UTC(str);


    // console.log('Date String: ' + ns)
    // console.log('Date utc seconds: ' + date)
    // console.log('Date from seconds: ' + new Date(date).toString())

    return new Date(date);

}

export const strip_html_tags = (str) => {
    if ((str === null) || (str === ''))
        return false;
    else
        str = str.toString();
    return str.replace(/<[^>]*>/g, '');
}


export const saveMainObject = (key, object) => {
    return new Promise((resolve, reject) => {

        AsyncStorage.setItem(key, object)
            .then(() => {
                // console.log(object)
                resolve(true);
            })
            .catch(() => {
                reject(false);
            })

    });
}
export const getSavedObject = (key) => {
    // console.log('calling')
    return new Promise((resolve, reject) => {
        AsyncStorage.getItem(key)
            .then(res => {
                // console.log(JSON.stringify(res))
                if (res !== null) {
                    resolve(res);
                }
                else {
                    // console.log("data not found")
                    resolve(false);
                }
            })
            .catch(err => {
                console.log("Error fetching saved item: " + err)
                reject(err)
            });
    });
};

export const getSavedObject2 = async (key) => {
    // console.log('calling')
    // return new Promise((resolve, reject) => {
    await AsyncStorage.getItem(key)
        .then(res => {
            // console.log(JSON.stringify(res))
            if (res !== null) {
                return (res);
            }
            else {
                // console.log("data not found")
                return (false);
            }
        })
        .catch(err => {
            console.log("Error fetching saved item: " + err)
            return false;
        });
    // });
};
export const removeMainObject = (key) => {
    AsyncStorage.removeItem(key)
}

export const getData = (url, method, formdata) => {
    console.log("Url: " + url)
    console.log((formdata))
    return new Promise((resolve, reject) => {
        var s = 404;
        getUser().then(token => {
            // console.log(url, method, token)
            fetch(url, {
                method: method,
                body: formdata,
                headers: {
                    Accept: 'application/json',
                    Authorization: token
                }
            })
                .then((response) => {
                        const statusCode = response.status;
                        var data = response.json();

                        console.log("Status code: " + statusCode);
                        // console.log("Status data: "+ JSON.stringify(data));
                        s = statusCode
                        return Promise.all([statusCode, data]);
                    }
                ).then(([status, data]) => {

                // console.log(data);
                if (status === 500) {
                    reject('Server Error')
                }
                else if (status === 422)
                    resolve(data)

                resolve(data)

            })
                .catch((error) => {
                    if (s === 200) {
                        resolve({"status": true})
                        s = 401
                    } else {
                        console.log("API Error: " + error)
                        if (error === 'TypeError: Network request failed')
                            reject('Network request failed. Try again.')
                        else
                            reject(error)

                    }
                });
        })
            .catch(err => {
                console.log("user not found " + err)
                reject("User logged out")
            })


    })

}
export const postMultipartData = (url, method, formdata) => {
    console.log("Url: " + url)
    console.log("form data: " + JSON.stringify(formdata))
    return new Promise((resolve, reject) => {
        var s = 404;
        getUser().then(token => {
            // console.log(url, method, token)
            fetch(url, {
                method: method,
                body: formdata,
                headers: {
                    Accept: 'application/json',
                    Authorization: token
                }
            })
                .then((response) => {
                        const statusCode = response.status;
                        var data = response.json();

                        console.log("Status code: " + statusCode);
                        // console.log("Status data: "+ JSON.stringify(data));
                        s = statusCode
                        return Promise.all([statusCode, data]);
                    }
                ).then(([status, data]) => {
                // console.log("Status data: "+ JSON.stringify(data));
                resolve(data)
            })
                .catch((error) => {
                    if (s == 200) {
                        resolve({"status": true})
                        s == 401
                    } else {
                        console.log("API Error: " + error)
                        reject(error)
                    }
                });
        })
            .catch(err => {
                console.log("user not found " + err)
                reject("User logged out")
            })
    })
}

export const getDataWithoutToken = (url, method, formdata) => {
    console.log("Url: " + url)
    // console.log("form data: " + JSON.stringify(formdata))
    return new Promise((resolve, reject) => {

        fetch(url, {
            method: method,
            body: formdata,
            headers: {
                Accept: 'application/json',
                //    'Content-Type': 'application/json',
                //     Authorization: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjkyMGQ0YTJiNzdmMTM0MmUxMmJlMzI5YjFjNzc2NmE2NmM1NzI0ZmZhMDBkMTNlZDE4YmIzMWZmYzI4M2QwNWE3NWU2YzFhM2RjY2RhZmQzIn0.eyJhdWQiOiI0IiwianRpIjoiOTIwZDRhMmI3N2YxMzQyZTEyYmUzMjliMWM3NzY2YTY2YzU3MjRmZmEwMGQxM2VkMThiYjMxZmZjMjgzZDA1YTc1ZTZjMWEzZGNjZGFmZDMiLCJpYXQiOjE1NjE5ODg5MDAsIm5iZiI6MTU2MTk4ODkwMCwiZXhwIjoxNTkzNjExMzAwLCJzdWIiOiI1Iiwic2NvcGVzIjpbIioiXX0.MFMjTrJQlo5zURdObSqJP3HdnbdBeXSkQyy8aV3RRUUpOy_g2tfKlv81IYn4P2sVT7dzuL7nPeM8PM_hyIZa569s9R76WhSh94XMJ4NJRGx9DUz6rO94CJL87ZCrfMurvYkDhpEKCQxmxvoELpdbkvuYWFDDnwjuLPimOMbtjeLxOeisVd2CKhnAySQz5c6WBDAyVfUWmg-n-vmKuXmeZnJB-4_fVB89ZjV1GjAjX-t3-hzwxGh0rbJeosroPet8JhaVg6L6e66nLUCyvKt3ZPmlP52DkbxJeZntJ6Fx6LZDc_evUdshawk52yofPimr3OR3BTXP5KBbPJDmRm55v_zrRELh3WPhSIu40kXovqz2BklKy3o7MWLMNPIbx597XaoReu_XKZy4Dq70Tyi32Ohr4acmGHP_8KHZqjRnJjv1VNh8C-WUsheJJik2_AUqwpk9nG8FvP3HEjpFDY_G-V_oMAc5W_533qL4tI2Kyao-Lhgj-pXDWqu5oyyGDAkWt0z47gFugLX7MvTiaawOj6eyQ3rWc8XQm65EyMj9rZ0FsIpbhcvwb89hjxHRnRStdhH8alWvwTNStQYtKNgOSdFEOk91d_9qxme7r9RrvzmsFWI1qh2JmlQc9huzQ2rOEvQfyao-Dal1RrV_KTVnMbP1-IOFE4hDynnZuvMxt5o"
            }
        })
            .then((response) => {
                    const statusCode = response.status;
                    var data = response.json();
                    console.log("Status code: " + statusCode);
                    console.log("Status data: " + (data));
                if (statusCode === 200 && url ==='http://ahlanguide.com/api/v1/forgot/password') {
                    resolve({"status": true})
                }

                    return Promise.all([statusCode, data]);
                }
            ).then(([status, data]) => {
            if (status == 500)
                reject('Server Error')
            else if (status == 422)
                resolve(data)
            else if (status == 403)
                reject("You are not authorized to login.")

            resolve(data)
        })
            .catch((error) => {
                console.log("API Error: " + error)
                reject(error)
            });

    })

}








