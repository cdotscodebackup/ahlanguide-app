


// export const BASE_URL= 'http://hajj.creative-dots.com'
export const BASE_URL= 'http://ahlanguide.com'
export const fcm_web_route= BASE_URL+'/fcm/'
export const admin_dashboard_url= BASE_URL+'/admin/dashboard'
export const base_picture_url= BASE_URL+'/storage/sections/';
export const base_section_picture_url= BASE_URL+'/storage/StepImages/';
export const base_profile_picture_url= BASE_URL+'/storage/profile_images/';
export const base_guides_url= BASE_URL+'/storage/guides/';
export const base_video_url= BASE_URL+'/storage/StepViodes/';
export const video_url= 'https://file-examples.com/wp-content/uploads/2017/04/';
export const base_hotel_picture_url= BASE_URL+'/storage/hotel_images/';

// http://www.ahlanguide.com/storage/profile_images/1574143599_download.jpg
// http://ahlanguide.com/storage/StepImages/1574143669_download.jpg


export const login_url=BASE_URL+ '/api/v1/login'
export const logout_url=BASE_URL+ '/api/v1/logout'
export const get_all_trips_url=BASE_URL+ '/api/v1/client/trip'
export const get_current_trip_url=BASE_URL+ '/api/v1/client/latest_trip'
export const single_trip_url=BASE_URL+ '/api/v1/client/trip/'

export const save_alert_url=BASE_URL+ '/api/v1/client/emergency_alert/'
export const save_feedback_url=BASE_URL+ '/api/v1/client/feedback/'

export const update_user_url=BASE_URL+ '/api/v1/client/update'
export const update_dp_url=BASE_URL+ '/api/v1/client/dp'
export const send_msg_url=BASE_URL+ '/api/v1/client/message'
export const get_all_msg_url=BASE_URL+ '/api/v1/client/message/'

export const fcm_client_url=BASE_URL+ '/api/v1/client/fcm'
export const fcm_agent_url=BASE_URL+ '/api/v1/admin/fcm'
export const admin_login_url=BASE_URL+ '/api/v1/admin/login'

export const forget_password_url=BASE_URL+ '/api/v1/forgot/password'
export const forget_password_check_email_url=BASE_URL + '/api/v1/resetcheck'


export const internetError='No internet connection'
export const general_network_error='Some error occurred. Try again.'
export const permissionError='*Enable location permission to send emergency alert.'
export const gpsError='*Enable GPS to send emergency alert.'




export const trip_key='trip_key'
export const all_trips_key='all_trips_key'
export const notification_key='notification_key'
export const message_key='message_key'
export const guide_key='guide_key'
