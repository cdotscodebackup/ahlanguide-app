class CartConstants {

    static key='12345';
    static user_login_url='https://ahmedjahanzaib.com/index.php?route=extension/feed/rest_api/login&key=12345';
    static user_signup_url='https://ahmedjahanzaib.com/index.php?route=extension/feed/rest_api/register&key=12345';


    static home_slide_show_images='https://ahmedjahanzaib.com/index.php?route=extension/feed/rest_api/getSlideShow&key=12345';
    static home_latest_products=(limit)=>{
        return 'https://ahmedjahanzaib.com/index.php/?route=extension/feed/rest_api/getLatest&limit='+limit+'&key=12345';
    }
    static home_special_products=(limit)=>{
        return 'https://ahmedjahanzaib.com/index.php/?route=extension/feed/rest_api/getLatest&limit='+limit+'&key=12345';
    }

    static addtocart_url='https://ahmedjahanzaib.com/index.php?route=extension/feed/rest_api/addCart&key=12345';
    static cartitem_url='https://ahmedjahanzaib.com/index.php?route=extension/feed/rest_api/cartProducts&key=12345';
    static edit_account_url='https://ahmedjahanzaib.com/index.php?route=extension/feed/rest_api/editCustomer&key=12345'
    static edit_password_url='https://ahmedjahanzaib.com/index.php?route=extension/feed/rest_api/editPassword&key=12345'
    static categories_url='https://ahmedjahanzaib.com/index.php?route=extension/feed/rest_api/getCategories&key=12345';
    static product_list_by_category_url=(id)=>{
        return 'https://ahmedjahanzaib.com/index.php?route=extension/feed/rest_api/products&category_id='+id+'&key=12345';
    }
    static menu_categories='http://ahmedjahanzaib.com/index.php?route=extension/feed/rest_api/getMenuCategories&key=dOTVR0FzhdV6';



}

export default CartConstants;