import React from "react";
import { View, Text,Button,StyleSheet ,TouchableOpacity,TouchableWithoutFeedback,Image, } from "react-native";
import Colors from "../Styles/Colors";




export class NoNetwork extends React.Component {
    _onPress = () => {
        this.props.onPressItem(this.props.item);
      }; 
    render() {
        return (
            <View style={{flex: 1,width:'100%',height:'100%',backgroundColor:Colors.light_background_color, position:"absolute"}}>
              <Text style={{flex:1,justifyContent:"flex-start",
                            alignSelf:"center",margin:20,
                            }} >
                    No Network Connection
              </Text>
          </View>
        );
      }


  }
  
