import React, { PureComponent } from 'react';
import { View, Text, NetInfo, Dimensions, StyleSheet,Alert } from 'react-native';

const { width } = Dimensions.get('window');

function MiniOfflineSign() {
  return (
    <View style={styles.offlineContainer}>
      <Text style={styles.offlineText}>No Internet Connection</Text>
    </View>
  );
}

class OfflineNotice extends React.Component {
    _isMounted=false;
    state = {
    isInternetConnected: true
  };
  

  componentDidMount() {
      this._isMounted=true;
   NetInfo.isConnected.addEventListener('connectionChange', this.handleChange);
    // TripAlert.alert("ofline mount");
  }

  componentWillUnmount() {
    // TripAlert.alert('ofline unmount')
    // NetInfo.removeEventListener('connectionChange', this.handleConnectivityChange);
    this._isMounted=false;
}

  handleChange = isInternet => {
    // TripAlert.alert('connectivity changed'+ this.state.isInternetConnected)
    if(this._isMounted)
        this.setState({ isInternetConnected:isInternet });
  }

  render() {
    if (!this.state.isInternetConnected) {
      return <MiniOfflineSign />;
    }
    return null;
  }
}

const styles = StyleSheet.create({
  offlineContainer: {
    backgroundColor: '#b52424',
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    width,
    position: 'absolute',
    top: 0
  },
  offlineText: { color: '#fff' }
});

export default OfflineNotice;