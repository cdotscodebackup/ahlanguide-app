import React from "react";
import {
    View,
    StyleSheet,
    TouchableWithoutFeedback,
    Image,
} from "react-native";
import Colors from "../Styles/Colors";
import dimen from "../Styles/Dimen";
import AppText from "./AppText";

export class GuideCard extends React.Component {

    _onPress = () => {
        this.props.onPressItem(this.props.item);
    };
    _onPressDownload = () => {
        this.props.onPressItemDownload(this.props.item);
    };
    _onPressYoutube = () => {
        this.props.onPressYoutube(this.props.item);
    };


    render() {
        let title = this.props.item.title === null ? '' : this.props.item.title;
        let isPath = this.props.item.path !== null
        let isYoutubeLink = this.props.item.youtube_link !== null
        let fileTypeName = 'link';

        if (isPath) {
            let filetype = this.props.item.path.split('.');
            if (filetype.length > 0) {
                fileTypeName = filetype[filetype.length - 1];
            }
        }
        return (
            <View style={styles.container}>
                <TouchableWithoutFeedback onPress={this._onPress}>
                    <View style={styles.titleContainer}>
                        <AppText style={styles.titleText}>
                            {title}
                        </AppText>
                        {<AppText style={styles.typeText}>
                                {fileTypeName}
                        </AppText>}
                    </View>
                </TouchableWithoutFeedback>

                {isYoutubeLink &&
                <TouchableWithoutFeedback onPress={this._onPressYoutube}>
                    <View style={styles.downloadButtonContainer}>
                        <Image style={styles.downloadImage} source={require('../Asset/video_icon.png')}/>
                    </View>
                </TouchableWithoutFeedback>}

                {isPath && false &&
                <TouchableWithoutFeedback onPress={this._onPressDownload}>
                    <View style={styles.downloadButtonContainer}>
                        <Image style={styles.downloadImage} source={require('../Asset/download_guide.png')}/>
                    </View>
                </TouchableWithoutFeedback>}
            </View>
        );
    }


}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row', flex: 1, justifyContent: 'space-between', alignItems: 'center',
        borderBottomWidth: 1, borderColor: Colors.border_color
    },
    titleContainer: {
        justifyContent: 'flex-start',
        padding: dimen.app_padding,
        width: 0,
        flexGrow: 1,
        alignItems: 'flex-start'
    },
    titleText: {fontSize: 17, color: Colors.primary_color},
    typeText: {fontSize: 12, marginTop: 4},
    downloadButtonContainer: {padding: dimen.app_padding},
    downloadImage: {flex: 1, width: 16, height: 16, resizeMode: 'contain', alignSelf: 'center'},


});
