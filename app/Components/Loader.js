import {ActivityIndicator, Dimensions, View, StyleSheet, Image, TouchableOpacity} from "react-native";
import React from "react";
import Colors from "../Styles/Colors";
import AppText from "./AppText";
import dimen from "../Styles/Dimen";

export const Loader = () => (
    <View style={{
        justifyContent: 'center', alignItems: 'center', position: 'absolute',
        left: 0,
        right: 0,
        opacity: 0.5,
        backgroundColor: 'black',
        top: 0,
        bottom: 0,
    }}>
        <ActivityIndicator size="large" color={Colors.primary_color}/>
    </View>
)

export class Chooser extends React.Component {

    cancelPress = () => {
        this.props.cancelPress(this.props.item);
    };
    takePhoto = () => {
        this.props.takePhotoPress(this.props.item);
    };
    choosePhoto = () => {
        this.props.choosePhotoPress(this.props.item);
    };

    render() {
        return (

            <View style={{
                justifyContent: 'center', alignItems: 'center', position: 'absolute',
                left: 0,
                right: 0,
                opacity: 1,
                backgroundColor: 'rgba(52, 52, 52, 0.8)',
                top: 0,
                bottom: 0,
            }}>
                <View style={{
                    width: dialogWidth,
                    backgroundColor: Colors.home_widget_background_color,
                    padding: dimen.app_padding,
                    borderRadius: dimen.border_radius
                }}>
                    <View>
                        <TouchableOpacity onPress={this.choosePhoto}>
                            <AppText style={style.dialogItemStyle}>Choose File</AppText>
                        </TouchableOpacity>
                    </View>
                    <View>
                        <TouchableOpacity onPress={this.takePhoto}>
                            <AppText style={style.dialogItemStyle}>Open Camera</AppText>
                        </TouchableOpacity>
                    </View>
                    <View>
                        <TouchableOpacity onPress={this.cancelPress}>
                            <AppText style={[style.dialogItemStyle, {alignSelf: 'flex-end'}]}>Cancel</AppText>
                        </TouchableOpacity>
                    </View>
                </View>
                {/*<ActivityIndicator size="large" color={Colors.primary_color}/>*/}
            </View>
        )
    }
}

const deviceWidth = Dimensions.get('window').width;
const dialogWidth = (deviceWidth * 2) / 3;

const style = StyleSheet.create({
    dialogItemStyle: {fontSize: 16, margin: 10}
})