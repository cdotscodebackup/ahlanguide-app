import React from "react";
import {
    View,
    Text,
    Button,
    StyleSheet,
    TouchableOpacity,
    TouchableHighlight,
    TouchableWithoutFeedback,
    Image,
} from "react-native";
import {Card} from "react-native-elements"
import Colors from "../Styles/Colors";
import HomeStyles from "../Styles/HomeStyles";
import dimen from "../Styles/Dimen";
import AppText from "./AppText";
import {getSavedObject, saveMainObject} from "../Classes/auth";
import {trip_key} from "../Classes/UrlConstant";

export class CheckListCard extends React.Component {
    constructor() {
        super()
        this.state = {
            hidenfield: false,
            isselected: false,
        }
    }

    handleCheckBox = () => {


        this.setState((prevState) => ({
            isselected: !prevState.isselected
        }), function () {
            // console.log('key: '+this.props.item.description)
            // console.log('value: '+this.state.isselected.toString())

            saveMainObject(this.props.item.description + this.props.item.id, this.state.isselected.toString())
                .then((res) => {
                    // console.log("saved")
                })
                .catch((err) => {
                    // console.log("error saving ")
                })
        });


    }

    componentDidMount() {
        getSavedObject(this.props.item.description + this.props.item.id)
            .then((data) => {
                // console.log("data:  " +  data);
                if (data == 'true') {
                    // console.log("data true:  " +  data);
                    this.setState(() => ({isselected: true}));
                }
                // else
                // this.setState(() => ({isselected : false}));

            }).catch((err) => {
            // console.log("chek list error:  " +  err);
        })
    }

    render() {
        return (
            <View style={{
                flexDirection: 'row', marginBottom: 5, marginTop: 5,
                flex: 1, justifyContent: 'flex-start', alignItems: 'flex-start'
            }}>
                {this.props.item.description != 'No check list for this section' &&
                <View style={{marginRight: 10, justifyContent: 'center', alignItems: 'center'}}>
                    {!this.state.isselected &&
                    <TouchableOpacity onPress={this.handleCheckBox}>
                        <View style={{
                            width: 20,
                            height: 20,
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderWidth: 1,
                            borderColor: Colors.normal_text_color
                        }}>
                        </View>
                    </TouchableOpacity>
                    }
                    {this.state.isselected &&
                    <TouchableOpacity onPress={this.handleCheckBox}>
                        <View style={{
                            width: 20,
                            height: 20,
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: Colors.primary_color
                        }}>
                            <Image source={require('../Asset/tick.png')}
                                   style={{width: 11, height: 11, alignSelf: 'center', resizeMode: 'contain'}}/>
                        </View>
                    </TouchableOpacity>
                    }

                </View>}
                <View style={{width: 0, flexGrow: 1,}}>
                    <AppText style={{lineHeight: 28}}>
                        {this.props.item.description}
                    </AppText>
                </View>
            </View>
        );
    }


}

const style = StyleSheet.create({
    wraper: {
        backgroundColor: '#F00303',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        color: '#FFFFFF',
        fontSize: 35,
        fontWeight: 'bold'

    },
    subtitle: {
        color: 'white',
        fontWeight: '200'
    }
});

