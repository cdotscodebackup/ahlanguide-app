import React from "react";
import {Image, StyleSheet, TouchableOpacity, View, Text, Dimensions} from "react-native";
import Colors from "../Styles/Colors";
import dimen from "../Styles/Dimen";
import AppText from "./AppText";
import TextStyles from "../Styles/TextStyles";
import {base_profile_picture_url, base_section_picture_url} from "../Classes/UrlConstant";
import {strip_html_tags} from "../Classes/auth";


const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export class TripStepCard extends React.Component {
    constructor() {
        super()
    }

    _onPress_check_list = () => {
        this.props.onPressCheckList(this.props.item.step_check_lists);

    };
    _onPress_important_information = () => {
        this.props.onPressImportantInformation(this.props.item.information);
    };
    imagePress = () => {
        if (this.props.item.path !== null) {
            // console.log(base_section_picture_url + this.props.item.path)
            this.props.imagePress(base_section_picture_url + this.props.item.path);
        }
    };

    render() {

        let tripStep = this.props.item;
        let stepimage = tripStep.path;
        let title = tripStep.title
        var description = strip_html_tags(tripStep.description)

        return (
            <View style={styles.mainContainer}>
                <TouchableOpacity onPress={this.imagePress} style={styles.imageContainer}>
                    <Image style={styles.imageStyle}
                           source={stepimage === null ? require('../Asset/dumy_img.png') : {
                               uri: base_section_picture_url + stepimage
                           }}
                    />
                </TouchableOpacity>
                <View style={styles.descContainer}>
                    <View style={styles.titleContainer}>
                        <AppText style={styles.titleText}>
                            {title}
                        </AppText>
                    </View>


                    <View style={styles.descDetailsContainer}>
                        <Text style={styles.descText}>
                            {description}
                        </Text>
                    </View>

                    <View style={styles.buttonContainer}>

                        {this.props.item.step_check_lists.length > 0 &&
                        <TouchableOpacity onPress={this._onPress_check_list}>
                            <View style={TextStyles.smallButtonView}>
                                <AppText style={TextStyles.smallButtonText}>
                                    Check List
                                </AppText>
                            </View>
                        </TouchableOpacity>
                        }
                        {this.props.item.information !== '' &&
                        <TouchableOpacity onPress={this._onPress_important_information}>
                            <View style={[TextStyles.smallButtonView, {marginLeft: 10}]}>
                                <AppText style={TextStyles.smallButtonText}>
                                    Important Information
                                </AppText>
                            </View>
                        </TouchableOpacity>
                        }
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: Colors.home_widget_background_color,
        marginBottom: 10,
        borderWidth: 1,
        overflow: 'hidden',
        borderColor: Colors.border_color,
        borderRadius: 10,
        flexDirection: 'column', flex: 1,
    },
    imageContainer: {
        width: windowWidth - (dimen.app_padding * 2),
        height: (windowWidth / 1.7) - (dimen.app_padding * 2),
        borderRadius: 10,
        overflow: 'hidden',
        // backgroundColor: 'red'
    },
    imageContainer2: {
        // width: '100%',
        // height: 200,
        width: windowWidth - (dimen.app_padding * 2),
        height: (windowWidth / 1.7) - (dimen.app_padding * 2)
    },
    imageStyle: {
        flex: 1,
        resizeMode: 'cover'
    },
    descContainer: {
        flex: 1,
        overflow: 'hidden',
        padding: dimen.app_padding,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'stretch'
    },
    titleText: {fontSize: 15, fontWeight: 'bold', color: Colors.primary_color},
    titleContainer: {flex: 1,},
    descDetailsContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        marginTop: 10,
        overflow: 'hidden'
    },
    descText: {
        flex: 1, fontSize: 14, lineHeight: 24, paddingTop: 4, justifyContent: 'center',
        color: Colors.normal_text_color
    },
    buttonContainer: {
        flex: 1,
        overflow: 'hidden',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginTop: dimen.app_padding,
        alignItems: 'flex-start',
    },


});

