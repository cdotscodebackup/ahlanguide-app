import React from "react";
import { View, Text,Button,StyleSheet  } from "react-native";

export class Splash extends React.Component {
      render() {
        return (
            <View style={style.wraper}>
                <Text style={style.title}>'Open Cart'</Text>
                <Text style={style.subtitle}>'Powerd By Jaci Technology'</Text>
            </View>
        );
      }


  } 
  const style = StyleSheet.create({
    wraper: {
        backgroundColor: '#F00303',
        flex: 1,
        justifyContent: 'center', 
        alignItems: 'center'
      },
      title: {
        color: '#FFFFFF',
        fontSize: 35,
        fontWeight: 'bold'

      },
      subtitle: {
          color: 'white',
          fontWeight: '200'
      }
});