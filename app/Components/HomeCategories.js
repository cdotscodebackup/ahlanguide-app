import React from "react";
import { View, Text,Button,StyleSheet ,TouchableOpacity,TouchableWithoutFeedback,Image, } from "react-native";

export class HomeCategories extends React.Component {
    _onPress = () => {
        this.props.onPressItem(this.props.item);
      }; 
    render() {
        return (
            <TouchableOpacity style={{flex:1,justifyContent:"center",alignItems:"center"}} onPress={this._onPress} >
                                <View style={{height:100,width:140,padding:10,borderWidth:0.5,borderRightColor:'grey'
                                                ,borderTopColor:'white',borderBottomColor:'white',alignItems:"center",justifyContent:"center",
                                                borderLeftColor:'white'}}>
                                    <Image
                                        style={{flex:1,width:100,height:100,resizeMode:"contain",backgroundColor:'white'}}
                                        source={{uri: this.props.item.image}}
                                    />
                                    {/* <Text style={{alignSelf:"center"}}>{this.props.item.name}</Text>
                                    <Text style={{alignSelf:"center"}}>{this.props.item.price} PKR</Text> */}
                                </View>
            </TouchableOpacity>
        );
      }


  } 
  const style = StyleSheet.create({
    wraper: {
        backgroundColor: '#F00303',
        flex: 1,
        justifyContent: 'center', 
        alignItems: 'center'
      },
      title: {
        color: '#FFFFFF',
        fontSize: 35,
        fontWeight: 'bold'

      },
      subtitle: {
          color: 'white',
          fontWeight: '200'
      }
});

 // <TouchableWithoutFeedback  onPress ={() => showItemDetails(item.id)}>
                            //     <View style={{height:200,width:'50%',margin:1,borderWidth:1,borderColor:'grey'}}>
                            //         <Image
                            //             style={{flex:1}}
                            //             source={{uri: item.image}}
                            //         />
                            //         <Text style={{alignSelf:"center"}}>{item.name}</Text>
                            //         <Text style={{alignSelf:"center"}}>{item.price} PKR</Text>
                            //     </View>
                            // </TouchableWithoutFeedback>