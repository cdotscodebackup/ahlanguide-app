// packages
import {BackHandler} from 'react-native';
/**
 * Attaches an event listener that handles the android-only hardware
 * back button
 * @param  {Function} callback The function to call on click
 */

const handlerback=(callback) => {
    callback();
    // return true;
}
const handleAndroidBackButton = callback => {
    BackHandler.addEventListener('hardwareBackPress', handlerback(callback));
};
/**
 * Removes the event listener in order not to add a new one
 * every time the view component re-mounts
 */
const removeAndroidBackButtonHandler = () => {
    console.log('listener removed')
    BackHandler.removeEventListener('hardwareBackPress', handlerback());
}



export {handleAndroidBackButton, removeAndroidBackButtonHandler};