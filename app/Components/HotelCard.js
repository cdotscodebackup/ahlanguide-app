import React from "react";
import {
    View,
    StyleSheet,
    TouchableWithoutFeedback,TouchableOpacity,
    Image, Dimensions,
} from "react-native";
import Colors from "../Styles/Colors";
import dimen from "../Styles/Dimen";
import AppText from "./AppText";
import {base_hotel_picture_url, base_section_picture_url} from "../Classes/UrlConstant";

export class HotelCard extends React.Component {
    constructor() {
        super()
    }

    _onPress = () => {
        this.props.onPressItem(this.props.item);
    };
    imagePress = () => {
        if (this.props.item.triphoteldetail.hotel.path !== null) {
            // console.log(base_section_picture_url + this.props.item.path)
            this.props.imagePress(base_hotel_picture_url + this.props.item.triphoteldetail.hotel.path);
        }
    };

    render() {
        let item = this.props.item;
        let path = item.triphoteldetail.hotel.path;
        let hotelName = item.triphoteldetail.hotel.name !== null ? item.triphoteldetail.hotel.name : '';
        let hotelAddress = item.triphoteldetail.hotel.address !== null ? item.triphoteldetail.hotel.address : 'Not Available';
        let roomNumber = item.room_number !== null ? item.room_number : "";

        return (
            <View style={styles.mainContainer}>
                <TouchableOpacity onPress={this.imagePress} style={styles.imageContainer}>
                    <Image style={styles.image}
                           source={path === null ? require('../Asset/dumy_img.png') : {
                               uri: base_hotel_picture_url + path
                           }}
                    />
                </TouchableOpacity>
                <View style={styles.dataContainer}>
                    <AppText style={styles.hotelNameText}>
                        {hotelName}
                    </AppText>
                    <View style={styles.buildingNameContainer}>
                        <AppText style={styles.buildingNameHeadingText} numberOfLines={3}>
                            Adress:{" "}
                        </AppText>
                        <View style={styles.buildingNameTextContainer}>
                            <AppText style={styles.buildingNameText} numberOfLines={3}>
                                {hotelAddress}
                            </AppText>
                        </View>
                    </View>
                    <View style={styles.roomNoContainer}>
                        <AppText style={styles.roomNoHeadingText} numberOfLines={3}>
                            Room No:{" "}
                        </AppText>
                        <AppText style={styles.roomNoText} numberOfLines={3}>
                            {roomNumber}
                        </AppText>
                    </View>
                    <View style={styles.locationContainer}>
                        <AppText style={styles.locationHeadingText} numberOfLines={3}>
                            Location:{" "}
                        </AppText>
                        <TouchableWithoutFeedback onPress={this._onPress}>
                            <AppText style={styles.locationButtonText} numberOfLines={3}>
                                click here
                            </AppText>
                        </TouchableWithoutFeedback>
                    </View>

                </View>
            </View>
        );
    }


}

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: Colors.home_widget_background_color,
        marginLeft: dimen.app_padding,
        marginRight: dimen.app_padding,
        marginBottom: dimen.app_padding,
        borderWidth: 1,
        borderColor: Colors.border_color,
        borderRadius: dimen.border_radius,
        flexDirection: 'row',
        flex: 1,
    },
    imageContainer: {
        // width: '30%',
        height: (windowWidth / 3) + (windowWidth / 9),
        width: windowWidth / 3.5,
        borderRadius: dimen.border_radius,
        overflow: 'hidden',
    },
    image: {
        flex: 1,
        resizeMode: 'cover',
    },
    dataContainer: {
        // width: '70%',
        flex: 1,
        padding: 12,
        flexDirection: 'column',
        justifyContent: 'space-evenly',
        alignItems: 'stretch'
    },
    hotelNameText: {
        fontSize: dimen.textinputfontsize,
        fontFamily: 'roboto-bold',
        color: Colors.primary_color
    },
    buildingNameContainer: {flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start'},
    buildingNameHeadingText: {marginTop: 5, fontSize: 12, color: 'black'},
    buildingNameTextContainer: {width: 0, flexGrow: 1,},
    buildingNameText: {marginTop: 5, fontSize: 12, color: Colors.normal_text_color},
    roomNoContainer: {flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start'},
    roomNoHeadingText: {marginTop: 5, fontSize: 12, color: 'black'},
    roomNoText: {marginTop: 5, fontSize: 12, color: Colors.normal_text_color},
    locationContainer: {flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start'},
    locationHeadingText: {marginTop: 5, fontSize: 12, color: 'black'},
    locationButtonText: {
        borderBottomColor: Colors.primary_color,
        borderBottomWidth: 1,
        marginTop: 5,
        fontSize: 12,
        color: Colors.primary_color
    },
});
