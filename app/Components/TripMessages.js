import React from "react";
import {
    View,
    Dimensions,
    Text,
    Button,
    StyleSheet,
    TouchableOpacity,
    TouchableHighlight,
    TouchableWithoutFeedback,
    Image,
} from "react-native";
import {Card} from "react-native-elements"
import Colors from "../Styles/Colors";
import HomeStyles from "../Styles/HomeStyles";
import dimen from "../Styles/Dimen";
import AppText from "./AppText";
import {ScrollView} from "react-native-gesture-handler";

export class TripMessages extends React.Component {
    render() {
        return (
            <View style={{
                width: Dimensions.get('window').width - (dimen.app_padding * 6),
                marginBottom: 5, paddingLeft: 5, paddingRight: 5,flex:1,
            }}>

                <View style={{flex: 1, alignSelf: 'flex-start'}}>
                    <AppText style={{color: Colors.primary_color, fontSize: dimen.textinputfontsize}}>
                        New Message
                    </AppText>
                    <AppText style={{color: Colors.normal_text_color, marginTop: 5}}>
                        {this.props.item.message}
                    </AppText>
                </View>

            </View>
        );
    }


}
