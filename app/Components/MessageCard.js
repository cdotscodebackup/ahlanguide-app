import React from "react";
import { View, Text,Button,StyleSheet ,TouchableOpacity,TouchableHighlight,TouchableWithoutFeedback,Image, } from "react-native";
import {Card} from "react-native-elements"
import Colors from "../Styles/Colors";
import HomeStyles from "../Styles/HomeStyles";
import dimen from "../Styles/Dimen";
import AppText from "./AppText";
import TextStyles from "../Styles/TextStyles";
import {getLocalizedDate} from "../Classes/auth";
import {base_profile_picture_url} from "../Classes/UrlConstant";

export class MessageCard extends React.Component {
    constructor(){
        super()
        this.state={
            hidenfield:false,
        }
    }
    _onPress = () => {
        this.props.onPressItem(this.props.item);
      };


    render() {
        var dumy=this.props.item.created_at.date;
        if (dumy != null) {
            var d = getLocalizedDate(this.props.item.created_at.date)
        }else{
            d=new Date();
        }
        return (
          <View style={{flex:1,}}>
              {this.props.item.client_id != null && this.props.item.agent_id == null &&
              <View style={style.clientContainer}>
                  <View style={style.clientInnerContainer}>
                      <AppText style={style.messageTextStyle}>
                          {this.props.item.message}
                      </AppText>
                      <AppText style={style.clientDateTextStyle}>
                          {d.toDateString()+'  '+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds()}
                      </AppText>
                  </View>

                  <View style={[style.profileContainer ,{marginLeft:5,}]}>
                      {this.props.item.client.picture_path === null &&
                      <Image source={require('../Asset/user.png')}
                             style={style.dummyImageStyle}/>
                      }
                      {this.props.item.client.picture_path !== null &&
                      <Image source={{uri: base_profile_picture_url + this.props.item.client.picture_path}}
                             style={style.profileImageStyle}/>
                      }
                  </View>

              </View>
              }

              {this.props.item.agent_id != null  &&
              <View style={style.agentContainer}>
                  <View style={[style.profileContainer,{marginRight:5,}]}>
                      {this.props.item.agent.picture_path === null &&
                      <Image source={require('../Asset/user.png')}
                             style={style.dummyImageStyle}/>
                      }
                      {this.props.item.agent.picture_path !== null &&
                      <Image source={{uri: base_profile_picture_url + this.props.item.agent.picture_path}}
                             style={style.profileImageStyle}/>
                      }
                  </View>

                  <View style={style.agentInnerContainer}>
                      <AppText style={style.agentMessageText}>
                          {this.props.item.message}
                      </AppText>
                      <AppText style={style.agentDateText}>
                          {d.toDateString()+'  '+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds()}
                      </AppText>
                  </View>
              </View>
              }




          </View>
        );
      }


  }
  const style = StyleSheet.create({
    clientContainer:{
        marginTop:10,
        overflow:'hidden',
        flexDirection:'row',
        justifyContent: 'flex-end',
        alignItems: 'center',

    },
      clientInnerContainer:{
          backgroundColor:Colors.home_widget_background_color,
          padding:10,
          borderWidth:1,
          borderColor:Colors.border_color,
          borderTopLeftRadius:dimen.border_radius,
          borderTopRightRadius:dimen.border_radius,
          borderBottomLeftRadius:dimen.border_radius,
          flexShrink:1,
      },
      messageTextStyle:{
          // alignSelf:'flex-end',
          fontSize: 14,
          color: Colors.normal_text_color,
          marginBottom:7
      },
      clientDateTextStyle:{alignSelf:'flex-end',fontSize:9,color:Colors.gray},

      agentContainer:{marginTop:10,overflow:'hidden',
          flexDirection:'row', justifyContent: 'flex-start',alignItems: 'center'},
      agentInnerContainer:{
          backgroundColor:Colors.home_widget_background_color,
          padding:10,
          borderWidth:1,
          borderColor:Colors.border_color,
          borderTopLeftRadius:dimen.border_radius,
          borderTopRightRadius:dimen.border_radius,
          borderBottomRightRadius:dimen.border_radius,
          flexShrink:1,

      },
      agentMessageText:{
          // alignSelf: 'flex-end',
          fontSize: 14,
          color: Colors.normal_text_color,
          marginBottom:7
      },
      agentDateText:{alignSelf:'flex-start',fontSize:9,color:Colors.gray},

      profileContainer: {
          width: 50,
          height: 50,
          alignSelf: 'center',
          overflow: 'hidden',
          borderRadius: 25,
          borderColor: Colors.border_color,
          backgroundColor:Colors.home_widget_background_color,
          borderWidth: 1,
          marginTop: 0,
      },
      dummyImageStyle: {
          width: 50,
          height: 50,
          borderRadius: 25,
          backgroundColor: Colors.light_border_color,
          borderWidth: 1,
          resizeMode: 'contain',
          alignSelf: 'center'
      },
      profileImageStyle: {
          width: 50,
          height: 50,
          borderRadius: 25,
          borderWidth: 1,
          resizeMode: 'contain',
          alignSelf: 'center'
      },




});

