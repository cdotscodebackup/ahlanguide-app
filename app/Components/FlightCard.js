import React from "react";
import {
    View,
    Text,
    StyleSheet,
    TouchableWithoutFeedback,
    Image,
} from "react-native";
import Colors from "../Styles/Colors";
import dimen from "../Styles/Dimen";
import {base_section_picture_url} from "../Classes/UrlConstant";

export class FlightCard extends React.Component {



    render() {
        const item = this.props.item.flight;

        let fromCountry = item.from_airport == null ? 'N/A' : item.from_airport.code == null ? 'N/A' : item.from_airport.code
        let toCountry = item.to_airport == null ? 'N/A' : item.to_airport.code == null ? 'N/A' : item.to_airport.code
        let fromCity = item.from_airport == null ? 'N/A' : item.from_airport.city == null ? 'N/A' : item.from_airport.city.name == null ? 'N/A' : item.from_airport.city.name;
        let toCity = item.to_airport == null ? 'N/A' : item.to_airport.city == null ? 'N/A' : item.to_airport.city.name == null ? 'N/A' : item.to_airport.city.name;

        let dDateTime= item.departure_datetime.split(' ') ;
        let aDateTime= item.arrival_datetime.split(' ') ;

        let ddate= dDateTime[0]
        let  dtime= dDateTime[1]
        let adate= aDateTime[0]
        let atime= aDateTime[1];

        let airLineName=item.airline.name==null? 'N/A': item.airline.name
        let flight=item.pnr==null? 'N/A': item.pnr
        let terminal=item.terminal==null? 'N/A': item.terminal
        let gate=item.gate==null? 'N/A': item.gate
        let note=item.note==null? 'N/A': item.note

        return (

            <TouchableWithoutFeedback>
                <View style={styles.mainContainer}>
                    <View style={styles.toAirportContainer}>
                        <Text style={styles.countryNameText}>
                            {fromCountry}
                        </Text>
                        <Image style={styles.dotLineImage} source={require('../Asset/line.png')}/>
                        <Image style={styles.planeImage} source={require('../Asset/plane.png')}/>
                        <Image style={styles.dotLineImage} source={require('../Asset/line.png')}/>
                        <Text style={styles.countryNameText}>
                            {toCountry}
                        </Text>
                    </View>

                    <View style={styles.cityContainer}>
                        <Text style={styles.cityText}>{fromCity} </Text>
                        <Text style={styles.cityText}>{toCity} </Text>
                    </View>

                    <View style={styles.departureTimeContainer}>
                        <Text style={styles.timeText}>{dtime}</Text>
                        <Text style={styles.timeText}>{atime}</Text>
                    </View>
                    <View style={styles.departureDateContainer}>
                        <Text style={styles.dateText}>{ddate}</Text>
                        <Text style={styles.dateText}>{adate}</Text>
                    </View>

                    <View style={styles.line}/>

                    <View style={styles.airlineDetailsContainer}>
                        <View style={{flex: 1}}>
                            <Text style={{fontSize: 13, alignSelf: 'flex-start'}}>
                                Flight
                            </Text>
                        </View>
                        <View style={{flex: 1}}>
                            <Text style={{fontSize: 13, alignSelf: 'center'}}>
                                Terminal
                            </Text>
                        </View>
                        <View style={{flex: 1}}>
                            <Text style={{fontSize: 13, alignSelf: 'flex-end'}}>
                                Gate
                            </Text>
                        </View>
                    </View>
                    <View style={[styles.airlineDetailsContainer, {marginTop: 3,}]}>
                        <View style={{flex: 1}}>
                            <Text style={{fontSize: 12, fontFamily: 'roboto-bold', alignSelf: 'flex-start'}}>
                                {flight}
                            </Text>
                        </View>
                        <View style={{flex: 1}}>
                            <Text style={{fontSize: 12, fontFamily: 'roboto-bold', alignSelf: 'center'}}>
                                {terminal}
                            </Text>
                        </View>
                        <View style={{flex: 1}}>
                            <Text style={{fontSize: 12, fontFamily: 'roboto-bold', alignSelf: 'flex-end'}}>
                                {gate}
                            </Text>
                        </View>
                    </View>

                    {note!== 'N/A' &&
                    <View style={styles.cityContainer}>
                        <Text style={styles.noteText}>Note:{" "} {note}  </Text>
                    </View>
                    }

                    {airLineName!== 'N/A' &&
                    <View style={styles.cityContainer}>
                        <Text style={styles.noteText}>Airline:{" "} {airLineName}  </Text>
                    </View>
                    }


                </View>
            </TouchableWithoutFeedback>
        );
    }


}


const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: Colors.home_widget_background_color, borderWidth: 1,
        borderColor: Colors.border_color, padding: dimen.app_padding,
        marginLeft: dimen.app_padding, marginRight: dimen.app_padding, marginBottom: dimen.app_padding,
        borderRadius: dimen.border_radius, flexDirection: 'column', flex: 1,
    },
    toAirportContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start'
    },
    cityContainer: {
        flex: 1,
        marginTop: 3,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start'
    },
    departureTimeContainer: {
        flex: 1,
        marginTop: 8,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start'
    },
    departureDateContainer: {
        flex: 1,
        marginTop: 4,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start'
    },
    line: {
        borderBottomWidth: 1, borderBottomColor: Colors.border_color,
        marginTop: dimen.app_padding, marginBottom: dimen.app_padding
    },
    airlineDetailsContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'flex-start'
    },

    countryNameText: {fontSize: 28},
    dotLineImage: {width: 45, height: 3, alignSelf: 'center'},
    planeImage: {width: 39, height: 39, alignSelf: 'center'},
    cityText: {fontSize: 13},
    noteText: {fontSize: 10},
    timeText: {fontSize: 13},
    dateText: {fontSize: 14, fontFamily: 'roboto-bold',},


})




