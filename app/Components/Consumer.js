import React from "react";

const initialState = {
    notification_value: ""
};

export const MyContext = React.createContext(initialState);


export const Provider = MyContext.Provider;
export const Consumer = MyContext.Consumer;