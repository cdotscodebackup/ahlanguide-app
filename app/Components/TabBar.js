import * as React from "react"
import {
    View,
    StyleSheet, AsyncStorage
} from "react-native"
import Tab from "./Tab"
import Colors from "../Styles/Colors";
import dimen from "../Styles/Dimen";
import {trip_key} from "../Classes/UrlConstant";

const S = StyleSheet.create({
    container: {
        flexDirection: "row",
        borderColor: Colors.normal_text_color,
        borderWidth: 1,
        position: 'absolute',
        bottom: 0,
        width: "100%",
        height: dimen.bottom_tab_height,
        elevation: 20,
        backgroundColor: Colors.home_widget_background_color,
        // backgroundColor: 'red',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        justifyContent: 'space-evenly',
        alignItems: 'stretch'
    },

    tabButton: {flex: 1, justifyContent: "center", alignItems: "center", backgroundColor: 'red'}
});

// rgba(52, 52, 52, 0.8)


export default class TabBar extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {

        const {
            renderIcon,
            getLabelText,
            activeTintColor,
            inactiveTintColor,
            onTabPress,
            onTabLongPress,
            getAccessibilityLabel,
            navigation,
        } = this.props;
        const {routes, index: activeRouteIndex} = navigation.state;

        return (
            <View style={[S.container]}>

                {routes.map((route, routeIndex) => {
                    const isRouteActive = routeIndex === activeRouteIndex;
                    const tintColor = isRouteActive ? activeTintColor : inactiveTintColor;
                    return (

                        <Tab key={routeIndex}
                             id={routeIndex}
                             onPress={() => onTabPress({route})}
                             accessibilityLable={getAccessibilityLabel({route})}
                             focused={isRouteActive}
                             // currentid={this.idd}
                        />

                    );
                })}
            </View>
        );

    }


}

const TabBar1 = (props) => {
    // const { navigation, position } = props
    const {
        renderIcon,
        getLabelText,
        activeTintColor,
        inactiveTintColor,
        onTabPress,
        onTabLongPress,
        getAccessibilityLabel,
        navigation,
    } = props;
    const {routes, index: activeRouteIndex} = navigation.state;
    // console.log(props)


    return (
        <View style={[S.container]}>

            {routes.map((route, routeIndex) => {
                const isRouteActive = routeIndex === activeRouteIndex;
                const tintColor = isRouteActive ? activeTintColor : inactiveTintColor;
                // {console.log(routeIndex)}

                return (

                    <Tab key={routeIndex}
                         id={routeIndex}
                         onPress={() => onTabPress({route})}
                         accessibilityLable={getAccessibilityLabel({route})}
                         focused={isRouteActive}
                         // currentid={'16'}
                    />

                );
            })}
        </View>
    );
};

// export default TabBar


//
// <TouchableWithoutFeedback
// key={routeIndex}
// // style={S.tabButton}
// onPress={() => {
//     onTabPress({ route });
// }}
// // onLongPress={() => {onTabLongPress({ route });}}
//
// accessibilityLabel={getAccessibilityLabel({ route })}
//     >
//     {/*{renderIcon({ route, focused: isRouteActive, tintColor })}*/}
// <Tab key={routeIndex}
//      onPress={()=>onTabPress({route})}
//      accessibilityLable={getAccessibilityLabel({route})}
//
// />
// {/*<Text>{getLabelText({ route })}</Text>*/}
// </TouchableWithoutFeedback>
