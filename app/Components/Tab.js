import * as React from "react"
import {AsyncStorage, Image, StyleSheet, TouchableOpacity, View} from "react-native"
import {MyContext} from "./Consumer";
import {getSavedObject} from "../Classes/auth";
import {trip_key} from "../Classes/UrlConstant";


export default class Tab extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            currentid: ''
        }
    }

    static contextType = MyContext;

    getTripId() {
        AsyncStorage.getItem(trip_key)
            .then(res => {
                var jsondata = JSON.parse(res)
                if (jsondata) {
                    this.setState({currentid: jsondata.trip_id})
                }
                else
                    this.setState({currentid: 0})
            })
            .catch(err => {
                // console.log(err)
            });
    }

    async componentDidMount() {
        this.idd = await this.getTripId()
    }

    componentDidUpdate() {
        // console.log(this.context);
    }


    render() {
        return (
            <TouchableOpacity style={[styles.tabcontainer, {flexGrow: 1}]} onPress={this.props.onPress}>
                <View style={{}}>
                    {(this.props.id == 0) &&
                    <View>
                        <Image
                            source={this.props.focused ? require('../Asset/home_active.png') : require('../Asset/home_rest.png')}
                            style={styles.tabiconstyle}/>

                        {this.context.val_alert === 'emergency_alert' && this.state.currentid == this.context.val_trip_id &&
                        <View style={[styles.newMessageIcon]}/>
                        }
                    </View>
                    }
                    {(this.props.id == 1) &&
                    <Image
                        source={this.props.focused ? require('../Asset/guide.png') : require('../Asset/guide_rest.png')}
                        style={styles.tabiconstyle}/>
                    }
                    {(this.props.id == 2) &&
                    <Image
                        source={this.props.focused ? require('../Asset/hotel_active.png') : require('../Asset/hotel_rest.png')}
                        style={styles.tabiconstyle}/>
                    }
                    {(this.props.id == 3) &&
                    <Image
                        source={this.props.focused ? require('../Asset/flight_active.png') : require('../Asset/flight_rest.png')}
                        style={styles.tabiconstyle}/>
                    }
                    {(this.props.id == 4) &&
                    <Image
                        source={this.props.focused ? require('../Asset/alert_active.png') : require('../Asset/alert_rest.png')}
                        style={styles.tabiconstyle}/>
                    }
                    {(this.props.id == 5) &&
                    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', flexGrow: 1}}>
                        <View>
                            <Image
                                source={this.props.focused ? require('../Asset/message_active.png') : require('../Asset/message_rest.png')}
                                style={styles.tabiconstyle}/>
                            {this.context.val_message === 'message' && this.state.currentid == this.context.val_trip_id &&
                            <View style={[styles.newMessageIcon]}/>
                            }
                        </View>
                    </View>
                    }
                    {(this.props.id == 6) &&
                    <Image
                        source={this.props.focused ? require('../Asset/setting_active.png') : require('../Asset/setting_rest.png')}
                        style={styles.tabiconstyle}/>
                    }


                </View>

            </TouchableOpacity>

        );
    }


}

// const Tab = ({id, onPress, focused}) => {
//     // console.log(onPress)
//     const contextType = MyContext;
//
//
//     return (
//         <TouchableOpacity onPress={onPress}>
//
//             <View style={styles.tabcontainer}>
//                 {(id == 0) &&
//                 <View>
//                     <Image
//                         source={focused ? require('../Asset/message_active.png') : require('../Asset/message_rest.png')}
//                         style={styles.tabiconstyle}/>
//                     {this.context.val =='message' &&
//                     <Image
//                         source={require('../Asset/home_rest.png')}
//                         style={[styles.newMessageIcon]}/>}
//                 </View>
//
//                 }
//                 {(id != 0) &&
//                 <Image
//                     source={focused ? require('../Asset/home_active.png') : require('../Asset/home_rest.png')}
//                     style={styles.tabiconstyle}/>
//                 }
//                 {/*{this.state.inputValue == 'message' &&*/}
//                 {/*< Image*/}
//                 {/*source={require('./app/Asset/message_rest.png')}*/}
//                 {/*style={[styles.newMessageIcon]}/>*/}
//                 {/*}*/}
//             </View>
//
//         </TouchableOpacity>
//     )
// }

// const Tab = ({id, onPress, focused}) => {
//     // console.log(onPress)
//     const contextType = MyContext;
//     return (
//         <TouchableOpacity onPress={onPress}>
//
//             <View style={styles.tabcontainer}>
//                 {(id == 0) &&
//                 <View>
//                     <Image
//                         source={focused ? require('../Asset/message_active.png') : require('../Asset/message_rest.png')}
//                         style={styles.tabiconstyle}/>
//                     {this.context.val =='message' &&
//                     <Image
//                         source={require('../Asset/home_rest.png')}
//                         style={[styles.newMessageIcon]}/>}
//                 </View>
//
//                 }
//                 {(id != 0) &&
//                 <Image
//                     source={focused ? require('../Asset/home_active.png') : require('../Asset/home_rest.png')}
//                     style={styles.tabiconstyle}/>
//                 }
//                 {/*{this.state.inputValue == 'message' &&*/}
//                 {/*< Image*/}
//                 {/*source={require('./app/Asset/message_rest.png')}*/}
//                 {/*style={[styles.newMessageIcon]}/>*/}
//                 {/*}*/}
//             </View>
//
//         </TouchableOpacity>
//     )
// }

// export default Tab
const styles = StyleSheet.create({
    tabiconstyle: {
        width: 25,
        height: 25,
        padding: 2,
        // flex: 1,flexGrow:1,
        resizeMode: 'contain'
    },
    newMessageIcon: {
        width: 6,
        height: 6,
        // padding: 1,
        borderRadius: 3,
        position: 'absolute',
        resizeMode: 'contain',
        backgroundColor: 'red',
        top: 0,
        right: 0,
        // left: 0,
        // bottom: 0,
    },
    tabcontainer: {
        flex: 1, justifyContent: 'center', alignItems: 'center', flexGrow: 1,
    }

});


