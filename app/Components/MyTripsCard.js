import React from "react";
import {
    View,
    Text,
    Button,
    StyleSheet,
    TouchableOpacity,
    TouchableHighlight,
    TouchableWithoutFeedback,
    Image, Dimensions
} from "react-native";
import {Card} from "react-native-elements"
import Colors from "../Styles/Colors";
import HomeStyles from "../Styles/HomeStyles";
import dimen from "../Styles/Dimen";
import AppText from "./AppText";
import TextStyles from "../Styles/TextStyles";
import {MyContext} from "./Consumer";
import {base_picture_url, base_profile_picture_url} from "../Classes/UrlConstant";
import {simpleAlert} from "../Classes/auth";


const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export class MyTripsCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hidenfield: false,
        }
    }

    static contextType = MyContext;

    _onPress = () => {
        this.props.onPressItem(this.props.item);
    };
    imagePress = () => {
        if (this.props.item.trip.path!==null){
            console.log(this.props.item.trip.path)
            this.props.imagePress(base_profile_picture_url + this.props.item.trip.path);
        }
    };

    render() {
        let trip = this.props.item;
        let end_date = new Date(trip.trip.end_date);
        let date = new Date();
        let isDate = trip.trip.dates_check;
        return (
            <View >
                <View
                    style={[style.mainContainer, date > end_date ? {backgroundColor: Colors.expiredTripBackground,} : {backgroundColor: Colors.home_widget_background_color}]}>
                    <TouchableOpacity onPress={this.imagePress} style={style.imageContainer}>
                        <Image style={style.imageStyle}
                               source={trip.trip.path === null ? require('../Asset/dumy_img.png') : {
                                   uri: base_profile_picture_url + trip.trip.path
                               }}
                        />
                    </TouchableOpacity>
                    <View style={style.detailContainer}>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-start'}}>
                            <View style={{width: '100%'}}>
                                <AppText style={style.tripTitleStyle}
                                         numberOfLines={1}>
                                    {trip.trip.title}
                                </AppText>
                            </View>
                        </View>
                        {isDate !== 1 &&
                        <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-start'}}>
                            <AppText style={{fontSize: 9}} numberOfLines={1}>
                                Start: {trip.trip.start_date}
                            </AppText>
                            <AppText style={{fontSize: 9}} numberOfLines={1}>
                                End: {trip.trip.end_date}
                            </AppText>
                        </View>}
                        <AppText style={{marginTop: 5, fontSize: 11, color: Colors.normal_text_color}}
                                 numberOfLines={4}>
                            {trip.trip.description}
                        </AppText>
                        <TouchableOpacity onPress={this._onPress} style={style.enterTripButton}>
                            <View style={TextStyles.smallButtonView}>
                                <AppText style={TextStyles.smallButtonText}>
                                    Enter Trip
                                </AppText>
                            </View>
                        </TouchableOpacity>
                    </View>

                    {(this.context.val_message === 'message' || this.context.val_alert === 'emergency_alert') &&
                    this.props.item.trip_id == this.context.val_trip_id &&
                    <View style={[style.newMessageIcon]}/>
                    }
                </View>


            </View>
        );
    }


}

const style = StyleSheet.create({
    tripTitleStyle: {fontSize: 15, fontWeight: 'bold', color: Colors.primary_color},
    wraper: {
        backgroundColor: '#F00303',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        color: '#FFFFFF',
        fontSize: 35,
        fontWeight: 'bold'

    },
    subtitle: {
        color: 'white',
        fontWeight: '200'
    },
    newMessageIcon: {
        width: 8,
        height: 8,
        // padding: 1,
        borderRadius: 4,
        position: 'absolute',
        resizeMode: 'contain',
        backgroundColor: 'red',
        top: 5,
        right: 5,
    },
    mainContainer: {
        marginBottom: 10,
        borderWidth: 1,
        borderColor: Colors.border_color,
        borderRadius: 10,
        flexDirection: 'row',
        flex: 1,

    },
    imageContainer: {
        height: (windowWidth / 3) + (windowWidth / 9),
        width: windowWidth / 3.5,
        borderRadius: 10,
        overflow: 'hidden',
        // backgroundColor: 'red',
    },
    imageStyle: {
        flex: 1,
        resizeMode: 'cover',
    },
    detailContainer: {
        // width: '70%',
        flex: 1,
        padding: 10,
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'stretch'
    },
    enterTripButton: {
        marginTop: 5,
        overflow: 'hidden',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-start',
    },

});
