import React from "react";
import {
    View,
    Text,
    Button,
    StyleSheet,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Image,
    AsyncStorage,
} from "react-native";
import Colors from "../Styles/Colors";
import dimen from "../Styles/Dimen";
import AppText from "./AppText";
import {MyContext} from "./Consumer";
import {trip_key} from "../Classes/UrlConstant";
import TextStyles from "../Styles/TextStyles";

export class HomeHeaderQiblaMain extends React.Component {
    _onPress = () => {
        this.props.onPressItem(this.props.item);
    };

    render() {
        return (
            <TouchableOpacity style={styles.qiblaMainContainer} onPress={this._onPress}>
                <View style={styles.qiblaImageContainer}>
                    <Image style={styles.qiblaImageStyle}
                           source={require("../Asset/qibla.png")}>
                    </Image>
                </View>
            </TouchableOpacity>
        );
    }
}

export class HomeHeaderSetting extends React.Component {
    _onPress = () => {
        this.props.onPressItem(this.props.item);
    };

    render() {
        return (
            <TouchableOpacity style={{flex: 1, elevation: 5,}}
                              onPress={this._onPress}>
                <View style={{
                    justifyContent: "flex-end", flexDirection: 'row',
                    alignItems: "center",
                    margin: 0, padding: 0,
                }}>
                    {/*<AppText>17 July, 2019</AppText>*/}
                    <Image style={{height: 30, width: 30, margin: 10, padding: 0,}}
                           source={require("../Asset/setting_dark.png")}>

                    </Image>
                </View>

            </TouchableOpacity>
        );
    }
}
export class HomeHeaderReload extends React.Component {
    _onPress = () => {
        this.props.onPressItem(this.props.item);
    };

    render() {
        return (
            <TouchableOpacity style={{flex: 1, elevation: 5,}}
                              onPress={this._onPress}>
                <View style={{
                    justifyContent: "flex-end", flexDirection: 'row',
                    alignItems: "center",
                    margin: 0, padding: 0,
                }}>
                    {/*<AppText>17 July, 2019</AppText>*/}
                    <Image style={{height: 30, width: 30, margin: 10, padding: 0,}}
                           source={require("../Asset/reload.png")}>

                    </Image>
                </View>

            </TouchableOpacity>
        );
    }
}

export class HomeHeaderSignout extends React.Component {
    _onPress = () => {
        this.props.onPressItem();
    };

    render() {
        return (
            <TouchableOpacity style={{flex: 1, elevation: 5,}}
                              onPress={this._onPress}>
                <View style={{
                    justifyContent: "flex-end", flexDirection: 'row',
                    alignItems: "center",
                    margin: 0, padding: 0,
                }}>
                    <AppText>
                        Sign Out
                    </AppText>
                    <Image style={{height: 18, width: 18, marginRight: dimen.app_padding, padding: 0,}}
                           source={require("../Asset/signout.png")}>
                    </Image>
                </View>
            </TouchableOpacity>
        );
    }


}

export default class BackHeaderButton extends React.Component {
    _onPress = () => {
        this.props.onPressItem(this.props.item);
    };

    render() {
        return (
            <TouchableOpacity style={{flex: 1, elevation: 5,}}
                              onPress={this._onPress}>
                <View style={{
                    justifyContent: "center",
                    alignItems: "center",
                    margin: 0, padding: 0,
                }}>
                    <Image style={{
                        height: 20, width: 20,
                        margin: dimen.app_padding, resizeMode: 'contain'
                    }}
                           source={require("../Asset/back_arrow.png")}>

                    </Image>
                </View>
            </TouchableOpacity>
        );
    }
}

export  class BackHeaderButtonAdmin extends React.Component {
    _onPress = () => {
        this.props.onPressItem();
    };

    render() {
        return (
            <TouchableOpacity style={{flex: 1, elevation: 5,}}
                              onPress={this._onPress}>
                <View style={{
                    justifyContent: "center",
                    alignItems: "center",
                    margin: 0, padding: 0,
                }}>
                    <Image style={{
                        height: 20, width: 20,
                        margin: dimen.app_padding, resizeMode: 'contain'
                    }}
                           source={require("../Asset/back_arrow.png")}>

                    </Image>
                </View>
            </TouchableOpacity>
        );
    }
}

export class AllTripsHeaderButton extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            currentid: '',
            shouldshowdot: false
        }
    }

    static contextType = MyContext;

    getTripId() {
        AsyncStorage.getItem(trip_key)
            .then(res => {
                var jsondata = JSON.parse(res)
                if (jsondata) {
                    this.setState({
                        currentid: jsondata.trip_id,
                        shouldshowdot: jsondata.trip_id != this.context.val_trip_id
                    })
                }
                else
                    this.setState({currentid: 0})
            })
            .catch(err => {
            });
    }

    async componentDidMount() {
        this.idd = await this.getTripId()
    }

    _onPress = () => {
        this.props.onPressItem(this.props.item);
    };

    render() {

        // console.log(this.context)
        return (
            <TouchableOpacity style={styles.allTripHeaderMainContainer} onPress={this._onPress}>
                <View style={styles.allTripHeaderContainer}>
                    <Image style={styles.allTripsImage} source={require("../Asset/all_trips.png")}/>

                    {this.state.currentid != this.context.val_trip_id && this.state.shouldshowdot &&
                    (this.context.val_message === 'message' || this.context.val_alert === 'emergency_alert') &&

                    <View style={[TextStyles.allTripsHeaderDot]}/>
                    }
                </View>
            </TouchableOpacity>
        );
    }


}

export class LogoTitle extends React.Component {
    render() {
        return (
            <Image
                source={require('../Asset/header_logo.png')}
                style={{height: '95%', resizeMode: 'contain', justifyContent: 'center', alignSelf: 'center'}}
            />
        );
    }
}

const styles = StyleSheet.create({
    qiblaMainContainer: {flex: 1, elevation: 5,},
    qiblaImageContainer: {
        justifyContent: "flex-end", flexDirection: 'row',
        alignItems: "center",
        margin: 0, padding: 0,
    },
    qiblaImageStyle: {height: 30, width: 30, margin: 10, padding: 0,},
    allTripHeaderContainer: {
        justifyContent: "center",
        alignItems: "center",
        margin: 0, padding: 0,
    },
    allTripsImage: {
        height: 35, width: 55,
        margin: dimen.app_padding, resizeMode: 'contain'
    },
    allTripHeaderMainContainer: {flex: 1, elevation: 5,},


})


