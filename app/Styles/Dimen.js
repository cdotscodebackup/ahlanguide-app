
const dimen={
    small_padding:10,
    app_padding:15,
    buttonfont:15,
    textinputfontsize:14, //50

    bottom_user_padding:30,
    bottom_margin_for_bottom_menu:56,
    bottom_tab_height:56,
    border_radius:10,

}

export default dimen;