const Colors = {

    primary_color: '#eca112',
    accent_color: '#0ebebe',
    primary_dark_color: '#9e0232',

    normal_text_color: '#363636',
    heading_text_color: '#231f20',
    button_text_color: '#231f20',

    active_arrow_color:'#231f20',
    rest_arrow_color:'#acacac',

    border_color: '#d6d6d6',
    homelighbackground: '#eceff8',
    expiredTripBackground: '#D6D6D6',

    home_widget_background_color: '#ffffff',
    home_background_color: '#f8f8f8',

    header_text_color: '#ffffff',
    light_background_color: '#e6e8e8',
    light_border_color: '#e5e5e5',
    text_prominent_color: '#25437d',
    tabbar_background_color: '#333333',

    black: '#000000',
    night: '#333333',
    charcoal: '#474747',
    gray: '#7D7D7D',
    lightishgray: '#9D9D9D',
    lightgray: '#D6D6D6',
    smoke: '#EEEEEE',
    white: '#FFFFFF',
    ypsDark: '#47546E',
    yps: '#637599',
    ypsLight: '#7B92BF',
    cosmic: '#963D32'

};
export default Colors;
