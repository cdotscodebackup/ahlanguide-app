import React from "react";
import {
    Alert,
    Image,
    NetInfo,
    RefreshControl,
    Text,
    TouchableOpacity, StyleSheet,
    TouchableWithoutFeedback,
    TouchableNativeFeedback,
    View, Dimensions
} from "react-native";
import Session, {SessionKeys} from '../Classes/Session';
import {FlatList, ScrollView} from "react-native-gesture-handler";
import dimen from "../Styles/Dimen";
import AppText from "../Components/AppText";
import Colors from "../Styles/Colors";
import {TripStepCard} from "../Components/TripStepCard";
import {HomeHeaderQiblaMain, HomeHeaderSetting, AllTripsHeaderButton, LogoTitle} from "../Components/HeaderComponents";
import {
    errorAlert,
    getData,
    getSavedObject,
    getUserObject,
    isNetConnected,
    removeMainObject,
    saveMainObject
} from "../Classes/auth";
import {base_video_url, internetError, notification_key, single_trip_url, trip_key} from "../Classes/UrlConstant";
import {Video} from 'expo-av';
import VideoPlayer from 'expo-video-player'
import {TripMessages} from "../Components/TripMessages";
import {Consumer, MyContext} from "../Components/Consumer";
import {MyTripsCard} from "../Components/MyTripsCard";


const scrollWidth = Dimensions.get('window').width - (dimen.app_padding * 6);
const videoWidth = Dimensions.get('window').width - (dimen.app_padding * 2);

export class TripDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isSplash: true,
            isqibla: false,
            isInternetConnected: true,
            tripsteps: [],
            tripsection: [],
            trip_updates: [],
            trip_current_update: 0,
            tripdata: {},
            userdata: {},
            mute: false,
            shouldPlay: false,

            showvideo: true,

            isLoading: false,

            stepIndex: 0,
            currentStepIndexx: 0,
            rightActive: false,
            leftActive: false,

            notification: {},
            trip_details_component: false,
            agencyName: '',
            agencyMessage: '',
            tripEndDate: '',
            videoPath: '',
            stepTitle: '',
            noStepError: '',

            hideBottomBar: false


        };
        this.currentStepIndex = 0;
        this.playbackObject = null;
    }

    static navigationOptions = ({navigation}) => {
        return {

            headerLeftContainerStyle: {},
            // headerTitle: <LogoTitle/>,

            headerStyle: {
                backgroundColor: Colors.primary_color,
            },
            headerTitleStyle: {
                justifyContent: 'center',
                alignItems: 'center',
            },
            headerTitleContainerStyle: {
                left: 120,
                right: 120,
                justifyContent: 'center',
                alignItems: 'center'
            },

            headerRight: (navigation.getParam('isqibla') ?
                    <HomeHeaderQiblaMain onPressItem={() => navigation.navigate('QiblaDirection')}/> : null
            ),
            // headerRight: (
            //     <HomeHeaderRefresh onPressItem={navigation.getParam('refreshTripData')}/>
            // ),


            //hide backtitle and show custom button
            headerBackTitleVisible: false,

            headerLeft: (
                <AllTripsHeaderButton onPressItem={() => navigation.navigate('AllTripsStack')}/>
            ),
        }
    };

    static contextType = MyContext;

    componentWillMount() {
        //set navigation param for showing header item or hiding
        this.props.navigation.setParams({
            isqibla: false,
            // refreshTripData: this.refreshTripData
        });

        this.loadTripData()
    }

    componentDidMount() {
        getSavedObject(notification_key)
            .then((notification) => {
                if (!notification) {
                }
            })
            .catch((err) => {
                console.log("error: " + err)
            })
    }

    componentDidUpdate() {
        if (this.context.val_trip_id == this.state.tripdata.trip_id && this.context.val_alert === 'emergency_alert') {
            this.refreshTripData()
            this.context.val_alert = '';
            removeMainObject(notification_key);
        }
    }

    loadTripData() {
        getSavedObject(trip_key)
            .then((data) => {
                this.updateState(data)
            }).catch((err) => {
            console.log('error getting trip object' + err);
        })
        getUserObject().then((user) => {
            this.setState({
                userdata: JSON.parse(user),
            })
        })
    }

    updateState = (data) => {

        let jsondata = JSON.parse(data)
        // console.log(jsondata.trip);
        // console.log(jsondata.current_step.step.sort);
        let tripsteps = []
        if (jsondata.current_step !== null && jsondata.current_step.step !== null && jsondata.current_step.step.step_section !== null) {
            tripsteps = jsondata.current_step.step.step_section;
            if (jsondata.current_step.step.sort === 'asc') {
                // tripsteps=tripsteps;
            } else if (jsondata.current_step.step.sort === 'desc') {
                tripsteps = tripsteps.reverse();
            }
        } else {
            tripsteps = [];
        }

        console.log(jsondata.current_step)


        this.setState({
            agencyMessage: 'We are with you during your journey',
            noStepError: 'You are not assigned to any step yet. Please contact your agent.',
            tripdata: jsondata,
            // tripsteps: jsondata.current_step == null ? [] : jsondata.current_step.step == null ? [] : jsondata.current_step.step.step_section == null ? [] : jsondata.current_step.step.step_section,
            tripsteps: tripsteps,
            videoPath: jsondata.current_step == null ? '' : jsondata.current_step.step == null ? '' : jsondata.current_step.step.last_video_path == null ? '' : jsondata.current_step.step.last_video_path,
            stepTitle: jsondata.current_step == null ? '' : jsondata.current_step.step == null ? '' : jsondata.current_step.step.title == null ? '' : jsondata.current_step.step.title,
            agencyName: jsondata.trip.travel_agency.agency_name,
            tripEndDate: jsondata.trip.end_date,
            trip_updates: jsondata.push_notification == null ? [] : jsondata.push_notification,
            rightActive: jsondata.push_notification !== null && jsondata.push_notification.length > 0,
            hideBottomBar: jsondata.trip.app_menu === 1,

        }, function () {

        })

        this.props.navigation.setParams({
            isqibla: jsondata.trip.is_qibla_widget_enable == "1" ? true : false,
        });

        let bhide = jsondata.trip.app_menu === 1;
        saveMainObject('hideBottomTab', bhide.toString())
            .then(res => {
            })
            .catch(err => console.log(err))
    }
    refreshTripData = () => {
        isNetConnected()
            .then(() => {
                this.setState({isLoading: true})
                getData(single_trip_url + this.state.tripdata.trip_id, 'GET', '')
                    .then((res) => {
                        // console.log("current trip: " +JSON.stringify(res))
                        this.setState({isLoading: false})
                        this.updateState(JSON.stringify(res))
                        saveMainObject(trip_key, JSON.stringify(res))
                            .then((res) => {
                            })
                            .catch((res) => {
                                console.log("error saving data: " + res)
                            })

                        if (this.context.val_trip_id === this.state.tripdata.trip_id) {
                            this.context.val_alert = '';
                            removeMainObject(notification_key);
                        }
                    })
                    .catch(err => {
                        this.setState({isLoading: false})
                        console.log("error fetching data : " + err)
                        // reject(false)
                    });

            })
            .catch(() => {
                errorAlert(internetError)
                this.setState({isLoading: false})

            })
        this.loadTripData()
    }

    checklist(checklist) {
        this.props.navigation.navigate('CheckList', {'checklist': checklist});
    };

    ImportantInformation(item) {
        this.props.navigation.navigate('ImportantInformation', {'importantInfo': item});
    };

    _nextMessage() {
        // console.log('next tapped...')
        if (this.state.currentStepIndexx < this.state.trip_updates.length - 1) {

            this.setState({currentStepIndexx: this.state.currentStepIndexx + 1}, function () {
                this.flatListRef.scrollToIndex({index: this.state.currentStepIndexx, animated: true});

            })
            this.setState(
                {stepIndex: this.currentStepIndex,}, function () {
                    this._updateArrows()
                }
            )
        }
    }

    _previousMessage() {
        // console.log('next tapped...')
        if (this.state.currentStepIndexx > 0) {

            this.setState({currentStepIndexx: this.state.currentStepIndexx - 1}, function () {
                this.flatListRef.scrollToIndex({index: this.state.currentStepIndexx, animated: true});
            })

            this.setState(
                {stepIndex: this.currentStepIndex,}, function () {
                    this._updateArrows()
                }
            )
        }
    }

    _updateArrows = () => {
        if (this.state.stepIndex > 0)
            this.setState({leftActive: true})
        else
            this.setState({leftActive: false})

        if (this.state.stepIndex < this.state.trip_updates.length - 1)
            this.setState({rightActive: true})
        else
            this.setState({rightActive: false})
    }

    updateArrowsOnScroll = (event) => {
        let scrolloffset = event.nativeEvent.contentOffset.x;
        let stepIndexx = Math.floor((scrolloffset + 10) / scrollWidth);
        // console.log('scroll updates: ' + event.nativeEvent.contentOffset.x + '   ' + scrollWidth + '   ' + stepIndexx);

        this.currentStepIndex = stepIndexx
        this.setState({stepIndex: stepIndexx}, function () {
            this._updateArrows()
        })

        // console.log("update arrow" + this.state.stepIndex)
        // if (this.state.stepIndex > 0)
        //     this.setState({leftActive: true})
        // else
        //     this.setState({leftActive: false})
        // if (this.state.stepIndex < this.state.trip_updates.length - 1)
        //     this.setState({rightActive: true})
        // else
        //     this.setState({rightActive: false})

    }


    rightArrowPress = () => {

        if (this.state.trip_current_update < (this.state.trip_updates.length - 1)) {
            this.setState({trip_current_update: this.state.trip_current_update + 1})
        }

    }
    leftArrowPress = () => {
        if (this.state.trip_current_update > 0) {
            this.setState({trip_current_update: this.state.trip_current_update - 1})
        }
    }


    render() {
        let trip_date = this.state.tripdata.start_date != null ? this.state.tripdata.start_date : '';
        let username = this.state.userdata.name !== null ? this.state.userdata.name : "";
        // let agencyName ='agency name' ;//this.state.userdata.name !== null ? this.state.userdata.name : "";
        // agencyName= this.state.tripdata.trip.travel_agency.agency_name
        // agencyName= 'Agency Name';
        let end_date = new Date(this.state.tripEndDate);
        let date = new Date();

        return (
            <View style={styles.mainContainer}>
                <Image source={require('../Asset/bg.png')}
                       style={styles.backgroundImage}/>
                <ScrollView
                    style={[styles.scrollViewStyle, this.state.hideBottomBar ? {marginBottom: 0} : {marginBottom: 60}]}
                    contentContainerStyle={styles.scrollViewContainerStyle}
                    bounces={true}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.isLoading}
                            onRefresh={this.refreshTripData}
                        />
                    }>
                    <View style={{flex: 1}}>
                        <View style={{marginBottom: dimen.app_padding}}>
                            <View>
                                <AppText style={styles.ahlanText}>
                                    {this.state.agencyName}
                                </AppText>
                                <View style={styles.ahlanDescText}>
                                    <AppText>{this.state.agencyMessage}</AppText>
                                    <AppText>{trip_date}</AppText>
                                </View>
                            </View>
                        </View>

                        <View>
                        {/*this is previous implementation of trip udpates*/}
                        {/*{this.state.trip_updates.length > 0 &&*/}
                        {/*<View style={styles.tripUpdateContainer}>*/}

                        {/*    <View style={styles.leftArrowContainer}>*/}
                        {/*        {this.state.leftActive &&*/}
                        {/*        <TouchableWithoutFeedback style={styles.leftArrowContainer}*/}
                        {/*                                  onPress={this._previousMessage.bind(this)}>*/}
                        {/*            <Image source={require('../Asset/arrow_left.png')}*/}
                        {/*                   style={styles.leftArrowImageStyle}/>*/}
                        {/*        </TouchableWithoutFeedback>*/}
                        {/*        }*/}
                        {/*        {!this.state.leftActive &&*/}
                        {/*        <Image source={require('../Asset/arrow_left_rest.png')}*/}
                        {/*               style={styles.leftArrowImageStyle}/>*/}
                        {/*        }*/}
                        {/*    </View>*/}

                        {/*    <FlatList*/}
                        {/*        ref={(ref) => {*/}
                        {/*            this.flatListRef = ref;*/}
                        {/*        }}*/}
                        {/*        initialNumToRender={1}*/}
                        {/*        initialScrollIndex={0}*/}
                        {/*        refreshing={false}*/}
                        {/*        pagingEnabled={true}*/}

                        {/*        scrollEventThrottle={16}*/}
                        {/*        onScroll={this.updateArrowsOnScroll}*/}

                        {/*        data={this.state.trip_updates}*/}
                        {/*        keyExtractor={(item, index) => item.id.toString()}*/}
                        {/*        numColumns={1}*/}
                        {/*        bounces={false}*/}
                        {/*        horizontal={true}*/}
                        {/*        scrollEnabled={false}*/}
                        {/*        showsHorizontalScrollIndicator={false}*/}
                        {/*        renderItem={({item}) => (<TripMessages*/}
                        {/*            // onPressCheckList={this.checklist.bind(this)}*/}
                        {/*            // onPressImportantInformation={this.ImportantInformation.bind(this)}*/}
                        {/*            item={item}/>)}*/}
                        {/*    />*/}

                        {/*    {this.state.rightActive &&*/}
                        {/*    <TouchableOpacity*/}
                        {/*        style={styles.leftArrowContainer}*/}
                        {/*        onPress={this._nextMessage.bind(this)}>*/}
                        {/*        <Image source={require('../Asset/arrow_right.png')}*/}
                        {/*               style={styles.rightArrowImageStyle}/>*/}
                        {/*    </TouchableOpacity>*/}
                        {/*    }*/}
                        {/*    {!this.state.rightActive &&*/}
                        {/*    <Image source={require('../Asset/arrow_right_rest.png')}*/}
                        {/*           style={styles.rightArrowImageStyle}/>*/}
                        {/*    }*/}
                        {/*</View>*/}
                        {/*}*/}

                        </View>

                        {this.state.trip_updates.length > 0 &&
                        <View style={styles.tripUpdateContainer}>

                            <TouchableOpacity style={styles.leftArrowContainer}
                                                      onPress={this.leftArrowPress.bind(this)}>
                                <Image source={require('../Asset/arrow_left.png')}
                                       style={[styles.leftArrowImageStyle,
                                           this.state.trip_current_update > 0 ? {tintColor: Colors.active_arrow_color} : {tintColor: Colors.rest_arrow_color}
                                       ]}/>
                            </TouchableOpacity>

                            <TripMessages item={this.state.trip_updates[this.state.trip_current_update]}/>

                            <TouchableOpacity style={styles.leftArrowContainer}
                                              onPress={this.rightArrowPress.bind(this)}>
                                <Image source={require('../Asset/arrow_right.png')}
                                       style={[styles.rightArrowImageStyle,
                                           this.state.trip_current_update < (this.state.trip_updates.length - 1) ?
                                               {tintColor: Colors.active_arrow_color} : {tintColor: Colors.rest_arrow_color}
                                       ]}/>
                            </TouchableOpacity>

                        </View>
                        }


                        {this.state.tripsteps.length == 0 &&
                        <AppText style={[styles.expiredTripText, {margin: dimen.app_padding * 2,}]}>
                            {this.state.noStepError}
                        </AppText>
                        }

                        {this.state.tripsteps.length > 0 && date < end_date &&
                        <AppText style={styles.stepTitleText}>
                            {this.state.stepTitle}
                        </AppText>
                        }
                        {date < end_date &&
                        <View style={{marginBottom: 10}}>
                            <FlatList
                                data={this.state.tripsteps}
                                keyExtractor={(item, index) => item.id.toString()}
                                numColumns={1}
                                bounces={false}
                                renderItem={({item}) => (
                                    <TripStepCard
                                        onPressCheckList={this.checklist.bind(this)}
                                        onPressImportantInformation={this.ImportantInformation.bind(this)}
                                        item={item}
                                        imagePress={(path) => this.props.navigation.navigate('ImageScreen', {path: path})}
                                    />)}
                            />
                        </View>
                        }
                        {this.state.videoPath != '' && date < end_date &&
                        <View style={styles.videoContainer}>
                            {this.state.showvideo &&
                            <VideoPlayer
                                videoProps={{
                                    shouldPlay: false,
                                    resizeMode: Video.RESIZE_MODE_CONTAIN,
                                    source: {uri: base_video_url + this.state.videoPath}
                                }}
                                inFullscreen={true}
                                width={videoWidth}
                                height={200}
                                debug={false}
                                showControlsOnLoad={true}
                                errorCallback={(error) => {
                                    this.setState({showvideo: false})
                                    // console.log(error.message);
                                    // console.log(error.type);
                                    // console.log(error.obj);
                                }}
                                switchToLandscape={() => {
                                }}
                            />
                            }
                            {!this.state.showvideo &&
                            <View style={styles.videoErrorContainer}>
                                <AppText style={styles.videoErrorText}>
                                    Video format not supported
                                </AppText>
                            </View>}
                        </View>
                        }

                        {date > end_date &&
                        <AppText style={styles.expiredTripText}>
                            This trip has expired.
                        </AppText>
                        }
                    </View>
                </ScrollView>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    mainContainer: {flex: 1, justifyContent: "flex-start", alignItems: "stretch"},
    backgroundImage: {
        bottom: 0,
        left: 0,
        right: 0,
        top: 0,
        width: '100%',
        height: '100%',
        position: 'absolute',
        resizeMode: 'cover'
    },
    scrollViewStyle: {
        flex: 1,
        // marginBottom: 60
    },
    scrollViewContainerStyle: {
        paddingTop: dimen.app_padding,
        paddingLeft: dimen.app_padding,
        paddingRight: dimen.app_padding,
    },
    ahlanText: {
        fontFamily: 'yellowtail-regular',
        fontSize: 32,
    },
    ahlanDescText: {flexDirection: 'row', justifyContent: 'space-between'},
    stepTitleText: {
        color: Colors.normal_text_color,
        marginTop: dimen.app_padding,
        marginBottom: dimen.app_padding,
        fontFamily: 'roboto-bold',
        fontSize: 17
    },
    videoContainer: {
        height: 200,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: Colors.border_color,
        overflow: 'hidden',
        backgroundColor: Colors.home_widget_background_color,
        marginBottom: dimen.app_padding * 2,
        justifyContent: 'flex-start',
        alignItems: 'stretch'
    },
    videoErrorContainer: {
        height: 200,
        backgroundColor: Colors.home_widget_background_color,
        justifyContent: 'center',
        alignItems: 'center'
    },
    videoErrorText: {fontSize: 16},
    expiredTripText: {
        fontSize: 16,
        marginTop: dimen.app_padding * 2,
        alignSelf: 'center',
        textAlign: 'center',
    },

    tripUpdateContainer: {
        marginBottom: 10,
        paddingTop: dimen.app_padding, paddingBottom: dimen.app_padding,
        borderWidth: 1,
        borderRadius: 10, borderColor: Colors.border_color,
        backgroundColor: Colors.home_widget_background_color,
        flexDirection: 'row', justifyContent: 'space-between'
    },
    leftArrowContainer: {
        justifyContent: 'center',
        alignSelf: 'center',
        height:30
    },
    leftArrowImageStyle: {
        marginLeft: 7.5,
        marginRight: 7.5,
        width: 15,
        height: 15,
        justifyContent: 'center',
        alignSelf: 'center',
        resizeMode: 'contain'
    },
    rightArrowImageStyle: {
        marginLeft: 7.5,
        marginRight: 7.5,
        width: 15,
        height: 15,
        justifyContent: 'center',
        alignSelf: 'center',
        resizeMode: 'contain'
    },


})
