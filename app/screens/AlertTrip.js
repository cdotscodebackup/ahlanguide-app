import React from "react";
import {
    View,
    Text,
    Button,
    StyleSheet,
    ScrollView,
    Alert,
    TouchableOpacity,
    Image, NetInfo
} from "react-native";
import Colors from "../Styles/Colors";
import dimen from "../Styles/Dimen";
import AppText from "../Components/AppText";
import {Loader} from "../Components/Loader";
import {HomeHeaderQiblaMain, AllTripsHeaderButton, LogoTitle} from "../Components/HeaderComponents";
import TextStyles from "../Styles/TextStyles";
import {
    errorAlert,
    getData,
    getLocalizedDate,
    getSavedObject,
    getUserObject, isNetConnected,
    saveMainObject, simpleAlert, successAlert
} from "../Classes/auth";
import {
    general_network_error, gpsError,
    internetError,
    permissionError,
    save_alert_url,
    single_trip_url,
    trip_key
} from "../Classes/UrlConstant";
import Constants from 'expo-constants';
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';

export class AlertTrip extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isSplash: true,
            tripsteps: [{"id": "1"}, {"id": "2"}],
            tripdata: {},
            userdata: {},
            isSendAlert: false,
            isqibla: false,
            isLoadingAlertSend: false,
            last_emergency_alert_datetime: '',
            last_date_time: '',
            location: null,
            errorMessage: "Error",
            permissionAvailable: false,
            permissionError: '',
            gpsEnabled: false,
            lat: 0.0,
            lon: 0.0,
            buttonDisabled: false,
        };
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerLeftContainerStyle: {},
            // headerTitle: <LogoTitle/>,

            headerStyle: {
                backgroundColor: Colors.primary_color,
            },
            headerTitleStyle: {
                justifyContent: 'center',
                alignItems: 'center',
            },
            headerTitleContainerStyle: {
                left: 120,
                right: 120,
                justifyContent: 'center',
                alignItems: 'center'
            },

            headerRight: (navigation.state.isqibla ?
                    <HomeHeaderQiblaMain onPressItem={() => navigation.navigate('UserStack')}/> : null
            ),
            //hide backtitle and show custom button
            headerBackTitleVisible: false,

            headerLeft: (
                <AllTripsHeaderButton onPressItem={() => navigation.navigate('AllTripsStack')}/>
            ),
        }


    };

    componentWillMount() {
        this.loadTripData()
        // this._getLocationAsync();
    }

    loadTripData() {
        getSavedObject(trip_key)
            .then((data) => {
                this.setState({
                    tripdata: JSON.parse(data),
                    last_emergency_alert_datetime: JSON.parse(data).last_emergency_alert_datetime != null ? JSON.parse(data).last_emergency_alert_datetime.date : ''
                })
            }).catch(() => {
        })

        getUserObject().then((user) => {
            this.setState({
                userdata: JSON.parse(user),
            })
        })
    }

    updateState = (data) => {
        this.setState({
            tripdata: JSON.parse(data),
            tripsteps: JSON.parse(data).current_step.step.step_section,
            tripsection: JSON.parse(data).current_step.step,
            trip_updates: JSON.parse(data).trip_updates == null ? [] : JSON.parse(data).trip_updates,
        })
    }
    refreshTripData = () => {

        this.setState({isLoading: true})
        getData(single_trip_url + this.state.tripdata.pivot.id, 'GET', '')
            .then((res) => {
                this.setState({isLoading: false})
                this.updateState(JSON.stringify(res))
                saveMainObject(trip_key, JSON.stringify(res))
                    .then((res) => {
                    })
                    .catch((res) => {
                    })
            })
            .catch(err => {
                this.setState({isLoading: false})
            });

    }
    _getLocationAsync = async () => {
        let {status} = await Permissions.askAsync(Permissions.LOCATION);
        if (status !== 'granted') {
            this.setState({permissionAvailable: false,});
        }
        else {
            this.setState({permissionAvailable: true,});
            Location.getProviderStatusAsync()
                .then((loc) => {
                    console.log(loc);
                    this.setState({gpsEnabled: loc.gpsAvailable})
                })
                .catch((err) => {
                    console.log(err);
                })

            let location = await Location.getCurrentPositionAsync({});
            this.setState({
                location: location,
                lat: location.coords.latitude,
                lon: location.coords.longitude
            });
        }

    };

    sendAlertToAgent() {
        Alert.alert(
            'Alert ',
            'Are you sure you want to send alert?',
            [
                // {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                {text: 'OK', onPress: () => this.sendAlertAfterPermission()},
            ],
            {cancelable: false},)
    };

    sendAlertAfterPermission = () => {

        Permissions.askAsync(Permissions.LOCATION)
            .then((res) => {
                // console.log(res)
                if (res.status === 'granted') {
                    this.setState({permissionAvailable: false});
                    Location.getProviderStatusAsync()
                        .then((loc) => {
                            if (loc.gpsAvailable) {
                                this.setState({permissionAvailable: false});
                                Location.getCurrentPositionAsync({})
                                    .then((locationObject) => {
                                        this.setState({
                                            location: locationObject,
                                            lat: locationObject.coords.latitude,
                                            lon: locationObject.coords.longitude
                                        }, function () {
                                            this.sendAlert();
                                        });
                                    })
                                    .catch((locationError) => {
                                    })
                            }
                            else {
                                this.setState({
                                    permissionAvailable: true,
                                    permissionError: gpsError
                                });
                                errorAlert('Enable location to send alerts.')
                            }
                        })
                        .catch((err) => {
                            // console.log(err);
                        })
                }
                else {
                    // console.log('not Granted');
                    this.setState({
                        permissionAvailable: true,
                        permissionError: permissionError
                    });
                }
            })
            .catch(()=>{})


        // if (status !== 'granted') {
        //     this.setState({permissionAvailable: false,});
        // }
        // else {
        //     this.setState({permissionAvailable: true,});
        //     Location.getProviderStatusAsync()
        //         .then((loc) => {
        //             console.log(loc);
        //             this.setState({gpsEnabled: loc.gpsAvailable})
        //         })
        //         .catch((err) => {
        //             console.log(err);
        //         })
        //
        //     let location = await Location.getCurrentPositionAsync({});
        //     this.setState({
        //         location: location,
        //         lat: location.coords.latitude,
        //         lon: location.coords.longitude
        //     });
        // }
        //
        // if (!this.state.permissionAvailable) {
        //     Alert.alert(
        //         'Error',
        //         'For alert you need to enable location permissions,'
        //         ,
        //         [
        //             // {
        //             //     text: 'Cancel',
        //             //     onPress: () => console.log('Cancel Pressed'),
        //             //     style: 'cancel',
        //             // },
        //             {
        //                 text: 'Okay',
        //                 onPress: () => {
        //                     console.log('Open pressed')
        //                     this._getLocationAsync();
        //
        //                 }
        //             },
        //         ],
        //         {cancelable: true},)
        //
        // }
        // else {
        //     this._getLocationAsync();
        // }
    }

    sendAlert() {
        var end_date = new Date(this.state.tripdata.trip.end_date);
        // console.log(this.state.tripdata.trip.end_date)
        var date = new Date();
        if (date > end_date){
            simpleAlert('Trip has expired. You cannot send alert for an expired trip.')
            return;
        }

        isNetConnected().then(() => {
            this.setState({isLoadingAlertSend: true});
            let formdata = new FormData();
            formdata.append("lat", this.state.lat)
            formdata.append("lon", this.state.lon)

            getData(save_alert_url + this.state.tripdata.pivot.id, 'POST', formdata)
                .then((res) => {
                    this.setState({isLoadingAlertSend: false});
                    if (res.status) {
                        successAlert(res.message)
                        var date = new Date();
                        var month = (date.getMonth() + 1) < 10 ? '0' + date.getMonth() : date.getMonth()
                        var day = (date.getDay()) < 10 ? '0' + date.getDay() : date.getDay()
                        var hours = (date.getHours()) < 10 ? '0' + date.getHours() : date.getHours()
                        var minutes = (date.getMinutes()) < 10 ? '0' + date.getMinutes() : date.getMinutes()
                        var seconds = (date.getSeconds()) < 10 ? '0' + date.getSeconds() : date.getSeconds()

                        this.setState({
                            buttonDisabled: true,
                            // last_emergency_alert_datetime: date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDay()
                            //     + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds() + '.00000'
                            // last_date_time: date.getFullYear() + '-' + month + '-' + day
                            //     + ' ' + hours + ':' + minutes + ':' + seconds + '.00000'
                            last_date_time: date.toDateString() + ' | ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds()

                        })
                        this.startTimer();

                    }
                    else {
                        errorAlert(res.message);
                        // var date= new Date();
                        // this.setState({
                        //     last_emergency_alert_datetime: date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDay()
                        //         +'T'+date.getHours()+':'+date.getMinutes()+':'+date.getSeconds()+'.000Z'
                        // })
                    }

                })
                .catch((err) => {
                    this.setState({isLoadingAlertSend: false});
                    // console.log("Error while sending alert" + err)
                    errorAlert(general_network_error)
                })
        }).catch(() => {
            errorAlert(internetError)
        })
    }

    startTimer() {
        setTimeout(function () {
            this.setState({buttonDisabled: false})
        }.bind(this), 10000)
    }

    render() {

        let text = 'Waiting..';
        if (this.state.errorMessage) {
            text = this.state.errorMessage;
        } else if (this.state.location) {
            text = JSON.stringify(this.state.location);
        }


        var dateComponent = "Not Available", timeComponent = "Not Available";
        if (this.state.last_date_time !== '')
            dateComponent = this.state.last_date_time
        else {
            if (this.state.last_emergency_alert_datetime !== '') {
                var dumy = this.state.last_emergency_alert_datetime;
                var d = getLocalizedDate(dumy)
                dateComponent = d.toDateString() + ' | ' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
            }
        }
        // var str = (this.state.tripdata.last_emergency_alert_datetime);
        // var parts = str.slice(0, -1).split('T');
        // var dateComponent = parts[0];
        // var timeComponent = parts[1];
        return (
            <View style={styles.mainContainer}>
                <Image source={require('../Asset/bg.png')}
                       style={styles.backgroundImage}/>
                <View style={styles.container} bounces={true}>
                    <View style={styles.container2}>
                        <View style={styles.disclaimerContainer}>
                            <AppText style={styles.disclaimerHeadingText}>
                                Emergency Alert
                            </AppText>
                            <AppText style={styles.disclaimerText}>
                                اگر آپ کو کوئی ہنگامی صورتحال ہے اور آپ کو کسی مدد کی ضرورت ہے تو پھر جمع کرانے والے بٹن
                                پر کلک کریں۔

                                ہم فوری طور پر آپ سے رابطہ کریں گے۔

                            </AppText>
                        </View>
                        <View style={styles.lastAlertContainer}>
                            <AppText style={styles.lastAlertHeading}>
                                Last Alert
                            </AppText>
                            <AppText style={styles.lastAlertTimeText}>
                                {dateComponent}
                            </AppText>
                        </View>
                        {this.state.permissionAvailable &&
                        <AppText style={{
                            color: Colors.primary_dark_color,
                            fontSize: 9
                        }}>{this.state.permissionError}</AppText>
                        }
                        <View>
                            <TouchableOpacity
                                disabled={this.state.buttonDisabled} onPress={this.sendAlertToAgent.bind(this)}
                                style={[styles.sendAlertButton, TextStyles.buttonStyle,
                                    {backgroundColor: this.state.buttonDisabled ? Colors.border_color : Colors.primary_color}
                                ]}>
                                <AppText style={[TextStyles.buttonTextStyle]}>
                                    Submit
                                </AppText>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                {this.state.isLoadingAlertSend && <Loader/>}

            </View>

        )
    }
}

const styles = StyleSheet.create({
    mainContainer: {flex: 1, justifyContent: "flex-start", alignItems: "stretch"},
    backgroundImage: {
        bottom: 0,
        left: 0,
        right: 0,
        top: 0,
        width: '100%',
        height: '100%',
        position: 'absolute',
        resizeMode: 'cover'
    },
    container: {
        flex: 1, paddingTop: dimen.app_padding, paddingLeft: dimen.app_padding,
        paddingRight: dimen.app_padding,
        marginBottom: 60 + dimen.app_padding
    },
    container2: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'stretch',
        padding: dimen.app_padding,
        borderWidth: 1,
        borderRadius: 10,
        borderColor: Colors.border_color,
        backgroundColor: Colors.home_widget_background_color
    },
    disclaimerContainer: {flex: 1, justifyContent: 'center', alignItems: 'center'},
    disclaimerHeadingText: {color: Colors.primary_color, fontSize: 24},
    disclaimerText: {
        color: Colors.normal_text_color, fontSize: 18, lineHeight: 30, paddingTop: 10,
        alignItems: 'center', alignSelf: 'center', justifyContent: 'center', marginTop: 10
    },
    lastAlertContainer: {justifyContent: 'center', alignItems: 'center'},
    lastAlertHeading: {color: Colors.normal_text_color, fontSize: 14},
    lastAlertTimeText: {
        color: Colors.normal_text_color, fontSize: 14,
        alignItems: 'center', alignSelf: 'center', justifyContent: 'center', marginTop: 10
    },
    sendAlertButton: {
        marginBottom: 0,
        marginLeft: 0,
        marginRight: 0
    },


})
