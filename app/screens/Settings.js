import React from "react";
import {
    ActionSheetIOS,
    Alert,
    DatePickerAndroid,
    DatePickerIOS,
    Image,
    KeyboardAvoidingView,
    NetInfo,
    Picker,
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from "react-native";
import Colors from "../Styles/Colors";
import TextStyles from "../Styles/TextStyles";
import {
    errorAlert,
    getData,
    getUserObject,
    isNetConnected,
    onSignInUser,
    onSignOutUser,
    successAlert
} from "../Classes/auth";
import {
    base_profile_picture_url,
    general_network_error,
    internetError,
    logout_url,
    update_dp_url,
    update_user_url
} from "../Classes/UrlConstant";
import {Chooser, Loader} from "../Components/Loader";
import BackHeaderButton, {HomeHeaderSignout, AllTripsHeaderButton, LogoTitle} from "../Components/HeaderComponents";
import dimen from "../Styles/Dimen";
import AppText from "../Components/AppText";
import {TextInput} from "react-native-gesture-handler";

import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants'
import * as Permissions from 'expo-permissions'
import * as Location from "expo-location";

export class Settings extends React.Component {
    static navigationOptions = ({navigation}) => {
        return {
            title: 'Settings',
            // headerLayoutPreset:'left',

            headerLeftContainerStyle: {},
            // headerTitle: <LogoTitle/>,

            headerStyle: {
                backgroundColor: Colors.primary_color,
                // justifyContent: 'center'

            },
            // headerTintColor: '#fff',
            headerTitleStyle: {
                justifyContent: 'center',
                alignItems: 'center',
                // fontWeight: 'bold',
            },
            headerTitleContainerStyle: {
                left: 120,
                right: 120,
                justifyContent: 'center',
                alignItems: 'center'
            },

            headerRight: (
                <HomeHeaderSignout onPressItem={navigation.getParam('logoutUser')}/>
            ),
            //hide backtitle and show custom button
            headerBackTitleVisible: false,

            headerLeft: (
                <AllTripsHeaderButton onPressItem={() => navigation.navigate('AllTripsStack')}/>
            ),
        }


    };

    constructor(props) {
        super(props);
        this.state = {
            date: new Date(),
            dateios: new Date(),
            showDatePickerIOS: false,


            userdata: {},
            isLoading: false,
            showChooser: false,
            picture_to_upload: '',
            picture_path: null,

            name: "",
            email: "",
            password: '',
            cnPassword: "",
            primary_phone_number: "",
            secondary_phone_number: "",
            emergency_phone_number: "",
            occupation: "",
            blood_group: "",
            decease: "",
            special_needs: "",
            postal_address: "",
            dob: "",
            gender: "",
            gender_show: "",
            interest: "",
            cnic: "",
            passport_no: "",


            genderlist: ['Select one', 'Male', 'Female', 'Other'],

        };
    }

    componentDidMount() {
        // set handler method with setParams
        this.props.navigation.setParams({logoutUser: this.logoutUser});
        this.loadTripData();
        this.getPermissionAsync();
    }


    logoutUser = () => {
        // onSignOutUser()
        // this.props.navigation.navigate('Auth')
        this.setState({isLoading: true})
        getData(logout_url, 'POST', '')
            .then(res => {
                if (res.status) {
                    this.setState({
                        isLoading: false,
                    }, function () {
                        onSignOutUser()
                        this.props.navigation.navigate('Auth')

                    });
                }
                else {
                    this.setState({
                        isLoading: false
                    })
                    onSignOutUser()
                    this.props.navigation.navigate('Auth')
                }
            })
            .catch(err => {
                this.setState({
                    isLoading: false
                })
                errorAlert("Error logging out")
            })
    }
    loadTripData = () => {
        getUserObject()
            .then((user) => {
                var u = JSON.parse(user).user_type
                var g = u.gender;
                var sss = () => {
                    if (g === 'male')
                        return 'Male'
                    else if (g === 'female')
                        return 'Female'
                    else
                        return 'Other'
                };
                // console.log('gender: '+u.gender)
                this.setState({
                        userdata: JSON.parse(user),
                        picture_path: u.picture_path,


                        name: u.name,
                        passport_no: u.passport_no===null?"":u.passport_no,
                        cnic: u.cnic===null?"":u.cnic,
                        email: JSON.parse(user).email,
                        password: "",
                        cnPassword: "",
                        primary_phone_number: u.primary_phone_number,
                        secondary_phone_number: u.secondary_phone_number===null?"":u.secondary_phone_number,
                        emergency_phone_number: u.emergency_phone_number===null?"":u.emergency_phone_number,
                        occupation: u.occupation===null?"":u.occupation,
                        blood_group: u.blood_group===null?"":u.blood_group,
                        decease: u.decease===null?"":u.decease,
                        special_needs: u.special_needs===null?"":u.special_needs,
                        postal_address: u.postal_address===null?"":u.postal_address,
                        dob: u.dob,
                        gender: u.gender,
                        gender_show: sss(),
                        interest: u.interest,


                    }, function () {
                        // console.log("gender onload: " + this.state.gender)
                        // console.log("gender show onload: " + this.state.gender_show)
                    }
                )
            })
    }

    updateUser = () => {
        isNetConnected().then(() => {
            let formdata = new FormData();
            formdata.append("name", this.state.name)
            formdata.append("cnic", this.state.cnic)
            formdata.append("passport_no", this.state.passport_no)
            formdata.append("email", this.state.email)

            if (this.state.password != '') {
                if (this.state.password != this.state.cnPassword) {
                    errorAlert("Password and confirm password must match.")
                    return;
                }
                else {
                    formdata.append("password", this.state.password)
                    formdata.append("password_confirmation", this.state.cnPassword)

                }
            }
            formdata.append("primary_phone_number", this.state.primary_phone_number)
            formdata.append("secondary_phone_number", this.state.secondary_phone_number)
            formdata.append("emergency_phone_number", this.state.emergency_phone_number)
            formdata.append("occupation", this.state.occupation)
            formdata.append("blood_group", this.state.blood_group)
            // formdata.append("decease", this.state.decease)
            formdata.append("special_needs", this.state.special_needs)
            formdata.append("postal_address", this.state.postal_address)
            formdata.append("dob", this.state.dob)
            if (this.state.gender === 'Select one') {
                errorAlert("Select gender.")
                return;
            }
            formdata.append("gender", this.state.gender)

            this.setState({isLoading: true})
            getData(update_user_url, 'POST', formdata)
                .then((res) => {
                    this.setState({isLoading: false});
                    if (res.status) {
                        this.updateLocalUserData(res.user_type);
                        successAlert("Profile updated successfully.")
                    }
                    else {
                        var m = res.errors;
                        var keys = [];
                        for (var k in m) keys.push(k);

                        var mforuser = '';
                        // console.log('key 0 ' + (m[keys[0]]))

                        for (var k in keys)
                            mforuser = mforuser + m[keys[k]] + '\n'

                        Alert.alert(
                            'Error !',
                            mforuser,
                            [
                                {
                                    text: 'Okay',
                                    onPress: () => {
                                        // console.log('Open pressed')
                                    }
                                },
                            ],
                            {cancelable: false},
                        )

                        // console.log(JSON.stringify(res))
                    }
                })
                .catch((err) => {
                    this.setState({isLoading: false});
                    // console.log("Error: " + err)
                    errorAlert(general_network_error)
                })
        }).catch(() => {
            errorAlert(internetError)

        })
    }

    updateLocalUserData = (user_type) => {
        getUserObject().then((user) => {
            if (user != null) {
                var u = JSON.parse(user);
                u.user_type = user_type;

                onSignInUser(JSON.stringify(u));

            }
        })
    }
    updateLocalUserImage = (path) => {
        getUserObject().then((user) => {
            if (user != null) {
                var u = JSON.parse(user);
                u.user_type.picture_path = path;

                onSignInUser(JSON.stringify(u));

            }
        })
    }

    getPermissionAsync = async () => {
        if (Constants.platform.ios) {
            const {status} = await Permissions.askAsync(Permissions.CAMERA_ROLL);
            if (status !== 'granted') {
                alert('Sorry, we need camera roll permissions to make this work!');
            }
        }
    }

    imageChooserDialog = () => {
        this.setState({showChooser: true})

    }
    cancelPress = () => {
        this.setState({showChooser: false})
    }
    takePhotoPress = () => {
        this.setState({showChooser: false}, function () {
            this.openCameraforPhoto()
        })
    }
    choosePhotoPress = () => {
        this.setState({showChooser: false}, function () {
            this.pickImage();
        })

    }

    openCameraforPhoto = async () => {

        let {status} = await Permissions.askAsync(Permissions.CAMERA, Permissions.CAMERA_ROLL);
        if (status !== 'granted') {
            // errorAlert('Location permissionAvailable is required for working.')
            console.log('permissionAvailable not granted')
            this.setState({
                errorMessage: 'Permission to access location was denied',
            });
        }
        else {

            let result = await ImagePicker.launchCameraAsync({
                mediaTypes: ImagePicker.MediaTypeOptions.Images,
                allowsEditing: true,
                aspect: [3, 3],
                quality: 0.5,
            });
            if (!result.cancelled) {
                console.log('file size: ' + JSON.stringify(result) + "  " + result.name)
                this.setState({picture_to_upload: result.uri});
                this.updatePhotoPressed()
            }
        }
    };
    pickImage = async () => {

        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            allowsEditing: true,
            aspect: [3, 3],
            quality: 0.5,
        });
        if (!result.cancelled) {
            // console.log('file size: '+ JSON.stringify(result)+"  " +result.name)
            this.setState({picture_to_upload: result.uri});
            this.updatePhotoPressed()
        }
    };
    updatePhotoPressed = () => {
        isNetConnected().then(() => {
            this.setState({isLoading: true})
            const data = new FormData();
            data.append('picture', {
                uri: this.state.picture_to_upload,
                type: 'image/jpeg', // or photo.type
                name: 'testPhotoName'
            });
            getData(update_dp_url, 'POST', data)
                .then((res) => {
                    // console.log(res);
                    this.setState({isLoading: false})
                    if (res.user_type.picture_path) {
                        successAlert("Updated Successfully.")
                        this.updateLocalUserImage(res.user_type.picture_path)
                        this.setState({picture_path: res.user_type.picture_path})
                    }
                    else {
                        errorAlert(res.message);
                        // this.setState({picture_path: null})
                        // console.log(JSON.stringify(res))
                    }
                })
                .catch(err => {
                    // console.log("error fetching data : " + err)
                    errorAlert(general_network_error)
                    this.setState({isLoading: false})
                });
        }).catch(() => {
            errorAlert(internetError)
        })

    }

    openDatePicker = () => {
        try {
            const {action, year, month, day} = DatePickerAndroid.open({
                // Use `new Date()` for current date.
                // May 25 2020. Month 0 is January.
                date: new Date(2020, 4, 25),
            });
            if (action !== DatePickerAndroid.dismissedAction) {
                // Selected year, month (0-11), day
            }
        } catch ({code, message}) {
            console.warn('Cannot open date picker', message);
        }
    }

    setDateAndroid = async () => {
        try {
            const {
                action, year, month, day,
            } = await DatePickerAndroid.open({
                date: new Date(),
                // minDate: new Date(),
            });
            if (action !== DatePickerAndroid.dismissedAction) {
                this.setState({dob: year + '-' + (month+1) + '-' + day});
            }
        } catch ({code, message}) {
            console.warn('Cannot open date picker', message);
        }
    };
    setDateIOS = () => {
        this.setState({showDatePickerIOS: !this.state.showDatePickerIOS})
    };
    showActionSheet = () => {
        ActionSheetIOS.showActionSheetWithOptions({
                options: this.state.genderlist,
                // cancelButtonIndex: CANCEL_INDEX,
                // destructiveButtonIndex: this.state.feedback_categories_titles_ios!==undefined? this.state.feedback_categories_titles_ios.length-1:0,
            },
            (buttonIndex) => {
                this.setState({
                    gender: buttonIndex === 0 ? 'Select one' : buttonIndex === 1 ? 'male' : buttonIndex === 2 ? 'female' : 'not_specified',
                    gender_show: this.state.genderlist[buttonIndex]
                });
            });
    }

    render() {
        var dateComponent = "Not Available", timeComponent;
        if (this.state.dob != null) {
            var str = (this.state.dob);
            // console.log(this.state.dob)

            var parts = str.split(' ');
            dateComponent = parts[0];
            // dateComponent = new Date(this.state.dob).getDate();
            // console.log(dateComponent)

        }
        var showDatePicker = this.state.showDatePickerIOS ? <View
            style={{margin: 10, borderWidth: 1, borderRadius: dimen.border_radius,}}
        >
            <View style={{flexDirection: 'row', justifyContent: 'space-between', margin: dimen.app_padding}}>
                <AppText>Select Date</AppText>
                <TouchableOpacity onPress={this.setDateIOS}>
                    <AppText style={{color: Colors.primary_color}}>Done</AppText>
                </TouchableOpacity>
            </View>
            <DatePickerIOS
                date={this.state.dateios} onDateChange={(date) => {
                this.setState({
                    dateios: date,
                    dob: date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + ' '
                })
            }}
                mode="date"/>
        </View> : null

        return (
            <View style={{flex: 1}}>
                <Image source={require('../Asset/bg.png')} style={styles.backgroundImage}/>
                <KeyboardAvoidingView style={{flex: 1}}
                    // keyboardVerticalOffset={20}
                                      behavior="padding"
                                      enabled={true}>
                    <ScrollView style={{flex: 1,}}>
                        <View style={styles.scrollInnerContainer}>
                            <TouchableOpacity onPress={this.imageChooserDialog}>
                                <View style={styles.profileContainer}>
                                    {this.state.picture_path === null &&
                                    <Image source={require('../Asset/user.png')}
                                           style={styles.dummyImageStyle}/>
                                    }
                                    {this.state.picture_path !== null &&
                                    <Image source={{uri: base_profile_picture_url + this.state.picture_path}}
                                           style={styles.profileImageStyle}/>
                                    }
                                </View>
                            </TouchableOpacity>

                            <View style={styles.fieldContainer}>
                                <View style={TextStyles.stericContainer}>
                                    <AppText style={TextStyles.settingHeadingStyle}>
                                        Name
                                    </AppText>
                                    <Text style={TextStyles.stericStyle}>*</Text>
                                </View>
                                <TextInput onChangeText={(name) => this.setState({name})}
                                           value={this.state.name}
                                           placeholder="Name"
                                           style={[TextStyles.inputstyle]}/>
                            </View>
                            <View style={styles.fieldContainer}>
                                <AppText style={TextStyles.settingHeadingStyle}>
                                    CNIC
                                </AppText>
                                <TextInput onChangeText={(cnic) => this.setState({cnic})}
                                           value={this.state.cnic}
                                           placeholder="CNIC"
                                           style={[TextStyles.inputstyle]}/>
                            </View>
                            <View style={styles.fieldContainer}>
                                <AppText style={TextStyles.settingHeadingStyle}>
                                    Passport No
                                </AppText>
                                <TextInput onChangeText={(passport_no) => this.setState({passport_no})}
                                           value={this.state.passport_no}
                                           placeholder="Passport No"
                                           style={[TextStyles.inputstyle]}/>
                            </View>
                            <View style={styles.fieldContainer}>
                                <View style={TextStyles.stericContainer}>
                                    <AppText style={TextStyles.settingHeadingStyle}>
                                        Gender
                                    </AppText>
                                    <Text style={TextStyles.stericStyle}>*</Text>
                                </View>
                                {Platform.OS === 'ios' ?
                                    <TouchableOpacity onPress={this.showActionSheet}>
                                        <AppText onChangeText={(gender) => this.setState({gender})}
                                                 value={this.state.gender}
                                                 placeholder="Gender"
                                                 enabled={false}
                                                 style={[TextStyles.inputstyle]}>
                                            {this.state.gender_show}
                                        </AppText>
                                        <View style={styles.dropdownIosContainerDate}>
                                            <Image source={require('../Asset/drop_down.png')}
                                                   style={{width: 14, height: 14, resizeMode: 'contain'}}/>
                                        </View>

                                    </TouchableOpacity>
                                    :
                                    <View style={{
                                        borderRadius: 10,
                                        borderColor: Colors.border_color,
                                        margin: 10,
                                        backgroundColor: Colors.home_widget_background_color,
                                        borderWidth: 1, overflow: 'hidden'
                                    }}
                                    >
                                        <Picker
                                            style={{backgroundColor: Colors.home_widget_background_color,}}
                                            onValueChange={(g, index) => {
                                                this.setState({
                                                        gender: g,
                                                        gender_show: g
                                                    }, function () {
                                                    }
                                                )
                                            }}
                                            selectedValue={this.state.gender}
                                        >
                                            <Picker.Item label="Select one" value="Select one"/>
                                            <Picker.Item label="Male" value="male"/>
                                            <Picker.Item label="Female" value="female"/>
                                            <Picker.Item label="Other" value="not_specified"/>
                                        </Picker>
                                        <View style={{
                                            position: 'absolute',
                                            top: 0,
                                            left: 0,
                                            right: 10,
                                            bottom: 0,
                                            flexDirection: 'row',
                                            justifyContent: 'flex-end',
                                            alignItems: 'center'
                                        }}>
                                            <Image source={require('../Asset/drop_down.png')}
                                                   style={{width: 14, height: 14, resizeMode: 'contain'}}/>
                                        </View>
                                    </View>
                                }
                            </View>
                            <View style={styles.fieldContainer}>
                                <View style={TextStyles.stericContainer}>
                                    <AppText style={TextStyles.settingHeadingStyle}>
                                        Date of Birth
                                    </AppText>
                                    <Text style={TextStyles.stericStyle}>*</Text>
                                </View>
                                <TouchableOpacity
                                    onPress={Platform.OS != 'ios' ? this.setDateAndroid : this.setDateIOS}>
                                    <AppText onChangeText={(dob) => this.setState({dob})}
                                             value={this.state.dob}
                                             placeholder="YYYY-MM-DD"
                                             style={[TextStyles.inputstyle]}>
                                        {dateComponent}
                                    </AppText>

                                </TouchableOpacity>
                                {showDatePicker}
                            </View>
                            <View style={styles.fieldContainer}>
                                <AppText style={TextStyles.settingHeadingStyle}>
                                    Email
                                </AppText>
                                <AppText onChangeText={(email) => this.setState({email})}
                                         value={this.state.email}
                                         enabled={false}
                                         placeholder="Nick Name"
                                         style={[TextStyles.inputstyle, {color: Colors.gray}]}>
                                    {this.state.email}
                                </AppText>
                            </View>
                            <View style={styles.fieldContainer}>
                                <AppText style={TextStyles.settingHeadingStyle}>
                                    New Password
                                </AppText>
                                <TextInput onChangeText={(password) => this.setState({password})}
                                           value={this.state.password}
                                           placeholder="Password"
                                           secureTextEntry={true}
                                           style={[TextStyles.inputstyle]}/>
                            </View>
                            <View style={styles.fieldContainer}>
                                <AppText style={TextStyles.settingHeadingStyle}>
                                    Confirm Password
                                </AppText>
                                <TextInput onChangeText={(cnPassword) => this.setState({cnPassword})}
                                           value={this.state.cnPassword}
                                           placeholder="Confirm Password"
                                           secureTextEntry={true}
                                           style={[TextStyles.inputstyle]}/>
                            </View>
                            <View style={styles.fieldContainer}>
                                <AppText style={TextStyles.settingHeadingStyle}>
                                    Occupation
                                </AppText>
                                <TextInput onChangeText={(occupation) => this.setState({occupation})}
                                           value={this.state.occupation}
                                           placeholder="Occupation"
                                           style={[TextStyles.inputstyle]}/>
                            </View>
                            <View style={styles.fieldContainer}>
                                <View style={TextStyles.stericContainer}>
                                    <AppText style={TextStyles.settingHeadingStyle}>
                                        Phone Primary
                                    </AppText>
                                    <Text style={TextStyles.stericStyle}>*</Text>
                                </View>
                                <TextInput
                                    onChangeText={(primary_phone_number) => this.setState({primary_phone_number})}
                                    value={this.state.primary_phone_number}
                                    placeholder="Phone primary"
                                    style={[TextStyles.inputstyle]}/>
                            </View>
                            <View style={styles.fieldContainer}>
                                <AppText style={TextStyles.settingHeadingStyle}>
                                    Phone Secondary
                                </AppText>
                                <TextInput
                                    onChangeText={(secondary_phone_number) => this.setState({secondary_phone_number})}
                                    value={this.state.secondary_phone_number}
                                    placeholder="Phone Secondary"
                                    style={[TextStyles.inputstyle]}/>
                            </View>
                            <View style={styles.fieldContainer}>
                                <AppText style={TextStyles.settingHeadingStyle}>
                                    Emergency Contact
                                </AppText>
                                <TextInput
                                    onChangeText={(emergency_phone_number) => this.setState({emergency_phone_number})}
                                    value={this.state.emergency_phone_number}
                                    placeholder="Emergency Contact"
                                    style={[TextStyles.inputstyle]}/>
                            </View>
                            <View style={styles.fieldContainer}>
                                <AppText style={TextStyles.settingHeadingStyle}>
                                    Blood Group
                                </AppText>
                                <TextInput onChangeText={(blood_group) => this.setState({blood_group})}
                                           value={this.state.blood_group}
                                           placeholder="Blood Group"
                                           style={[TextStyles.inputstyle]}/>
                            </View>
                            <View style={styles.fieldContainer}>
                                <AppText style={TextStyles.settingHeadingStyle}>
                                    Special needs
                                </AppText>
                                <TextInput onChangeText={(special_needs) => this.setState({special_needs})}
                                           value={this.state.special_needs}
                                           placeholder="Special needs"
                                           style={[TextStyles.inputstyle]}/>
                            </View>
                            <View style={styles.fieldContainer}>
                                <View style={TextStyles.stericContainer}>
                                    <AppText style={TextStyles.settingHeadingStyle}>
                                        Address
                                    </AppText>
                                    <Text style={TextStyles.stericStyle}>*</Text>
                                </View>
                                <TextInput onChangeText={(postal_address) => this.setState({postal_address})}
                                           value={this.state.postal_address}
                                           placeholder="Address"
                                           style={[TextStyles.inputstyle]}/>
                            </View>

                            <TouchableOpacity onPress={this.updateUser}
                                              style={[{marginTop: 30}, TextStyles.buttonStyle]}>
                                <Text style={TextStyles.buttonTextStyle}>Save</Text>
                            </TouchableOpacity>

                        </View>

                    </ScrollView>
                </KeyboardAvoidingView>
                {this.state.isLoading && <Loader/>}
                {this.state.showChooser && <Chooser cancelPress={this.cancelPress} takePhotoPress={this.takePhotoPress}
                                                    choosePhotoPress={this.choosePhotoPress}/>}
            </View>
        )
    }
}


const styles = StyleSheet.create({
    backgroundImage: {
        bottom: 0,
        left: 0,
        right: 0,
        top: 0,
        width: '100%',
        height: '100%',
        position: 'absolute',
        resizeMode: 'cover'
    },
    scrollInnerContainer: {
        flex: 1,
        marginBottom: dimen.bottom_margin_for_bottom_menu + dimen.app_padding,
        marginLeft: dimen.app_padding, marginRight: dimen.app_padding, marginTop: dimen.app_padding,
        borderRadius: dimen.border_radius,
        overflow: 'hidden',
        borderColor: Colors.border_color,
        borderWidth: 1,
        backgroundColor: Colors.home_widget_background_color
    },
    profileContainer: {
        width: 150,
        height: 150,
        alignSelf: 'center',
        overflow: 'hidden',
        borderRadius: 75,
        borderColor: Colors.border_color,
        borderWidth: 1,
        marginTop: 10
    },
    dummyImageStyle: {
        width: 150,
        height: 150,
        borderRadius: 75,
        backgroundColor: Colors.light_border_color,
        borderWidth: 1,
        resizeMode: 'contain',
        alignSelf: 'center'
    },
    profileImageStyle: {
        width: 150,
        height: 150,
        borderRadius: 75,
        borderWidth: 1,
        resizeMode: 'cover',
        alignSelf: 'center'
    },
    fieldContainer: {marginTop: dimen.app_padding},
    dropdownIosContainerDate:{
        position: 'absolute',
        top: 0,
        left: 0,
        right: 20,
        bottom: 0,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },



})
