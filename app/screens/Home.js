import React from "react";
import {
    View, Text, Button, StyleSheet, Alert,
    ScrollView, Image, TextInput, ActivityIndicator, TouchableOpacity, TouchableWithoutFeedback,
    NetInfo, RefreshControl,
} from "react-native";

import {Header} from '../Components/CustomHeader';
import BackHeaderButton, {
    HomeHeaderSetting, LogoTitle,
} from '../Components/HeaderComponents';
import {FlatList} from "react-native-gesture-handler";
import {MyTripsCard} from "../Components/MyTripsCard";
import Colors from "../Styles/Colors";
import {errorAlert, getData, getSavedObject, saveMainObject} from "../Classes/auth";
import {Loader} from "../Components/Loader";
import dimen from "../Styles/Dimen";
import AppText from "../Components/AppText";
import {
    all_trips_key,
    get_all_trips_url,
    internetError,
    single_trip_url,
    trip_key
} from "../Classes/UrlConstant";
import {Notifications} from "expo";


export class Home extends React.Component {
    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: <LogoTitle/>,
            headerStyle: {
                backgroundColor: Colors.primary_color,
            },
            headerTitleStyle: {
                justifyContent: 'center',
                alignItems: 'center',
            },
            headerTitleContainerStyle: {
                left: 120,
                right: 120,
                justifyContent: 'center',
                alignItems: 'center'
            },
            headerBackTitleVisible: false,
            headerLeft: (navigation.getParam('shouldGoBack') ?
                    <BackHeaderButton onPressItem={() => navigation.navigate('AppHome')}/> : null
            ),
            // headerRight: (navigation.getParam('shouldShowSetting') ?
            //         <HomeHeaderSetting onPressItem={() => navigation.navigate('SettingsStack')}/> : null
            // ),
            headerRight: (
                <HomeHeaderSetting onPressItem={() => navigation.navigate('SettingsStack')}/>
            ),
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            checkConnection: false,//is Not Connected to internet
            isInternetConnected: true,
            isLoadingAllTrips: false,
            isLoadingAllTripsPull: false,
            trips: [],
            errorText: 'No trip assigned to you. Please contact your agent.',
            isLoading: false,
            shouldGoBack: false,
            shouldShowSetting: false,
            trip_length: -1,
            packageExpired: false,
        };

    }

    componentWillMount() {
        this.loadData();
        // this._notificationSubscription = Notifications.addListener(this._handleNotification);

    }

    componentDidMount() {
    }

    _handleNotification = (notification) => {
        this.setState({notification: notification});
        alert('notification')
        console.log('Origin: ' + this.state.notification.origin)
        console.log('Data:' + JSON.stringify(this.state.notification.data))
    };

    componentWillUnmount() {
    }

    loadData() {
        this.getAllTrips();
        getSavedObject(trip_key)
            .then((data) => {
                if (!JSON.parse(data).status)
                    this.updateNavigationParam()
            }).catch((err) => {
        })

    }

    updateNavigationParam = () => {
        this.props.navigation.setParams({
            shouldGoBack: false,
        });
    }
    updateShowSettingParam = () => {
        this.props.navigation.setParams({
            shouldShowSetting: true,
        });
    }

    getAllTrips = () => {
        NetInfo.isConnected.fetch()
            .then(isConnected => {
                if (isConnected) {
                    this.setState({isLoadingAllTrips: true,});
                    getData(get_all_trips_url, 'GET', '')
                        .then((res) => {
                            if (!res.status) {
                                this.setState({isLoadingAllTrips: false})
                                if (res.length > 0) {
                                    saveMainObject(all_trips_key, JSON.stringify(res))
                                        .then((res) => {
                                            this.updateNavigationParam()
                                        })
                                        .catch((res) => {
                                        })
                                    var current_trip = (res[0]);
                                    var end_date = new Date(current_trip.trip.travel_agency.package_end_date);
                                    // var end_date = new Date('2019-11-10');
                                    var date = new Date();
                                    if (date > end_date) {
                                        this.setState({packageExpired: true})
                                        this.updateShowSettingParam();
                                    } else {
                                        this.setState({packageExpired: false})
                                    }
                                }
                                this.updateState(res);
                            }
                        })
                        .catch((err) => {
                            errorAlert('Error fetching data.')
                            this.setState({isLoadingAllTrips: false,});
                            this.loadSavedTripData();
                        })
                } else {
                    errorAlert(internetError)
                    this.setState({isLoadingAllTrips: false})
                    this.loadSavedTripData();
                }
            })
            .catch(() => {
                errorAlert(internetError)
                this.loadSavedTripData();
            })
    }

    loadSavedTripData = () => {
        getSavedObject(all_trips_key)
            .then((data) => {
                this.updateState(JSON.parse(data))
            }).catch((err) => {
            errorAlert('Something went wrong');
        })
    }
    updateState = (data) => {
        if (data === false) data = null;
        this.setState({
            trips: data !== null ? data : [],
            trip_length: data !== null && data !== undefined ? data.length : 0,
        }, function () {
            if (this.state.trip_length === 0) { // show settings if no trip found
                this.updateShowSettingParam();
            }
        })
    }

    trip_pressed(item) {
        saveMainObject(trip_key, JSON.stringify(item))
            .then((res) => {
                // this.props.navigation.navigate('AppHome');

                if (item.trip.app_menu === 0) {//if need to show bottom menu
                    this.props.navigation.navigate('AppHome');
                } else if (item.trip.app_menu === 1) {// if hide bottom mene then go to different stack
                    this.props.navigation.navigate('CurrentTripsStack');
                }

            })
            .catch((res) => {
                console.log("error saving data")
            })


    };

    render() {
        return (
            <View style={styles.mainContainer}>
                <Image source={require('../Asset/bg.png')} style={styles.backgroundImage}/>
                {this.state.trips.length > 0 && !this.state.packageExpired &&
                <ScrollView style={styles.scrollview} bounces={true}
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.isLoadingAllTripsPull}
                                    onRefresh={this.getAllTrips}
                                />
                            }>
                    <View style={styles.selectTripTitle}>
                        <AppText>Select a Trip</AppText>
                    </View>
                    <View style={styles.allTripsContainer}>
                        <FlatList
                            data={this.state.trips}
                            keyExtractor={(item, index) => item.id.toString()}
                            numColumns={1}
                            renderItem={({item}) => (
                                <MyTripsCard
                                    onPressItem={this.trip_pressed.bind(this)}
                                    item={item}
                                    imagePress={(path)=>this.props.navigation.navigate('ImageScreen',{path:path})}
                                />)}
                        />
                    </View>
                </ScrollView>
                }
                {this.state.trip_length === 0 && !this.state.packageExpired &&
                <View style={styles.reloadContainer}>
                    <AppText style={styles.errorTextStyle}>{this.state.errorText}</AppText>
                    <TouchableOpacity onPress={this.loadData.bind(this)}>
                        <AppText style={styles.tapToReloadText}>Tap to reload</AppText>
                    </TouchableOpacity>
                </View>
                }

                {this.state.packageExpired &&
                <View style={styles.reloadContainer}>
                    <AppText style={styles.errorTextStyle}>Package has expired. Contact your agent.</AppText>
                    <TouchableOpacity onPress={this.loadData.bind(this)}>
                        <AppText style={styles.tapToReloadText}>Tap to reload</AppText>
                    </TouchableOpacity>
                </View>
                }

                {this.state.isLoadingAllTrips && <Loader/>}
            </View>
        )
    }
}


const styles = StyleSheet.create({
    backgroundImage: {
        bottom: 0,
        left: 0,
        right: 0,
        top: 0,
        width: '100%',
        height: '100%',
        position: 'absolute',
        resizeMode: 'cover'
    },
    scrollview: {flex: 1},
    selectTripTitle: {margin: dimen.app_padding},
    allTripsContainer: {padding: dimen.app_padding,},
    reloadContainer: {flex: 1, justifyContent: 'center', alignItems: 'center'},
    errorTextStyle: {
        marginBottom: dimen.app_padding,
        margin: dimen.app_padding * 2,
        textAlign: 'center'
    },
    tapToReloadText: {color: Colors.primary_color},

    mainContainer: {flex: 1},


})


