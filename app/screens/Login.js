import React from 'react';
import {
    Alert,
    Image,
    KeyboardAvoidingView,
    NetInfo,
    StyleSheet,
    Text,
    Platform,
    TouchableOpacity,
    View
} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import Colors from '../Styles/Colors';
import TextStyles from '../Styles/TextStyles';
import dimen from "../Styles/Dimen";
import {
    _storeData,
    errorAlert,
    getData,
    getDataWithoutToken,
    onSignInUser,
    saveMainObject,
    simpleAlert
} from "../Classes/auth";
import {Loader} from "../Components/Loader";
import {
    admin_login_url,
    fcm_agent_url,
    fcm_client_url,
    get_current_trip_url,
    internetError,
    login_url,
    trip_key
} from "../Classes/UrlConstant";
import {Notifications} from 'expo';
import * as Permissions from 'expo-permissions'
import AppText from "../Components/AppText";


async function registerForPushNotificationsAsync() {

    const {status: existingStatus} = await Permissions.getAsync(
        Permissions.NOTIFICATIONS
    );
    let finalStatus = existingStatus;
    // console.log("final status:"+ finalStatus)


    // only ask if permissions have not already been determined, because
    // iOS won't necessarily prompt the user a second time.
    if (existingStatus !== 'granted') {

        // Android remote notification permissions are granted during the app
        // install, so this will only ask on iOS
        const {status} = await Permissions.askAsync(Permissions.NOTIFICATIONS);
        finalStatus = status;
        console.log("final status:" + finalStatus)

    }

    if (Platform.OS === 'android') {
        await Notifications.createChannelAndroidAsync('message', {
            name: 'message',
            sound: true,
            priority: 'high',
            vibrate: [0, 250, 250, 250],
        });
    }


    // Stop here if the user did not grant permissions
    if (finalStatus !== 'granted') {
        // console.log("final status:"+finalStatus)
        return;
    }

    // Get the token that uniquely identifies this device
    let token = await Notifications.getExpoPushTokenAsync();
    return token;
}

export class Loginn extends React.Component {

    constructor(props) {
        super(props);
        this.state =
            {
                email: '',
                password: '',
                isLoading: false,
                hidesomelayout: false,
                isChecked: false,
            };
    }

    loginPressed = () => {
        const user = this.state;
        if (user.email == '') {
            simpleAlert('Enter your email');
        } else if (user.password == '') {
            simpleAlert('Enter your password');
        } else {
            NetInfo.isConnected.fetch()
                .then(isConnected => {
                    if (!this.state.isChecked)
                        this.loginUser()
                    else
                        this.loginAgent()
                })
                .catch(() => {
                    errorAlert(internetError)
                })
        }

    };

    loginUser = () => {
        this.setState({isLoading: true})

        let formdata = new FormData();
        formdata.append("email", this.state.email)
        formdata.append("password", this.state.password)

        getDataWithoutToken(login_url, 'POST', formdata)
            .then(res => {
                this.setState({isLoading: false})
                if (res.token) {
                    onSignInUser(JSON.stringify(res))
                    this.registerFcmUser();
                    getData(get_current_trip_url, 'GET', '')
                        .then((res) => {
                            this.setState({isLoading: false})
                            if (res != null) {
                                saveMainObject(trip_key , JSON.stringify(res))
                                    .then((res) => {
                                        this.props.navigation.navigate('AuthLoading')
                                    })
                                    .catch((res) => {
                                        this.props.navigation.navigate('AllTripsStack')
                                    })
                            } else {
                                this.props.navigation.navigate('AllTripsStack')
                            }
                        })
                        .catch(err => {
                            this.setState({isLoading: false})
                            this.props.navigation.navigate('AllTripsStack')
                        })
                }
                else if (res.errors) {
                    this.setState({isLoading: false})
                    errorAlert(res.message)
                }
            })
            .catch(err => {
                errorAlert("Wrong Credentials.")
                this.setState({isLoading: false,})
            })

    }
    loginAgent = () => {
        this.setState({isLoading: true})

        let formdata = new FormData();
        formdata.append("email", this.state.email)
        formdata.append("password", this.state.password)

        getDataWithoutToken(admin_login_url, 'POST', formdata)
            .then(res => {
                this.setState({isLoading: false})
                if (res.token) {
                    onSignInUser(JSON.stringify(res))
                    this.registerFcmAdmin();
                    this.props.navigation.navigate('AdminStack')
                }
                else if (res.errors) {
                    errorAlert(res.message)
                }
            })
            .catch(err => {
                errorAlert("Wrong Credentials.")
                this.setState({
                    isLoading: false,
                })
            })

    }

    registerFcmUser = () => {
        registerForPushNotificationsAsync()
            .then(token => {
                console.log("token: " + token)
                this.sendFcmTokenUser(token)
            })
            .catch(err => console.log("Error:  token" + err))

    }
    sendFcmTokenUser = (token) => {
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected) {
                let formdata = new FormData();
                formdata.append("fcm_token", token)
                getData(fcm_client_url, 'POST', formdata)
                    .then((res) => {
                        console.log("fcm saved")
                    })
                    .catch(err => {
                        console.log("fcm not saved")
                    });

            }
            else {
                errorAlert(internetError)
                this.setState({isLoading: false})
            }
        })

    }

    registerFcmAdmin = () => {

        registerForPushNotificationsAsync()
            .then(token => {
                this.sendfcmtokenAdmin(token)
                // console.log("token: "+token)
            })
            .catch(err => console.log("Error: " + err))
        console.log("Token from fun: ")
    }
    sendfcmtokenAdmin = (token) => {

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected) {
                let formdata = new FormData();
                formdata.append("fcm_token", token)
                getData(fcm_agent_url, 'POST', formdata)
                    .then((res) => {
                        console.log("fcm saved")
                    })
                    .catch(err => {
                        console.log("fcm not saved")
                    });

            }
            else {
                errorAlert(internetError)
                this.setState({isLoading: false})
            }
        })


    }

    handleCheckBox = () => {
        this.setState((prevState) => ({
            isChecked: !prevState.isChecked
        }));
    }

    loginAgentWeb = () => {
        this.props.navigation.navigate('AdminStack');
    }
    forgetPassword=() => {
        this.props.navigation.navigate('ForgotPassword')
    }

    render() {
        return (
            <View style={{flex: 1,}}>
                <Image source={require('../Asset/main_bg2.png')}
                       style={{flex: 1, position: 'absolute', resizeMode: 'repeat'}}/>
                <KeyboardAvoidingView style={{flex: 1}} behavior="padding" enabled={true}>
                    <View style={styles.container}>
                        <View style={styles.logoContainer}>
                            <Image source={require('../Asset/login_logo.png')} style={styles.backgroundImage}/>
                            <View style={styles.textContainer}>
                                <Text style={{alignSelf: "center"}}>Login to your account</Text>
                            </View>
                        </View>
                        <View style={styles.subContainer}>
                            <View style={styles.inputContainer}>
                                <TextInput onChangeText={(email) => this.setState({email})}
                                           value={this.state.email}
                                           placeholder="email"
                                           autoCapitalize='none'
                                           keyboardType={'email-address'}
                                           style={[TextStyles.inputstyle]}/>
                                <TextInput onChangeText={(password) => this.setState({password})}
                                           placeholder="password"
                                           value={this.state.password}
                                           autoCapitalize='none'
                                           secureTextEntry={true}
                                           style={TextStyles.inputstyle}/>
                            </View>
                            <View>
                                <TouchableOpacity style={[{marginBottom: dimen.app_padding}, TextStyles.buttonStyle]}
                                                  onPress={this.loginPressed.bind(this)}>
                                    <Text style={[TextStyles.buttonTextStyle]}>Log In</Text>
                                </TouchableOpacity>
                                <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                                    <View style={styles.loginAgentContainer}>
                                        <TouchableOpacity onPress={this.loginAgentWeb}>
                                            <AppText>
                                                Login as Agent
                                            </AppText>
                                        </TouchableOpacity>
                                    </View>
                                    <TouchableOpacity style={styles.forgetPasswordContainer}
                                                      onPress={this.forgetPassword}>
                                        <Text style={styles.forgetPasswordText}>
                                            Forget password?
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </View>
                    {this.state.isLoading && <Loader/>}
                </KeyboardAvoidingView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: dimen.app_padding,
        flexDirection: "column",
        justifyContent: 'space-evenly',
        alignItems: 'stretch'
    },
    logoContainer: {flex: 1, justifyContent: 'center', alignItems: 'stretch'},
    backgroundImage: {width: 200, height: 106, alignSelf: 'center', resizeMode: "contain",},
    textContainer: {marginTop: dimen.app_padding * 2},
    subContainer: {
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'stretch'
    },
    inputContainer: {marginTop: dimen.app_padding},
    loginAgentContainer: {
        flexDirection: 'row', alignSelf: 'flex-start', margin: 10,
        justifyContent: 'flex-start', alignItems: 'center'
    },
    forgetPasswordText:{
        color: Colors.normal_text_color,
        fontSize: dimen.textinputfontsize
    },
    forgetPasswordContainer:{color: Colors.normal_text_color, alignSelf: 'center'},


})


//checkbox
//     <View style={{marginRight: 10, justifyContent: 'center', alignItems: 'center'}}>
// {!this.state.isChecked &&
// <TouchableOpacity onPress={this.handleCheckBox}>
//     <View style={{
//         width: 20,
//         height: 20,
//         justifyContent: 'center',
//         alignItems: 'center',
//         borderWidth: 1,
//         borderColor: Colors.normal_text_color
//     }}>
//     </View>
// </TouchableOpacity>
// }
// {this.state.isChecked &&
// <TouchableOpacity onPress={this.handleCheckBox}>
//     <View style={{
//         width: 20,
//         height: 20,
//         justifyContent: 'center',
//         alignItems: 'center',
//         backgroundColor: Colors.primary_color
//     }}>
//         <Image source={require('../Asset/tick.png')}
//                style={{
//                    width: 11,
//                    height: 11,
//                    alignSelf: 'center',
//                    resizeMode: 'contain'
//                }}/>
//     </View>
// </TouchableOpacity>
// }
//
// </View>
