import React from "react";
import {
    View,
    Text,
    TextInput,
    Picker,
    Button,
    StyleSheet,
    ScrollView,
    Alert,
    TouchableOpacity,
    Image,
    CheckBox,
    ActionSheetIOS,
    TouchableHighlight, KeyboardAvoidingView, Platform, NetInfo, RefreshControl
} from "react-native";
import {FlatList} from "react-native-gesture-handler";
import Colors from "../Styles/Colors";
import dimen from "../Styles/Dimen";
import AppText from "../Components/AppText";
import {Loader} from "../Components/Loader";
import {
    HomeHeaderQiblaMain,
    AllTripsHeaderButton,
    LogoTitle
} from "../Components/HeaderComponents";
import TextStyles from "../Styles/TextStyles";
import {
    errorAlert,
    getData,
    getSavedObject,
    getUserObject,
    isNetConnected,
    removeMainObject,
    saveMainObject, simpleAlert, successAlert
} from "../Classes/auth";
import {
    general_network_error,
    get_all_msg_url, internetError, message_key, notification_key,
    save_alert_url,
    save_feedback_url,
    send_msg_url,
    single_trip_url,
    trip_key
} from "../Classes/UrlConstant";
import {MessageCard} from "../Components/MessageCard";
import {MyContext} from "../Components/Consumer";

export class Message extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isSplash: true,
            tripsteps: [],
            tripdata: {},
            userdata: {},
            isfeedback: false,
            ismessage: true,
            feedback_categories: [],
            isqibla: false,
            feedback_text: '',
            isLoading: false,
            isLoadingMessage: false,
            selected_categories: 'Select Category',
            selected_feedback_android: '',
            selectedfeedback_ios: 'none',
            feedback_categories_titles_ios: [],


            messageBody: '',
            allmessages: []

        };
    }

    static contextType = MyContext;

    static navigationOptions = ({navigation}) => {
        return {
            headerLeftContainerStyle: {},
            // headerTitle: <LogoTitle/>,

            headerStyle: {
                backgroundColor: Colors.primary_color,
            },
            headerTitleStyle: {
                justifyContent: 'center',
                alignItems: 'center',
            },
            headerTitleContainerStyle: {
                left: 120,
                right: 120,
                justifyContent: 'center',
                alignItems: 'center'
            },

            headerRight: (navigation.state.isqibla ?
                    <HomeHeaderQiblaMain onPressItem={() => navigation.navigate('UserStack')}/> : null
            ),
            //hide backtitle and show custom button
            headerBackTitleVisible: false,

            headerLeft: (
                <AllTripsHeaderButton onPressItem={() => navigation.navigate('AllTripsStack')}/>
            ),
        }

    };

    componentWillMount() {
        this.loadTripData()
    }

    componentDidUpdate() {
        if (this.context.val_trip_id === this.state.tripdata.trip_id && this.context.val_message === 'message') {
            this.getAllMesssages();
            this.context.val_message = '';
            removeMainObject(notification_key)
        }
    }

    loadTripData() {
        getSavedObject(trip_key)
            .then((data) => {
                var jsondata = JSON.parse(data);
                var newArray = [{
                    "id": 0,
                    "title": "Select Category",
                }]
                for (var i = 0; i < jsondata.feedback_categories.length; i++)
                    newArray.push(jsondata.feedback_categories[i]);

                this.setState({
                    tripdata: jsondata,
                    feedback_categories: newArray, //jsondata.feedback_categories,
                    selected_feedback_android: newArray[0].id,

                    feedback_categories_titles_ios: newArray.map((s, i) => {
                        return s.title
                    }),
                    selectedfeedback_ios: (jsondata.feedback_categories.map((s, i) => {
                        return s.title
                    }))[0]
                }, function () {
                    this.getAllMesssages()
                })
            }).catch(() => {
        })


        getUserObject().then((user) => {
            this.setState({
                userdata: JSON.parse(user),
            })
        })
    }

    updateState = (data) => {
        this.setState({
            tripdata: JSON.parse(data),
            tripsteps: JSON.parse(data).current_step.step.step_section,
            tripsection: JSON.parse(data).current_step.step,
            trip_updates: JSON.parse(data).trip_updates == null ? [] : JSON.parse(data).trip_updates,
        })
    }
    refreshTripData = () => {

        this.setState({isLoading: true})
        getData(single_trip_url + this.state.tripdata.pivot.id, 'GET', '')
            .then((res) => {
                // console.log("current trip: " +JSON.stringify(res))
                this.setState({isLoading: false})
                this.updateState(JSON.stringify(res))
                saveMainObject(trip_key, JSON.stringify(res))
                    .then((res) => {
                    })
                    .catch((res) => {
                    })

            })
            .catch(err => {
                this.setState({isLoading: false})
                console.log("error fetching data : " + err)
            });

    }

    feedbackPress() {
        this.setState({
            isfeedback: true,
            ismessage: false,
        })

    }

    messagePress() {
        this.setState({
            isfeedback: false,
            ismessage: true,
        })
        this.getAllMesssages()

    }

    sendfeedback() {
        var end_date = new Date(this.state.tripdata.trip.end_date);
        // console.log(this.state.tripdata.trip.end_date)
        var date = new Date();
        if (date > end_date) {
            simpleAlert('Trip has expired. You cannot send feedback for an expired trip.')
            return;
        }

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected) {
                if (Platform.OS === 'ios') {
                    if (this.state.feedback_text == '') {
                        simpleAlert("Feedback text should not be empty.")
                        return;
                    } else if ((this.state.selectedfeedback_ios) === 'Select Category') {
                        simpleAlert("Select a category to send feedback.")
                        return;
                    }
                } else {
                    if (this.state.feedback_text == '') {
                        simpleAlert("Feedback text should not be empty.")
                        return;
                    } else if (parseInt(this.state.selected_feedback_android) === 0) {
                        simpleAlert("Select a category to send feedback.")
                        return;
                    }
                }
                this.setState({isLoading: true})
                let formdata = new FormData();

                if (Platform.OS === 'ios') {
                    console.log(this.state.feedback_categories)
                    for (var i = 0; i < this.state.feedback_categories.length; i++) {
                        if (this.state.feedback_categories[i].title === this.state.selectedfeedback_ios) {

                            if (parseInt(this.state.feedback_categories[i]) === 0) {
                                simpleAlert("Select a category to send feedback.")
                                return;
                            }

                            formdata.append("feedback_category_id", parseInt(this.state.feedback_categories[i].id))
                            break;
                        }
                    }
                } else {
                    formdata.append("feedback_category_id", parseInt(this.state.selected_feedback_android))
                }

                formdata.append("feedback", this.state.feedback_text)
                getData(save_feedback_url + this.state.tripdata.pivot.id, 'POST', formdata)
                    .then((res) => {
                        this.setState({isLoading: false});
                        if (res.status) {
                            successAlert("Feedback sent successfully.")
                            this.setState({feedback_text: ''})
                        } else
                            errorAlert("Error while sending feedback")//+ (res.message));
                    })
                    .catch((err) => {
                        this.setState({isLoading: false});
                        errorAlert(general_network_error)


                    })

            } else {
                errorAlert(internetError)

            }
        })
    }

    sendMessage() {
        var end_date = new Date(this.state.tripdata.trip.end_date);
        // console.log(this.state.tripdata.trip.end_date)
        var date = new Date();
        if (date > end_date) {
            simpleAlert('Trip has expired. You cannot send message for an expired trip.')
            return;
        }

        isNetConnected().then(() => {
            if (this.state.messageBody == '') {
                return;
            }
            this.setState({isLoading: true})
            let formdata = new FormData();
            formdata.append("message", this.state.messageBody)
            formdata.append("trip_id", this.state.tripdata.trip_id)

            getData(send_msg_url, 'POST', formdata)
                .then((res) => {
                    this.setState({isLoading: false});
                    if (res.status) {
                        this.setState({messageBody: ''}, function () {
                            this.getAllMesssages()
                        })
                    } else
                        errorAlert((res.message));
                })
                .catch((err) => {
                    this.setState({isLoading: false});
                    alert(general_network_error)
                })

        }).catch(() => {
            errorAlert(internetError)
        })


    }

    getAllMesssages() {
        isNetConnected().then(() => {
            this.setState({isLoading: false, isLoadingMessage: true});
            getData(get_all_msg_url + this.state.tripdata.trip_id, 'GET', '')
                .then((res) => {
                    this.setState({isLoading: false, isLoadingMessage: false, allmessages: res},
                        function () {
                            saveMainObject(message_key, JSON.stringify(res))
                                .then((res) => {
                                })
                                .catch((err) => {
                                    console.error('error saving messeges.' + err)
                                })
                        });

                    // console.log(this.context.val_trip_id)
                    // console.log(this.state.tripdata.trip_id)
                    if (this.context.val_trip_id == this.state.tripdata.trip_id) {
                        this.context.val_message = '';
                        removeMainObject(notification_key)
                    }

                })
                .catch((err) => {
                    this.setState({isLoading: false, isLoadingMessage: false});
                    console.log("Error: " + err)
                    errorAlert(general_network_error)
                })

        }).catch((error) => {

            errorAlert(internetError)
            console.log(error);
            getSavedObject(message_key)
                .then((data) => {
                    this.setState({
                        allmessages: JSON.parse(data),
                    }, function () {
                    })
                })
                .catch(() => {
                })
        })
    }

    showActionSheet(feedbackitems) {
        ActionSheetIOS.showActionSheetWithOptions({
                options: this.state.feedback_categories_titles_ios,
                // cancelButtonIndex: CANCEL_INDEX,
                // destructiveButtonIndex: this.state.feedback_categories_titles_ios!==undefined? this.state.feedback_categories_titles_ios.length-1:0,
            },
            (buttonIndex) => {
                console.log(buttonIndex);
                this.setState({
                    selectedfeedback_ios: this.state.feedback_categories_titles_ios[buttonIndex]
                }, function () {
                    console.log(this.state.selectedfeedback_ios);
                });
            });
    }

    scrollToBottom = () => {
        setTimeout(() => {
            this.refs.myView.scrollToEnd({animated: false})
        }, 1000)

    }

    render() {
        let feedbackitems = this.state.feedback_categories.map((s, i) => {
            return <Picker.Item key={i} value={s.id.toString()} label={s.title}/>
        });
        let feedbackitemsios = this.state.feedback_categories.map((s, i) => {
            return s.title
        });
        return (
            <View style={styles.mainContainer}>
                <Image source={require('../Asset/bg.png')} style={styles.backgroundImage}/>
                <View style={styles.container}>
                    <View style={styles.buttonsContainer}>
                        <TouchableOpacity style={[styles.feedbackButtonContainer,
                            this.state.isfeedback ? {backgroundColor: Colors.primary_color} : {backgroundColor: Colors.home_widget_background_color}
                        ]}
                                          onPress={this.feedbackPress.bind(this)}>
                            <AppText style={styles.feedbackButtonText}>
                                Feedback
                            </AppText>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.feedbackButtonContainer,
                            this.state.ismessage ? {backgroundColor: Colors.primary_color} : {backgroundColor: Colors.home_widget_background_color}
                        ]}
                                          onPress={this.messagePress.bind(this)}>
                            <AppText style={styles.feedbackButtonText}>
                                Message
                            </AppText>
                        </TouchableOpacity>
                    </View>

                    {this.state.isfeedback &&
                    <View style={styles.feedbackContainer}>
                        <View>
                            <AppText style={styles.feedbackTitleText}>
                                Please provide your feedback.
                            </AppText>
                            {Platform.OS === 'ios' ?
                                <TouchableOpacity onPress={this.showActionSheet.bind(this)}>
                                    <View style={[TextStyles.inputstyle, styles.drowpdownIosContainer]}>
                                        <Image source={require('../Asset/drop_down.png')}
                                               style={styles.dropDownImageIos}/>
                                        <AppText>{this.state.selectedfeedback_ios}</AppText>
                                    </View>
                                </TouchableOpacity>
                                :
                                <View style={styles.feedbackAndroidDropdownContainer}>
                                    <Picker
                                        style={{backgroundColor: Colors.home_widget_background_color,}}
                                        onValueChange={(feedback) => {
                                            this.setState({selected_feedback_android: feedback});
                                        }}
                                        selectedValue={this.state.selected_feedback_android}
                                    >
                                        {feedbackitems}
                                    </Picker>
                                    <View style={styles.downArrowImageContainer}>
                                        <Image source={require('../Asset/drop_down.png')}
                                               style={styles.downArrowImageStyle}/>
                                    </View>
                                </View>
                            }

                            <View style={styles.feedbackInputContainer}>
                                <TextInput multiline={true}
                                           numberOfLines={3}
                                           placeholder="Type your feedback here..."
                                           returnKeyType="done"
                                           blurOnSubmit={true}
                                           style={styles.feedbackInputFieldStyle}
                                           onChangeText={(feedback_text) => this.setState({feedback_text})}
                                           value={this.state.feedback_text}
                                >
                                </TextInput>
                            </View>
                        </View>

                        <View>
                            <TouchableHighlight
                                onPress={this.sendfeedback.bind(this)}
                                style={[styles.feedbackSubmitButton, TextStyles.buttonStyle]}>
                                <Text style={[TextStyles.buttonTextStyle]}>Submit</Text>
                            </TouchableHighlight>
                        </View>
                    </View>}

                    {this.state.ismessage &&
                    <View style={styles.messageContainer}>
                        <ScrollView
                            ref={ref => this.scrollView = ref}
                            onContentSizeChange={(contentWidth, contentHeight) => {
                                this.scrollView.scrollToEnd({animated: true});
                            }}
                            style={styles.scrollViewStyle}
                            contentContainerStyle={styles.scrollViewContainerStyle}>
                            <View style={{marginBottom: 2}}>
                                <FlatList
                                    style={{}}
                                    data={this.state.allmessages}
                                    keyExtractor={(item, index) => item.id.toString()}
                                    numColumns={1}
                                    bounces={false}
                                    inverted={true}
                                    renderItem={({item}) => (
                                        <MessageCard item={item}/>
                                    )}
                                    // refreshControl={
                                    //     <RefreshControl
                                    //         refreshing={this.state.isLoadingMessage}
                                    //         onRefresh={this.getAllMesssages}
                                    //     />
                                    // }
                                />
                            </View>
                        </ScrollView>
                        <KeyboardAvoidingView
                            behavior={Platform.OS === "ios" ? "padding" : "padding"}
                            keyboardVerticalOffset={Platform.OS === "ios" ? 130 : 0}
                            // behavior="padding"
                            enabled={true}>

                            <View style={styles.messageTextBoxContainer}>
                                <TextInput
                                    style={styles.messageInputField}
                                    multiline={true}
                                    numberOfLines={1}
                                    placeholder="Type your message here..."
                                    returnKeyType="done"
                                    blurOnSubmit={true}
                                    onChangeText={(messageBody) => this.setState({messageBody})}
                                    value={this.state.messageBody}/>
                                <TouchableOpacity onPress={this.sendMessage.bind(this)}>
                                    <Image source={require('../Asset/sendmsg.png')}
                                           style={styles.sendMessageImageStyle}/>
                                </TouchableOpacity>
                            </View>
                        </KeyboardAvoidingView>
                    </View>}
                </View>
                {this.state.isLoading && <Loader/>}
            </View>
        )
    }
}


const styles = StyleSheet.create({
    mainContainer: {flex: 1, justifyContent: "flex-start", alignItems: "stretch"},
    backgroundImage: {
        bottom: 0,
        left: 0,
        right: 0,
        top: 0,
        width: '100%',
        height: '100%',
        position: 'absolute',
        resizeMode: 'cover'
    },
    container: {flex: 1, justifyContent: 'flex-start'},
    buttonsContainer: {
        borderWidth: 1,
        borderColor: Colors.border_color,
        borderRadius: dimen.border_radius,
        overflow: 'hidden',
        backgroundColor: Colors.home_widget_background_color,
        margin: dimen.app_padding,
        marginBottom: 5,
        justifyContent: 'space-evenly',
        alignItems: 'flex-start',
        flexDirection: 'row',

    },
    feedbackButtonText: {fontFamily: 'roboto-bold', fontSize: 16},
    feedbackButtonContainer: {flex: 1, padding: 8, justifyContent: 'center', alignItems: 'center'},
    feedbackContainer: {
        margin: dimen.app_padding, marginBottom: 70,
        flex: 1, justifyContent: 'space-between', alignItems: 'stretch',
    },
    feedbackTitleText: {fontFamily: 'roboto-bold', fontSize: 16},
    feedbackAndroidDropdownContainer: {
        borderWidth: 1,
        borderColor: Colors.border_color,
        borderRadius: dimen.border_radius,
        overflow: 'hidden',
        backgroundColor: Colors.home_widget_background_color,
        marginTop: dimen.app_padding
    },
    drowpdownIosContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        margin: 0,
    },
    downArrowImageContainer: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 10,
        bottom: 0,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    downArrowImageStyle: {width: 14, height: 14, resizeMode: 'contain'},
    dropDownIosContainer: {
        marginLeft: 0,
        marginRight: 0,
        marginTop: dimen.app_padding,
    },
    dropDownImageIos: {
        position: 'absolute',
        right: 10,
        width: 15,
        height: 15,
        resizeMode: 'contain',
    },
    dropImageIos: {width: 14, height: 14, resizeMode: 'contain'},
    feedbackInputContainer: {
        borderWidth: 1,
        borderColor: Colors.border_color,
        borderRadius: dimen.border_radius,
        overflow: 'hidden',
        backgroundColor: Colors.home_widget_background_color,
        marginTop: dimen.app_padding,
    },
    feedbackInputFieldStyle: {
        textAlignVertical: 'top',
        height: 80,
        color: Colors.normal_text_color,
        fontSize: 14,
        margin: dimen.app_padding
        // flex:1, justifyContent:'flex-start',alignItems:'flex-start'
    },
    feedbackSubmitButton: {marginBottom: 0, marginLeft: 0, marginRight: 0},

    messageContainer: {
        marginBottom: 60,
        flex: 1, justifyContent: 'flex-end', alignItems: 'stretch',
    },
    scrollViewStyle: {padding: dimen.app_padding},
    scrollViewContainerStyle: {flex: 1, justifyContent: 'flex-end', alignItems: 'stretch'},
    messageTextBoxContainer: {
        flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center',
        marginBottom: dimen.app_padding, marginLeft: dimen.app_padding, marginRight: dimen.app_padding,
        backgroundColor: Colors.home_widget_background_color,
        padding: 10,
        borderRadius: dimen.border_radius, borderWidth: 1, borderColor: Colors.border_color
    },
    messageInputField: {flex: 10, alignSelf: 'center',},
    sendMessageImageStyle: {
        flex: 1,
        marginLeft: 7,
        width: 18,
        height: 18,
        alignSelf: 'center',
        resizeMode: 'contain'
    },


})
