import React from "react";
import {ActivityIndicator, Alert, Image, Platform, StyleSheet, View} from "react-native";
import Colors from "../Styles/Colors";
import {HomeHeaderSignout, AllTripsHeaderButton, LogoTitle, HomeHeaderReload} from "../Components/HeaderComponents";
import {
    errorAlert,
    getData,
    getSavedObject,
    getUserObject, isSignedIn,
    onSignInUser,
    onSignOutUser,
    saveMainObject, USER
} from "../Classes/auth";
import {admin_dashboard_url, fcm_web_route, get_current_trip_url, logout_url, trip_key} from "../Classes/UrlConstant";
import BackHeaderButton from "../Components/HeaderComponents";
import * as Permissions from "expo-permissions";
import {Notifications} from "expo";
import BackHeaderButtonAdmin from "../Components/HeaderComponents";
import { WebView } from 'react-native-webview';


async function registerForPushNotificationsAsync() {

    const {status: existingStatus} = await Permissions.getAsync(
        Permissions.NOTIFICATIONS
    );
    let finalStatus = existingStatus;
    // console.log("final status:"+ finalStatus)


    // only ask if permissions have not already been determined, because
    // iOS won't necessarily prompt the user a second time.
    if (existingStatus !== 'granted') {

        // Android remote notification permissions are granted during the app
        // install, so this will only ask on iOS
        const {status} = await Permissions.askAsync(Permissions.NOTIFICATIONS);
        finalStatus = status;
        console.log("final status:" + finalStatus)

    }

    if (Platform.OS === 'android') {
        await Notifications.createChannelAndroidAsync('message', {
            name: 'message',
            sound: true,
            priority: 'high',
            vibrate: [0, 250, 250, 250],
        });
    }


    // Stop here if the user did not grant permissions
    if (finalStatus !== 'granted') {
        // console.log("final status:"+finalStatus)
        return;
    }

    // Get the token that uniquely identifies this device
    let token = await Notifications.getExpoPushTokenAsync();
    return token;
}

export class AdminHome extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            userdata: {},
            isloadingLoader: false,
            url: admin_dashboard_url,
            fcm: ''

        };
    }
    static navigationOptions = ({navigation}) => {
        return {
            title: 'Settings',
            // headerLayoutPreset:'left',

            headerLeftContainerStyle: {},
            headerTitle: <LogoTitle/>,

            headerStyle: {
                backgroundColor: Colors.primary_color,
                // justifyContent: 'center'

            },
            // headerTintColor: '#fff',
            headerTitleStyle: {
                justifyContent: 'center',
                alignItems: 'center',
                // fontWeight: 'bold',
            },
            headerTitleContainerStyle: {
                left: 120,
                right: 120,
                justifyContent: 'center',
                alignItems: 'center'
            },

            headerRight: (
                <HomeHeaderReload  onPressItem={navigation.getParam('reloadPage')}/>
            ),
            //hide backtitle and show custom button
            headerBackTitleVisible: false,

            // headerLeft: (
            //     <AllTripsHeaderButton onPressItem={()=> navigation.navigate('AllTripsStack')}/>
            // ),
            // headerLeft: ( navigation.getParam('canGoBack') ?
            //         <BackHeaderButton onPressItem={navigation.getParam('goback')}/> : null
            // ),
            headerLeft: (navigation.getParam('goToAuth') ?
                    <BackHeaderButtonAdmin onPressItem={() => navigation.navigate('Auth')}/> : null
            ),
        }


    };

    registerFcmAdmin = () => {
        registerForPushNotificationsAsync()
            .then(token => {
                this.setState({
                    fcm: token,
                })

                console.log(this.state.fcm);
            })
            .catch(err => console.log("Error: " + err))
    }

    componentDidMount() {
        this.props.navigation.setParams({
            logoutUser: this.logoutUser,
            reloadPage: this.reloadPage,
            goToAuthFun: this.goToAuthFun,
            goback: this.gobackWebview,
            goToAuth: false
        });
        this.registerFcmAdmin()
        this.updateGoToAuth();
        // console.log(this.props.navigation)

    }

    goToAuthFun = () => {
        this.props.navigation.navigate('AuthStack');
        console.log('auth function')
    }

    gobackWebview = () => {
        this.refs.WEBVIEW_REF.goBack();
        console.log('goback')
    }

    reloadPage= () => {
        this.refs.WEBVIEW_REF.reload();

    }
    logoutUser = () => {
        this.setState({isloadingLoader: true})
        getData(logout_url, 'POST', '')
            .then(res => {
                if (res.status) {
                    this.setState({
                        isloadingLoader: false,
                    }, function () {
                        onSignOutUser()
                        this.props.navigation.navigate('Auth')

                    });
                }
                else {
                    this.setState({
                        isloadingLoader: false
                    })
                    onSignOutUser()
                    this.props.navigation.navigate('Auth')
                }
            })
            .catch(err => {
                this.setState({
                    isLoading: false
                })
                errorAlert("Error logging out");

            })
    }

    loadTripData() {
        getSavedObject(trip_key)
            .then((data) => {
                this.setState({
                    tripdata: JSON.parse(data),
                })
            }).catch(() => {
        })

        getUserObject().then((user) => {
            this.setState({
                userdata: JSON.parse(user),
            })
        })
    }

    updateGoToAuth = () => {
        isSignedIn()
            .then((r) => {
                if (r) {// if it is logged in
                    getUserObject().then((user) => {
                        console.log(user);
                        if (JSON.parse(user).user_type == 'agent' || JSON.parse(user).user_type == 'staff') {
                            this.props.navigation.setParams({goToAuth: false});
                        }
                        else {
                            this.props.navigation.setParams({goToAuth: true});
                        }
                    })
                }
                else { // if it is logged out
                    console.log('logout');
                    this.props.navigation.setParams({goToAuth: true});
                }
            })
            .catch((e) => {
                console.log('error');
                this.props.navigation.setParams({goToAuth: true});
            })

        // getSavedObject(USER)
        //     .then((data) => {
        //         console.log(data);
        //         if (data) {
        //             if (JSON.parse(user).user_type === "agent") {
        //                 this.props.navigation.setParams({goToAuth: false});
        //             } else {
        //                 this.props.navigation.setParams({goToAuth: true});
        //             }
        //         } else {
        //             this.props.navigation.setParams({goToAuth: true});
        //         }
        //     })
        //     .catch((err) => {
        //         console.log(err);
        //         this.props.navigation.setParams({goToAuth :  true});
        //     })

    }

    onNavigationStateChange(navState) {
        this.props.navigation.setParams({canGoBack: navState.canGoBack});
        console.log(navState.url);

        // console.log('fcm out'+ this.state.fcm);

        if (navState.url === 'http://ahlanguide.com/admin/dashboard' && this.state.fcm !== '') {
            // console.log('fcm inside '+ this.state.fcm);
            this.setState({
                url: fcm_web_route + this.state.fcm
            }, function () {
                onSignInUser(JSON.stringify({"user_type": "agent"}))
                this.updateGoToAuth()
                // console.log('log in successfull')

            })
        }
        if (navState.url === 'http://ahlanguide.com/login') {
            onSignOutUser()
            this.updateGoToAuth()
            // console.log('logged in url, we logout user')
        }
        if (navState.url === 'http://ahlanguide.com/') {
            onSignOutUser()
            this.updateGoToAuth()
            // console.log('loggout user, we logout user')
        }
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <Image source={require('../Asset/bg.png')}
                       style={{flex: 1, position: 'absolute', resizeMode: 'repeat'}}/>

                <View style={{flex: 1}}>
                    <WebView style={{flex: 1}}
                             ref='WEBVIEW_REF'
                             onNavigationStateChange={this.onNavigationStateChange.bind(this)}
                             allowUniversalAccessFromFileURLs={true}
                             allowFileAccess={true}
                             bounces={false}
                        // source={{ uri: admin_dashboard_url, body: 'email=nabeel123456@gmail.com&password=ahlanguide123' , method:'POST' }}
                             source={{uri: this.state.url}}
                             // scalesPageToFit={true}
                             onLoad={() => {
                                 this.setState({isloadingLoader: true})
                             }}
                             onLoadStart={() => {
                                 this.setState({isloadingLoader: true})
                             }}
                             onLoadEnd={() => {
                                 this.setState({isloadingLoader: false})
                             }}
                    />
                    {this.state.isloadingLoader && <Loader/>}
                </View>

            </View>
        )

    }
}

const styles = StyleSheet.create({
    icon: {
        width: 24,
        height: 24,

    },
});

export const Loader = () => (
    <View style={{
        justifyContent: 'center', alignItems: 'center', position: 'absolute',
        left: 0,
        right: 0,
        // opacity: 0.4,
        // backgroundColor: 'black',
        top: 0,
        bottom: 0,
    }}>
        <Image source={require('../Asset/bg2.png')}
               style={{flex: 1, position: 'absolute', resizeMode: 'repeat'}}/>
        <ActivityIndicator size="large" color={Colors.primary_color}/>
    </View>
)

