import React, {Component} from 'react';
import {View, StyleSheet, Image, Animated, Alert, Easing, Dimensions, Platform, BackHandler} from 'react-native';
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions'
import Colors from "../Styles/Colors";
import BackHeaderButton from "../Components/HeaderComponents";
import {gpsError, permissionError} from "../Classes/UrlConstant";
import {errorAlert, getSavedObject} from "../Classes/auth";
import AppText from "../Components/AppText";
import dimen from "../Styles/Dimen";
import {Magnetometer} from 'expo-sensors';

export default class Compass extends Component {
    static navigationOptions = ({navigation}) => {
        return {
            title: 'Qibla Direction',
            headerStyle: {
                backgroundColor: Colors.primary_color,
                tintColor: Colors.normal_text_color,
            },

            headerLayoutPreset: 'left',
            headerTitleStyle: {
                justifyContent: 'flex-start',
                fontWeight: 'bold',
                marginLeft: 3
            },
            headerTitleContainerStyle: {
                left: 45,
            },
            headerLeftContainerStyle: {},
            tabBarVisible: false,
            headerBackTitleVisible: false,
            headerLeft: (
                <BackHeaderButton onPressItem={navigation.getParam('goback')}/>
            ),
        }
    };

    constructor() {
        super();
        this.spinValue = new Animated.Value(0);
        this.spinValue2 = new Animated.Value(0);
        this.locationSub = null;
        this.state = {
            location: null,
            lat: 0.0,
            lon: 0.0,
            errorMessage: null,
            heading: null,
            truenoth: null,
            qiblaDegree: 0,
            qiblashow: false,
            sub: null,
            errorString: '',
            errorVisible: false,
            hideBottomTab: false,

        };
        this.onBackClicked = this._onBackClicked.bind(this);
    }

    _onBackClicked = () => {
        this.goback()
        return true;
    }

    componentWillMount() {
        if (Platform.OS === 'android') {
            BackHandler.addEventListener('hardwareBackPress', this.onBackClicked);
        }
        getSavedObject('hideBottomTab')
            .then(res => {
                this.setState({hideBottomTab: JSON.parse(res)})
            })
            .catch(err => console.log(err))
    }

    componentWillUnmount() {
        if (Platform.OS === 'android') {
            BackHandler.removeEventListener("hardwareBackPress", this.onBackClicked);
        }
    }

    componentDidMount() {
        this._getLocationAsync();
        this.props.navigation.setParams({goback: this.goback});
    }

    componentWillUpdate() {
        this.spin()
    }

    checkForCompass = () => {
        Permissions.askAsync(Permissions.LOCATION)
            .then((res) => {
                // console.log(res)
                if (res.status === 'granted') {
                    this.setState({permissionAvailable: false});
                    Location.getProviderStatusAsync()
                        .then((loc) => {
                            if (loc.gpsAvailable) {
                                this.setState({permissionAvailable: false});
                                Location.getCurrentPositionAsync({})
                                    .then((locationObject) => {
                                        this.setState({
                                            location: locationObject,
                                            lat: locationObject.coords.latitude,
                                            lon: locationObject.coords.longitude
                                        }, function () {
                                            this.sendAlert();
                                        });
                                    })
                                    .catch((locationError) => {
                                    })
                            } else {
                                this.setState({
                                    permissionAvailable: true,
                                    permissionError: gpsError
                                });
                                errorAlert('Enable location to send alerts.')
                            }
                        })
                        .catch((err) => console.log(err))
                } else {
                    this.setState({
                        permissionAvailable: true,
                        permissionError: permissionError
                    });
                }
            })
            .catch(() => {
            })
    }

    goback = () => {
        if (this.locationSub != null) {
            this.locationSub.remove()
        }

        if (this.state.hideBottomTab) {
            this.props.navigation.navigate('CurrentTripsStack');
        } else {
            this.props.navigation.navigate('AppHome');
        }
    }

    _getLocationAsync = async () => {

        Magnetometer.isAvailableAsync().then((sensor) => {
            if (sensor) {
                Permissions.askAsync(Permissions.LOCATION)
                    .then((res) => {
                        if (res.status !== 'granted') {
                            this.setState({
                                errorString: 'Allow location permission.',
                                errorVisible: true,
                            });
                        } else {
                            this.watchHeading()
                        }
                    })
                    .catch((err) => {

                    })


            } else {
                this.setState({
                    errorString: 'Phone does not support this feature.',
                    errorVisible: true,
                });
                return;
            }
        }).catch((err) => {
            return;
        })
        // let {status} = await Permissions.askAsync(Permissions.LOCATION);
        // if (status !== 'granted') {
        //     console.log('permissionAvailable not granted')
        //     this.setState({
        //         errorString: 'Allow location permission.',
        //         errorVisible:true,
        //     });
        // }
        // else {
        //
        //     try {
        //         this.locationSub = await Location.watchHeadingAsync((obj) => {
        //             let heading = obj.magHeading;
        //             this.setState({heading: heading})
        //         })
        //     } catch (e) {
        //         this.setState({errorMessage: 'Phone not supported',});
        //
        //     }
        //     try {
        //         console.log('before getting current position.')
        //         let location = await Location.getCurrentPositionAsync({})
        //             .catch(() => {
        //                 this.setState({errorMessage: 'Enable location for Qibla Direction.',});
        //             });
        //         this.setState({
        //             location: location,
        //             lat: location.coords.latitude,
        //             lon: location.coords.longitude
        //         });
        //         let ss = this.calculateQibla2(21.422487, 39.826206)
        //
        //         if (ss < 0)
        //             ss += 360
        //         if (ss > 360)
        //             ss -= 360
        //
        //         this.setState({qiblaDegree: ss, qiblashow: true})
        //     } catch (e) {
        //         console.log(e);
        //         this.setState({errorMessage: 'Enable location for Qibla Direction.',});
        //     }
        // }
    };

    watchHeading = async () => {
        try {
            this.locationSub = await Location.watchHeadingAsync((obj) => {
                let heading = obj.magHeading;
                this.setState({heading: heading})
            })
        } catch (e) {
            // this.setState({errorMessage: 'Phone not supported',});
        }
        try {
            console.log('before getting current position.')
            let location = await Location.getCurrentPositionAsync({})
                .catch(() => {
                    this.setState({errorString: 'Enable location for Qibla Direction.',});
                });
            this.setState({
                location: location,
                lat: location.coords.latitude,
                lon: location.coords.longitude
            });
            let ss = this.calculateQibla2(21.422487, 39.826206)

            if (ss < 0)
                ss += 360
            if (ss > 360)
                ss -= 360

            this.setState({qiblaDegree: ss, qiblashow: true})
        } catch (e) {
            console.log(e);
            this.setState({errorMessage: 'Enable location for Qibla Direction.',});
        }
    }

    calculateQibla(latitude, longitude) {
        let phiK = latitude * Math.PI / 180.0;
        let lambdaK = latitude * Math.PI / 180.0;
        let phi = latitude * Math.PI / 180.0;
        let lambda = longitude * Math.PI / 180.0;
        let psi = 180.0 / Math.PI * Math.atan2(Math.sin(lambdaK - lambda), Math.cos(phi) * Math.tan(phiK) - Math.sin(phi) * Math.cos(lambdaK - lambda));

        return Math.round(psi);
    }

    degrees_to_radians = (degrees) => {
        var pi = Math.PI;
        return degrees * (pi / 180);
    }
    radians_to_degrees = (radian) => {
        var pi = Math.PI;
        return radian * (180 / pi);
    }

    calculateQibla2(latitude, longitude) {

        var lat1 = this.degrees_to_radians(this.state.lat);
        var lat2 = this.degrees_to_radians(latitude);

        var lon1 = this.degrees_to_radians(this.state.lon);
        var lon2 = this.degrees_to_radians(longitude);


        var y = Math.sin(lon2 - lon1) * Math.cos(lat2);
        var x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2) * Math.cos(lon2 - lon1);
        var brng = this.radians_to_degrees(Math.atan2(y, x));

        return Math.round(brng);
    }

    spin() {
        let start = JSON.stringify(this.spinValue);
        let heading = Math.round(this.state.heading);

        let rot = +start;
        let rotM = rot % 360;

        if (rotM < 180 && (heading > (rotM + 180)))
            rot -= 360;
        if (rotM >= 180 && (heading <= (rotM - 180)))
            rot += 360

        rot += (heading - rotM)

        Animated.timing(
            this.spinValue,
            {
                toValue: rot,
                duration: 300,
                easing: Easing.easeInOut
            }
        ).start()
    }

    spin2() {
        let start = JSON.stringify(this.spinValue2);
        let heading = Math.round(this.state.heading);

        let rot = +start;
        let rotM = rot % 360;

        if (rotM < 180 && (heading > (rotM + 180)))
            rot -= 360;
        if (rotM >= 180 && (heading <= (rotM - 180)))
            rot += 360

        rot += (heading - rotM)

        Animated.timing(
            this.spinValue2,
            {
                toValue: rot,
                duration: 300,
                easing: Easing.easeInOut
            }
        ).start()
    }

    render() {
        let LoadingText = 'Loading...';
        let display = LoadingText;

        if (this.state.errorMessage)
            display = this.state.errorMessage;

        const spin = this.spinValue.interpolate({
            inputRange: [0, 360],
            outputRange: ['-0deg', '-360deg']
        })
        const spin2 = this.spinValue2.interpolate({
            inputRange: [0, 360],
            outputRange: ['-0deg', '-360deg']
        })

        display = Math.round(JSON.stringify(this.spinValue))

        if (display < 0)
            display += 360
        if (display > 360)
            display -= 360

        var radius = compassRadius;
        var center = radius;

        var angle = this.state.qiblaDegree;
        var angleInRadians = this.degrees_to_radians(angle)

        var cos = Math.cos(angleInRadians);
        var sin = Math.sin(angleInRadians);

        let x = 0;
        let y = 100;

        if (angle >= 0 && angle <= 90) {
            x = radius * cos + center - 30;
            y = radius * sin + center - 30;
        } else if (angle > 90 && angle <= 180) {

            x = radius * (cos * (-1)) + center - 30;
            y = radius * sin + center - 30;
            // console.log(angle+'   '+x);
        } else if (angle > 180 && angle <= 270) {
            x = radius * (cos * -1) + center;
            y = radius - (radius * (sin * -1));
        } else if (angle > 270 && angle <= 360) {
            x = radius * (cos * -1);
            y = radius * (sin * -1);
        }

        return (
            <View style={{flex: 1, justifyContent: 'flex-start', alignItems: 'center'}}>
                {this.state.errorVisible &&
                <AppText style={styles.errorTextStyle}>
                    {this.state.errorString}
                </AppText>
                }
                <View style={styles.mainContainer}>
                    <Animated.View style={{
                        position: 'absolute',
                        width: compassWidth,
                        height: compassWidth,
                        transform: [{rotate: spin}],
                    }}>
                        <Image resizeMode='contain' source={require('../Asset/compass.png')}
                               style={styles.compassImageStyle}/>
                        {this.state.qiblashow &&
                        <View style={{width: 30, height: 30, left: y, top: x}}>
                            <Image resizeMode='contain' source={require('../Asset/qibla_compass.png')}
                                   style={styles.qiblaImageStyle}/>
                        </View>
                        }
                    </Animated.View>
                </View>
            </View>
        );
    }

}

// Device dimensions so we can properly center the images set to 'position: absolute'
const deviceWidth = Dimensions.get('window').width
const deviceHeight = Dimensions.get('window').height
const compassWidth = (deviceWidth * 2) / 3;
const compassRadius = compassWidth / 2;

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    compassImageStyle: {
        position: 'absolute',
        width: compassWidth,
        height: compassWidth,
        flex: 1,
        alignSelf: 'center'
    },
    qiblaImageStyle: {width: 30, height: 30, flex: 1},
    errorTextStyle: {
        marginTop: dimen.app_padding,
        color: Colors.primary_dark_color
    },


})
