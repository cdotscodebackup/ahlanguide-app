import React from "react";
import {View, Text, Button, StyleSheet, ScrollView, Alert, TouchableOpacity, Image, CheckBox} from "react-native";
import {FlatList} from "react-native-gesture-handler";
import Colors from "../Styles/Colors";
import dimen from "../Styles/Dimen";
import AppText from "../Components/AppText";
import BackHeaderButton from "../Components/HeaderComponents";
import {CheckListCard} from "../Components/CheckListCard";
import {getSavedObject} from "../Classes/auth";

export class CheckList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isSplash: true,
            checklist: [],
            hideBottomTab: false,

        };
    }

    static navigationOptions = ({navigation}) => {
        return {
            title: 'Check List',
            headerStyle: {
                backgroundColor: Colors.primary_color,
                tintColor: Colors.normal_text_color,
            },

            headerLayoutPreset: 'left',
            headerTitleStyle: {
                justifyContent: 'flex-start',
                fontWeight: 'bold',
                marginLeft: 3
            },
            headerTitleContainerStyle: {
                left: 45,
            },
            headerLeftContainerStyle: {},
            tabBarVisible: false,
            // headerRight: (
            //   <HeaderComponents onPressItem={
            //       ()=> navigation.navigate('UserStack')
            //   }/>
            // ),

            //hide backtitle and show custom button
            headerBackTitleVisible: false,
            headerLeft: (
                <BackHeaderButton onPressItem={() => navigation.goBack()}/>
            ),
        }
    };

    componentDidMount() {
        getSavedObject('hideBottomTab')
            .then(res => {
                this.setState({hideBottomTab: JSON.parse(res)})
            })
            .catch(err => console.log(err))
    }

    render() {
        const checklist = this.props.navigation.getParam('checklist', []);
        return (
            <View style={{flex: 1, justifyContent: "flex-start", alignItems: "stretch"}}>
                <Image source={require('../Asset/bg.png')}
                       style={{
                           bottom: 0,
                           left: 0,
                           right: 0,
                           top: 0,
                           width: '100%',
                           height: '100%',
                           position: 'absolute',
                           resizeMode: 'cover'
                       }}/>
                <ScrollView style={[{
                    flex: 1, paddingTop: dimen.app_padding, paddingLeft: dimen.app_padding,
                    paddingRight: dimen.app_padding,
                }, this.state.hideBottomTab ? {marginBottom: 10} : {marginBottom: 60}]}
                            bounces={true}>

                    <View style={{
                        flex: 1, padding: dimen.app_padding, marginBottom: 20,
                        borderWidth: 1, borderRadius: 10, borderColor: Colors.border_color,
                        backgroundColor: Colors.home_widget_background_color
                    }}>

                        {checklist.length > 0 &&
                        <FlatList
                            style={{flex: 1}}
                            data={checklist}
                            keyExtractor={(item, index) => item.id.toString()}
                            numColumns={1}
                            renderItem={({item}) => (
                                <CheckListCard item={item}/>
                            )}
                        />}
                        {checklist.length == 0 &&
                        <AppText style={{lineHeight: 28}}>
                            No check list for this section
                        </AppText>
                        }

                    </View>
                </ScrollView>
            </View>

        )
    }
}
