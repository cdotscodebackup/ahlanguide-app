import React from "react";
import {
    View,
    Text,
    Button,
    StyleSheet,
    TextInput,
    Image,
    ScrollView,
    Alert,
    ActivityIndicator,
    Platform
} from "react-native";
import { WebView } from 'react-native-webview';
import Colors from "../Styles/Colors";
import BackHeaderButton from "../Components/HeaderComponents";
import {getSavedObject, getUserObject} from "../Classes/auth";
import {base_guides_url, trip_key} from "../Classes/UrlConstant";

export class ImageScreen extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isSplash: true,
            tripsteps: [],
            tripdata: {},
            userdata: {},
            isloadingLoader: false,
            guide_data: {},
            file_path: '',
            isYoutubelink:false,
            isPdf:false,
            isImage: false,
            youtubelink:'',
        };
    }

    static navigationOptions = ({navigation}) => {
        return {
            title: 'Image',
            headerStyle: {
                backgroundColor: Colors.primary_color,
                tintColor: Colors.normal_text_color,
            },

            headerLayoutPreset: 'left',
            headerTitleStyle: {
                justifyContent: 'flex-start',
                fontWeight: 'bold',
                marginLeft: 3
            },
            headerTitleContainerStyle: {
                left: 45,
            },
            headerLeftContainerStyle: {},
            //hide backtitle and show custom button
            headerBackTitleVisible: false,
            headerLeft: (
                <BackHeaderButton onPressItem={() => navigation.goBack()}/>
            ),
        }
    };

    componentWillMount() {
        this.setState({
            isloadingLoader: false,
        })
    }

    componentDidMount() {
        this.setState({isloadingLoader:false});
        const file_path =this.props.navigation.getParam('path')
        this.setState({file_path})

    }

    render() {
        return (
            <View style={styles.container}>
                <Image source={require('../Asset/bg2.png')}
                       style={styles.backgroundImage}/>

                <View style={styles.mainContainer}>
                    <View style={{flex: 1}}>
                        <WebView style={{flex: 1}}
                                 source={{
                                     html: '<!DOCTYPE html>\n' +
                                         '<html>\n' +
                                         '<head>' +
                                         '<meta name="viewport" content="initial-scale=1, maximum-scale=3.0, minimum-scale=1, user-scalable=yes"/>' +
                                         '</head>' +
                                         '<body>\n' +
                                         '\n' +
                                         '<img  style="width: auto ;\n' +
                                         '  max-width: 100% ;\n' +
                                         '  margin: 0 auto;\n' +
                                         '  height: auto ; "  src="' + this.state.file_path + '"  >\n' +
                                         '\n' +
                                         '</body>\n' +
                                         '</html>'
                                 }}
                                    // scalesPageToFit={true}
                                 onLoad={() => {
                                     this.setState({isloadingLoader: true})
                                 }}
                                 onLoadStart={() => {
                                     this.setState({isloadingLoader: true})
                                 }}
                                 onLoadEnd={() => {
                                     this.setState({isloadingLoader: false})
                                 }}
                        />
                        {this.state.isloadingLoader && <Loader/>}
                    </View>
                </View>

            </View>
        )

    }
}

const styles = StyleSheet.create({
    container: {flex: 1},
    backgroundImage: {flex: 1, position: 'absolute', resizeMode: 'repeat'},
    mainContainer:{flex: 1, marginBottom: 10,},
})

export const Loader = () => (
    <View style={{
        justifyContent: 'center', alignItems: 'center', position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
    }}>
        <Image source={require('../Asset/bg2.png')}
               style={{flex: 1, position: 'absolute', resizeMode: 'repeat'}}/>
        <ActivityIndicator size="large" color={Colors.primary_color}/>
    </View>
)

