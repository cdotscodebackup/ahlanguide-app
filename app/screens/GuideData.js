import React from "react";
import {
    View,
    Text,
    Button,
    StyleSheet,
    TextInput,
    Image,
    ScrollView,
    Alert,
    ActivityIndicator,
    Platform
} from "react-native";
import { WebView } from 'react-native-webview';
import Colors from "../Styles/Colors";
import BackHeaderButton from "../Components/HeaderComponents";
import {getSavedObject, getUserObject} from "../Classes/auth";
import {base_guides_url, trip_key} from "../Classes/UrlConstant";

export class GuideData extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isSplash: true,
            tripsteps: [],
            tripdata: {},
            userdata: {},
            isloadingLoader: false,
            guide_data: {},
            file_path: '',
            isYoutubelink:false,
            isPdf:false,
            isImage: false,
            youtubelink:'',
        };
    }

    static navigationOptions = ({navigation}) => {
        return {
            title: 'Guide',
            headerStyle: {
                backgroundColor: Colors.primary_color,
                tintColor: Colors.normal_text_color,
            },

            headerLayoutPreset: 'left',
            headerTitleStyle: {
                justifyContent: 'flex-start',
                fontWeight: 'bold',
                marginLeft: 3
            },
            headerTitleContainerStyle: {
                left: 45,
            },
            headerLeftContainerStyle: {},
            //hide backtitle and show custom button
            headerBackTitleVisible: false,
            headerLeft: (
                <BackHeaderButton onPressItem={() => navigation.goBack()}/>
            ),
        }
    };

    componentWillMount() {
        this.setState({
            isloadingLoader: false,
            isImage: false,

        })
    }

    componentDidMount() {
        this.setState({isloadingLoader:false});

        const data =this.props.navigation.getParam('guidedata')
        if(data.path===null){
            this.setState({isYoutubelink:true, youtubelink:data.youtube_link})

        }else{
            this.setState({file_path : base_guides_url + data.path})
            // console.log(data.path)

            if(data.path.includes('.pdf')){
                this.setState({isPdf:true})
            }else if (data.path.includes('.png') ||data.path.includes('.jpeg')||data.path.includes('.jpg')){
                this.setState({isImage:true})
            }
        }


    }

    loadTripData() {
        getSavedObject(trip_key)
            .then((data) => {
                this.setState({
                    tripdata: JSON.parse(data),
                }, function () {
                })
            }).catch(() => {
        })

        getUserObject().then((user) => {
            this.setState({
                userdata: JSON.parse(user),
            })
        })
    }


    render() {
        return (
            <View style={styles.container}>
                <Image source={require('../Asset/bg2.png')}
                       style={styles.backgroundImage}/>

                <View style={styles.mainContainer}>
                    {this.state.isPdf &&
                    <View style={{flex: 1,}}>
                        <WebView style={{flex: 1}}
                                 source={{
                                     uri: Platform.OS === 'ios' ? this.state.file_path
                                         // : "https://drive.google.com/gview?embedded=true&url="+ base_guides_url + this.state.file_path
                                         // : "https://drive.google.com/viewerng/viewer?embedded=true&url="+ base_guides_url + this.state.file_path
                                         : "https://drive.google.com/viewerng/viewer?embedded=true&url="+ base_guides_url + this.state.file_path
                                 }}
                                 onLoad={() => {
                                     this.setState({isloadingLoader: true})
                                 }}
                                 onLoadStart={() => {
                                     this.setState({isloadingLoader: true})
                                 }}
                                 onLoadEnd={() => {
                                     this.setState({isloadingLoader: false})
                                 }}
                        />
                        {this.state.isloadingLoader && <Loader/>}
                    </View>
                    }

                    {this.state.isYoutubelink &&
                    <View style={{flex: 1,}}>
                        <WebView style={{flex: 1}}
                                 source={{
                                     uri: this.state.youtubelink
                                 }}
                                 onLoad={() => {
                                     this.setState({isloadingLoader: true})
                                 }}
                                 onLoadStart={() => {
                                     this.setState({isloadingLoader: true})
                                 }}
                                 onLoadEnd={() => {
                                     this.setState({isloadingLoader: false})
                                 }}
                        />
                        {this.state.isloadingLoader && <Loader/>}

                    </View>
                    }

                    {this.state.isImage &&
                    <View style={{flex: 1}}>
                        <WebView style={{flex: 1}}
                                 source={{
                                     html: '<!DOCTYPE html>\n' +
                                         '<html>\n' +
                                         '<head>' +
                                         '<meta name="viewport" content="initial-scale=.5, maximum-scale=4.0, minimum-scale=.25, user-scalable=yes"/>' +
                                         '</head>' +
                                         '<body>\n' +
                                         '\n' +
                                         '<img  style="width: auto ;\n' +
                                         '  max-width: 100% ;\n' +
                                         '  margin: 0 auto;\n' +
                                         '  height: auto ; "  src="' + this.state.file_path + '"  >\n' +
                                         '\n' +
                                         '</body>\n' +
                                         '</html>'
                                 }}
                                    // scalesPageToFit={true}
                                 onLoad={() => {
                                     this.setState({isloadingLoader: true})
                                 }}
                                 onLoadStart={() => {
                                     this.setState({isloadingLoader: true})
                                 }}
                                 onLoadEnd={() => {
                                     this.setState({isloadingLoader: false})
                                 }}
                        />
                        {this.state.isloadingLoader && <Loader/>}
                    </View>
                    }
                </View>
            </View>
        )

    }
}

const styles = StyleSheet.create({
    container: {flex: 1},
    backgroundImage: {flex: 1, position: 'absolute', resizeMode: 'repeat'},
    mainContainer:{flex: 1, marginBottom: 60,},
})

export const Loader = () => (
    <View style={{
        justifyContent: 'center', alignItems: 'center', position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
    }}>
        <Image source={require('../Asset/bg2.png')}
               style={{flex: 1, position: 'absolute', resizeMode: 'repeat'}}/>
        <ActivityIndicator size="large" color={Colors.primary_color}/>
    </View>
)

