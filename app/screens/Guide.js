import React from "react";
import {
    View,
    Text,
    Button,
    StyleSheet,
    TextInput,
    Image,
    ScrollView,
    Alert,
    WebView, Platform,
    RefreshControl, NetInfo, Linking, TouchableOpacity
} from "react-native";

import Colors from "../Styles/Colors";
import dimen from "../Styles/Dimen";
import AppText from "../Components/AppText";
import {FlatList} from "react-native-gesture-handler";
import {Loader} from "../Components/Loader";
import {GuideCard} from "../Components/GuideCard";
import {HomeHeaderQiblaMain, AllTripsHeaderButton, LogoTitle} from "../Components/HeaderComponents";
import {errorAlert, getData, getSavedObject, getUserObject, isNetConnected, saveMainObject} from "../Classes/auth";
import {
    base_guides_url,
    base_profile_picture_url,
    guide_key,
    internetError,
    single_trip_url,
    trip_key,
    update_user_url
} from "../Classes/UrlConstant";
import * as WebBrowser from 'expo-web-browser';
import * as FileSystem from 'expo-file-system';
import * as Sharing from 'expo-sharing';
import * as IntentLauncher from 'expo-intent-launcher';
import {isLoading} from "expo-font";
import PDFReader from 'rn-pdf-reader-js'


export class Guide extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isSplash: true,
            tripsteps: [],
            tripdata: {},
            userdata: {},
            trip_guide: [],
            isqibla: false,
            isLoading: false,
            isLoadingLoader: false
        };

    }

    static navigationOptions = ({navigation}) => {
        return {
            headerLeftContainerStyle: {},
            // headerTitle: <LogoTitle/>,

            headerStyle: {
                backgroundColor: Colors.primary_color,
            },
            headerTitleStyle: {
                justifyContent: 'center',
                alignItems: 'center',
            },
            headerTitleContainerStyle: {
                left: 120,
                right: 120,
                justifyContent: 'center',
                alignItems: 'center'
            },

            headerRight: (navigation.state.isqibla ?
                    <HomeHeaderQiblaMain onPressItem={() => navigation.navigate('UserStack')}/> : null
            ),
            //hide backtitle and show custom button
            headerBackTitleVisible: false,

            headerLeft: (
                <AllTripsHeaderButton onPressItem={() => navigation.navigate('AllTripsStack')}/>
            ),
        }


    };

    componentWillMount() {
        this.loadTripData()
    }

    loadTripData() {
        getSavedObject(trip_key)
            .then((data) => {
                this.updateState(data)
            }).catch(() => {
        })

        getUserObject().then((user) => {
            this.setState({
                userdata: JSON.parse(user),
            })
        })
    }

    updateState = (data) => {
        var jsondata = JSON.parse(data);
        this.setState({
            tripdata: jsondata,
            trip_guide: jsondata.trip.trip_guide != null ? jsondata.trip.trip_guide : []
        }, function () {

        })
    }
    refreshTripData = () => {

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected) {
                this.setState({isLoading: true})
                getData(single_trip_url + this.state.tripdata.trip_id, 'GET', '')
                    .then((res) => {
                        this.setState({isLoading: false})
                        this.updateState(JSON.stringify(res))
                        saveMainObject(trip_key, JSON.stringify(res))
                            .then((res) => {
                            })
                            .catch((res) => {
                                // console.log("error saving data: " + err)
                            })
                    })
                    .catch(err => {
                        this.setState({isLoading: false})
                        // console.log("error fetching data : " + err)
                    });
            } else {
                errorAlert(internetError)
            }
        })
    }

    guideItemPressedAndroid(item) {
        let base_guide_url = 'http://ahlanguide.com/storage/guides/';

        // this.props.navigation.navigate('GuideData', {'guidedata': item})

        // console.log(item)

        let title = item.title === null ? '' : item.title;
        let isPath = item.path !== null
        let isYoutubeLink = item.youtube_link !== null
        let fileTypeName = 'link';
        if (isPath) {
            let filetype = item.path.split('.');
            if (filetype.length > 0) {
                fileTypeName = filetype[filetype.length - 1];
            }
        }


        if (!isPath) {
            this.guideYoutubePressed(item)
            return;
        }

        getSavedObject(item.path).then((res) => {
            if (!res) {
                //download if not available
                this.setState({isLoading: true})
                isNetConnected().then(() => {
                    FileSystem.downloadAsync(
                        base_guide_url + item.path,
                        FileSystem.documentDirectory + item.path
                    )
                        .then(({uri}) => {
                            this.setState({isLoading: false})
                            // console.log('Finished downloading to ', uri);
                            saveMainObject(item.path, uri)
                                .then().catch()
                            this.openUri(uri)
                        })
                        .catch(error => {
                            this.setState({isLoading: false})
                            console.error(error);
                            errorAlert('Error downloading. Try again.')
                        });

                }).catch(() => {
                    errorAlert(internetError)
                })
            } else {
                this.openUri(res)
            }
        }).catch((err) => {
            console.log(err);
        })

    }

    guideItemPressed(item) {
        if (Platform.OS == 'ios' ) {
            this.props.navigation.navigate('GuideData', {'guidedata': item})
            return;
        }

        let base_guide_url = 'http://ahlanguide.com/storage/guides/';

        let title = item.title === null ? '' : item.title;
        let isPath = item.path !== null
        let isYoutubeLink = item.youtube_link !== null
        let fileTypeName = 'link';
        if (isPath) {
            let filetype = item.path.split('.');
            if (filetype.length > 0) {
                fileTypeName = filetype[filetype.length - 1];
            }
            console.log(fileTypeName)

            if (fileTypeName === 'png' || fileTypeName === 'jpg' ||fileTypeName === 'jpeg'  ) {
                this.props.navigation.navigate('GuideData', {'guidedata': item})
                return;
            }

        }
        if (!isPath) {
            this.guideYoutubePressed(item)
            return;
        }

        getSavedObject(item.path).then((res) => {
            if (!res) {
                //download if not available
                this.setState({isLoading: true})
                isNetConnected().then(() => {
                    FileSystem.downloadAsync(
                        base_guide_url + item.path,
                        FileSystem.documentDirectory + item.path
                    )
                        .then(({uri}) => {
                            this.setState({isLoading: false})
                            console.log('Finished downloading to ', uri);
                            saveMainObject(item.path, uri)
                                .then().catch()
                            this.openUri(uri)
                        })
                        .catch(error => {
                            this.setState({isLoading: false})
                            errorAlert('Error downloading. Try again.')
                            console.error(error);
                        });

                }).catch(() => {
                    errorAlert(internetError)
                })
            } else {
                this.openUri(res)
                console.log(res)
            }
        }).catch((err) => {
            console.log(err);
        })

    }

    getFileName = (item) => {
        var file_array = item.path.split('.')
        var file_extenssion = file_array[file_array.length - 1];
        var file_name = item.title + '_' + item.id + '.' + file_extenssion;
        return file_name;
    }

    openUri(res) {
        FileSystem.getContentUriAsync(res).then(cUri => {
            console.log(cUri);
            IntentLauncher.startActivityAsync('android.intent.action.VIEW', {
                data: cUri.uri,
                flags: 1,
            }).then((res) => console.log(res))
                .catch((err) => console.log(err))
            ;
        });

    }

    guideItemDownloadPressed(item) {
        isNetConnected().then(() => {
            Linking.openURL('file:///data/user/0/host.exp.exponent/files/ExperienceData/%2540anonymous%252Fmyapp-8e396730-e583-429f-b617-ca2194b170c4/1582628162_sample.pdf')

        }).catch(() => {
            errorAlert(internetError)
        })

    }

    guideYoutubePressed(item) {
        isNetConnected().then(() => {
            item.path = null;
            this.props.navigation.navigate('GuideData', {'guidedata': item})
        }).catch(() => {
            errorAlert(internetError)
        })

    }

    guideItemOpenPressed(item) {

        // Sharing.shareAsync(uri, {'mimeType': 'application/pdf'})
        getSavedObject(this.getFileName(item))
            .then((res) => {
                // console.log('get uri: '+ res);

                // FileSystem.getContentUriAsync(uri)
                //     .then(cUri => {
                //     console.log( 'new Uri: '+(cUri).uri);
                //
                //     // this.props.navigation.navigate('GuideData', {'guidedata': {'path': JSON.parse(cUri).uri}})
                //     IntentLauncher.startActivityAsync(IntentLauncher.ACTION_LOCATION_SOURCE_SETTINGS);
                //     IntentLauncher.startActivityAsync('android.intent.action.VIEW', {
                //         data: (cUri).uri,
                //         flags: 1
                //     })
                //         .then((res) => {
                //             console.log("result" + res.toString())
                //         })
                //         .catch((err) => console.log("error: " + err))
                //
                // }).catch((err) => {
                //      console.error("error when opening: " + err);
                // });

                // Linking.openURL(res)
                //     .then((res)=>console.log(res))
                //     .catch((res)=>console.log(res))

                // Sharing.shareAsync(res)

                this.guideItemPressed(res)

            })
            .catch(() => {
            })

    }

    guideItemOpenPressed(item) {
        isNetConnected().then(() => {
            Linking.openURL(item.youtube_link).then().catch((err) => {
                // console.log(err)
                errorAlert('Unable to open.');
            })
        }).catch(() => {
            errorAlert(internetError)
        })

    }

    _handleOpenWithWebBrowser = (uri) => {
        WebBrowser.openBrowserAsync(uri);
    }
    updateguidedata = async (guidedata) => {
        var guided = guidedata;
        for (var i = 0; i < guidedata.length; i++) {
            await getSavedObject(this.getFileName(guidedata[i]))
                .then((res) => {
                    if (!res) {
                        guided[i].is_saved = false;
                        guided[i].saved_path = '';
                    } else {
                        guided[i].is_saved = true;
                        guided[i].saved_path = res;
                    }
                })
                .catch(() => {
                })
        }
        return guided
    }

    render() {

        return (
            <View style={{flex: 1}}>
                <Image source={require('../Asset/bg.png')}
                       style={styles.backgroundImage}/>

                <View style={styles.container}>
                    <View>
                        <AppText style={styles.titleText}> Guides</AppText>
                    </View>
                    <View style={styles.guideContainer}>
                        <View style={{flex: 1}}>
                            {this.state.trip_guide.length > 0 &&
                            <FlatList
                                bounces={true}
                                refreshControl={
                                    <RefreshControl
                                        refreshing={this.state.isLoading}
                                        onRefresh={this.refreshTripData}
                                    />
                                }
                                data={this.state.trip_guide}
                                keyExtractor={(item, index) => item.id.toString()}
                                numColumns={1}
                                renderItem={({item}) => (
                                    <GuideCard
                                        onPressItem={this.guideItemPressed.bind(this)}
                                        onPressItemDownload={this.guideItemDownloadPressed.bind(this)}
                                        onPressYoutube={this.guideYoutubePressed.bind(this)}
                                        item={item}/>
                                )}
                            />
                            }
                            {this.state.trip_guide.length == 0 &&
                            <View style={styles.noDatacontainer}>
                                <AppText style={styles.noDataText}>
                                    No guides available.
                                </AppText>
                                <TouchableOpacity onPress={this.refreshTripData}>
                                    <Image source={require('../Asset/reload.png')} tintColor={Colors.primary_color}
                                           style={{width: 30, height: 30, resizeMode: 'contain'}}/>
                                </TouchableOpacity>
                            </View>
                            }
                        </View>
                    </View>

                </View>
                {this.state.isLoadingLoader && <Loader/>}
            </View>
        )

    }
}

const styles = StyleSheet.create({
    backgroundImage: {
        bottom: 0,
        left: 0,
        right: 0,
        top: 0,
        width: '100%',
        height: '100%',
        position: 'absolute',
        resizeMode: 'cover'
    },
    container: {flex: 1, margin: dimen.app_padding},
    titleText: {fontFamily: 'roboto-bold', fontSize: dimen.textinputfontsize,},
    guideContainer: {
        flex: 1, marginBottom: dimen.bottom_margin_for_bottom_menu, marginTop: dimen.app_padding,
        borderRadius: dimen.border_radius, overflow: 'hidden', borderColor: Colors.border_color,
        borderWidth: 1, backgroundColor: Colors.home_widget_background_color
    },
    noDatacontainer: {flex: 1, justifyContent: 'center', alignItems: 'center'},
    noDataText: {fontSize: 18},

})

