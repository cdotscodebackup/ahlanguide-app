import React from 'react';
import {
    ActivityIndicator, Alert,
    Image,
    StatusBar,
    StyleSheet,
    View,
} from 'react-native';
import {
    getSavedObject,
    getUserObject,
    isSignedIn,
} from "../Classes/auth";
import {trip_key} from "../Classes/UrlConstant";


export default class AuthLoadingScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            signedIn: false,
            checkedSignIn: false,
            isonetrip: false
        };
    }

    componentDidMount() {
        isSignedIn().then((res) => {
            if (res) {
                getUserObject().then((user) => {
                    if (JSON.parse(user).user_type == 'agent' || JSON.parse(user).user_type == 'staff') {
                        this.props.navigation.navigate('AdminStack')
                    } else {
                        getSavedObject(trip_key).then((res) => {
                            let current_trip = JSON.parse(res);

                            if (current_trip.trip !== undefined) {
                                var end_date = new Date(current_trip.trip.travel_agency.package_end_date);
                                var date = new Date();
                                if (date < end_date) {
                                    if (current_trip.status !== 'ongoing') {
                                        this.props.navigation.navigate('AllTripsStack')
                                    } else {
                                        var tripEndDate = new Date(current_trip.trip.end_date);
                                        if (date > tripEndDate) {
                                            this.props.navigation.navigate('AllTripsStack')
                                        } else {

                                            if (current_trip.trip.app_menu === 0) {
                                                this.props.navigation.navigate('AppHome');
                                            } else if (current_trip.trip.app_menu === 1) {
                                                this.props.navigation.navigate('CurrentTripsStack');
                                            }
                                            // this.props.navigation.navigate('AppHome')
                                        }
                                    }
                                } else {
                                    this.props.navigation.navigate('AllTripsStack')
                                }
                            } else {
                                this.props.navigation.navigate('AllTripsStack')
                            }
                        })
                            .catch((err) => {
                                // console.error(err)
                            })
                    }
                })
            } else {
                this.props.navigation.navigate('Auth')
            }
        })
            .catch((err) => {
            })


    }

    render() {
        return (
            <View style={styles.mainContainer}>
                <Image source={require('../Asset/bg2.png')}
                       style={styles.backgroundImage}/>
                <ActivityIndicator/>
                <StatusBar barStyle="default"/>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    mainContainer: {flex: 1, alignItems: 'center', justifyContent: 'center',},
    backgroundImage: {flex: 1, position: 'absolute', resizeMode: 'repeat'},

})
