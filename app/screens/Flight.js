import React from "react";
import {
    View,
    Text,
    Button,
    StyleSheet,
    ScrollView,
    Alert,
    TouchableOpacity,
    Image,
    CheckBox,
    RefreshControl, NetInfo
} from "react-native";
import {FlatList} from "react-native-gesture-handler";
import Colors from "../Styles/Colors";
import dimen from "../Styles/Dimen";
import AppText from "../Components/AppText";
import {HotelCard} from "../Components/HotelCard";
import {Loader} from "../Components/Loader";
import {FlightCard} from "../Components/FlightCard";
import {
    HomeHeaderQiblaMain,
    AllTripsHeaderButton,
    LogoTitle
} from "../Components/HeaderComponents";
import {errorAlert, getData, getSavedObject, getUserObject, isNetConnected, saveMainObject} from "../Classes/auth";
import {internetError, single_trip_url, trip_key} from "../Classes/UrlConstant";
import {TripStepCard} from "../Components/TripStepCard";

export class Flight extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isSplash: true,
            tripsteps: [],
            tripdata: {},
            userdata: {},
            trip_flights: [],
            isqibla: false,
            isLoading: false

        };
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerLeftContainerStyle: {},
            // headerTitle: <LogoTitle/>,

            headerStyle: {
                backgroundColor: Colors.primary_color,
            },
            headerTitleStyle: {
                justifyContent: 'center',
                alignItems: 'center',
            },
            headerTitleContainerStyle: {
                left: 120,
                right: 120,
                justifyContent: 'center',
                alignItems: 'center'
            },

            headerRight: (navigation.state.isqibla ?
                    <HomeHeaderQiblaMain onPressItem={() => navigation.navigate('UserStack')}/> : null
            ),
            //hide backtitle and show custom button
            headerBackTitleVisible: false,

            headerLeft: (
                <AllTripsHeaderButton onPressItem={() => navigation.navigate('AllTripsStack')}/>
            ),
        }
    };

    componentDidMount() {
        this.props.navigation.setParams({
            isqibla: false,
            refreshTripData: this.refreshTripData
        });

        this.loadTripData()
    }

    loadTripData() {
        getSavedObject(trip_key)
            .then((data) => {
                this.updateState(data)
            }).catch(() => {
        })

        getUserObject().then((user) => {
            this.setState({
                userdata: JSON.parse(user),
            })
        })
    }

    updateState = (data) => {
        // console.log(JSON.parse(data).trip_flights)
        this.setState({
            tripdata: JSON.parse(data),
            trip_flights: JSON.parse(data).trip_flights != null ? JSON.parse(data).trip_flights : []
        }, function () {
        })
    }
    refreshTripData = () => {
        isNetConnected().then(() => {
            this.setState({isLoading: true})
            getData(single_trip_url + this.state.tripdata.trip_id, 'GET', '')
                .then((res) => {
                    this.setState({isLoading: false})
                    this.updateState(JSON.stringify(res))
                    saveMainObject(trip_key, JSON.stringify(res))
                        .then((res) => {
                        })
                        .catch((res) => {
                            // console.log("error saving data: " + err)
                        })
                })
                .catch(err => {
                    this.setState({isLoading: false})
                    // console.log("error fetching data : " + err)
                });
        }).catch(() => {
            errorAlert(internetError)
        })
    }

    render() {
        return (
            <View style={styles.mainContainer}>
                <Image source={require('../Asset/bg.png')}
                       style={styles.backgroundImage}/>
                <View style={styles.titleContainer}>
                    <AppText style={styles.titleText}>
                        Flight Details
                    </AppText>
                </View>
                <View style={{flex: 1,}}>
                    {this.state.trip_flights.length > 0 &&
                    <FlatList
                        bounces={true}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.isLoading}
                                onRefresh={this.refreshTripData}
                            />
                        }
                        data={this.state.trip_flights}
                        keyExtractor={(item, index) => item.id.toString()}
                        numColumns={1}
                        renderItem={({item}) => (
                            <FlightCard
                                // onPressItem={this.home_product_pressed.bind(this)}
                                item={item}/>)}
                    />
                    }
                    {this.state.trip_flights.length == 0 &&
                    <View style={styles.noDataContainer}>
                        <AppText style={styles.noDataText}>
                            No flight has been assigned to yet. Please contact your agent.
                        </AppText>
                        <TouchableOpacity onPress={this.refreshTripData}>
                            <Image source={require('../Asset/reload.png')} tintColor={Colors.primary_color}
                                   style={{width: 30, height: 30, resizeMode: 'contain'}}/>
                        </TouchableOpacity>
                    </View>
                    }
                </View>
                {this.state.isLoading_latest_products && <Loader/>}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainContainer: {flex: 1, marginBottom: dimen.bottom_margin_for_bottom_menu},
    backgroundImage: {
        bottom: 0,
        left: 0,
        right: 0,
        top: 0,
        width: '100%',
        height: '100%',
        position: 'absolute',
        resizeMode: 'cover'
    },
    titleContainer: {
        marginTop: dimen.app_padding,
        marginLeft: dimen.app_padding,
        marginRight: dimen.app_padding,
        marginBottom: dimen.app_padding
    },
    titleText: {fontFamily: 'roboto-bold', fontSize: dimen.textinputfontsize},
    noDataContainer: {flex: 1, justifyContent: 'center', alignItems: 'center'},
    noDataText: {fontSize: 18, textAlign: 'center', margin: dimen.app_padding * 2, marginBottom: dimen.app_padding},


})
