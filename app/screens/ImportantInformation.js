import React from "react";
import {
    View,
    Text,
    Button,
    StyleSheet,
    ScrollView,
    Alert,
    TouchableOpacity,
    Image,
    CheckBox,
    Platform
} from "react-native";
import Colors from "../Styles/Colors";
import dimen from "../Styles/Dimen";
import AppText from "../Components/AppText";
import BackHeaderButton from "../Components/HeaderComponents";
import {getSavedObject, strip_html_tags} from "../Classes/auth";
import {WebView} from 'react-native-webview';


export class ImportantInformation extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isSplash: true,
            tripsteps: [],
            hideBottomTab: false,
        };
    }

    static navigationOptions = ({navigation}) => {
        return {
            title: 'Important Information',
            headerStyle: {
                backgroundColor: Colors.primary_color,
                tintColor: Colors.normal_text_color,
            },

            headerLayoutPreset: 'left',
            headerTitleStyle: {
                justifyContent: 'flex-start',
                fontWeight: 'bold',
                marginLeft: 3
            },
            headerTitleContainerStyle: {
                left: 45,
            },
            headerLeftContainerStyle: {},
            tabBarVisible: false,
            //hide backtitle and show custom button
            headerBackTitleVisible: false,
            headerLeft: (
                <BackHeaderButton onPressItem={() => navigation.goBack()}/>
            ),
        }
    };

    componentDidMount() {
        getSavedObject('hideBottomTab')
            .then(res => {
                this.setState({hideBottomTab: JSON.parse(res)})
            })
            .catch(err => console.log(err))
    }

    render() {
        let importantInfoIos = strip_html_tags(this.props.navigation.getParam('importantInfo', '').toString());
        let importantInfo = (this.props.navigation.getParam('importantInfo', '').toString());
        console.log(importantInfo);
        if (importantInfo == '')
            importantInfo = 'No Important Information for this Section'

        return (
            <View style={styles.mainContainer}>
                <Image source={require('../Asset/bg.png')} style={styles.backgroundImage}/>
                {Platform.OS === 'android' &&
                <WebView style={[styles.webviewAndroidStyle, this.state.hideBottomTab ? {marginBottom: 10} : {}]}
                    // source={{html: importantInfo}}
                         source={{
                             html: '<!DOCTYPE html>\n' +
                                 '<html>\n' +
                                 '<head>' +
                                 '<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=yes"/>' +
                                 '</head>' +
                                 '<body>\n' +
                                 '\n' +
                                 importantInfo +
                                 '\n' +
                                 '</body>\n' +
                                 '</html>'
                         }}
                         scalesPageToFit={true}
                    // onLoad={() => {
                    //     this.setState({isloadingLoader: true})
                    // }}
                    // onLoadStart={() => {
                    //     this.setState({isloadingLoader: true})
                    // }}
                    // onLoadEnd={() => {
                    //     this.setState({isloadingLoader: false})
                    // }}
                />
                }

                {Platform.OS === 'ios' &&
                <WebView style={[styles.webviewAndroidStyle, this.state.hideBottomTab ? {marginBottom: 10} : {}]}
                    // source={{html: importantInfo}}
                         source={{
                             html: '<!DOCTYPE html>\n' +
                                 '<html>\n' +
                                 '<head>' +
                                 '<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=yes"/>' +
                                 '</head>' +
                                 '<body>\n' +
                                 '\n' +
                                 importantInfo +
                                 '\n' +
                                 '</body>\n' +
                                 '</html>'
                         }}
                    // scalesPageToFit={true}
                />
                }
            </View>

        )
    }
}

const styles = StyleSheet.create({
    mainContainer: {flex: 1, justifyContent: "flex-start", alignItems: "stretch"},
    backgroundImage: {
        bottom: 0,
        left: 0,
        right: 0,
        top: 0,
        width: '100%',
        height: '100%',
        position: 'absolute',
        resizeMode: 'cover'
    },
    scrollView: {
        flex: 1,
        paddingTop: dimen.app_padding,
        paddingLeft: dimen.app_padding,
        paddingRight: dimen.app_padding,
        marginBottom: 60
    },
    scrollViewInsideContainer: {
        padding: dimen.app_padding,
        marginBottom: 30,
        borderWidth: 1,
        borderRadius: 10,
        borderColor: Colors.border_color,
        backgroundColor: Colors.home_widget_background_color,
        margin: dimen.app_padding,

    },
    webviewAndroidStyle: {
        flex: 1,
        borderRadius: 10,
        margin: dimen.app_padding,
        marginBottom: dimen.bottom_tab_height + 10

    },


})
