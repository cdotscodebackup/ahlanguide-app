import React, {Component} from 'react';
import {
    View, Text, Image, Alert, StyleSheet, TouchableOpacity, TouchableHighlight,KeyboardAvoidingView
}from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import TextStyles from "../Styles/TextStyles";
import {errorAlert, getDataWithoutToken, simpleAlert,} from "../Classes/auth";
import {Loader} from "../Components/Loader";
import dimen from "../Styles/Dimen";
import {forget_password_check_email_url, forget_password_url} from "../Classes/UrlConstant";

export class ForgotPassword extends React.Component {
    constructor(props) {
        super(props);
        this.state =
            {
                email: '',
                password: '',
                isLoading: false,
                hidesomelayout: false,
            };
    }

    goback = () => {
        this.props.navigation.goBack();
    }
    checkEmail = () => {
        if (this.state.email == '') {
            simpleAlert('Enter your email to reset password.')
            return;
        }
        this.setState({isLoading: true})
        let formdata = new FormData();
        formdata.append("email", this.state.email)

        getDataWithoutToken(forget_password_check_email_url, 'POST', formdata)
            .then(res => {
                this.setState({isLoading: false,})
                console.log((res))
                if (res===true) {
                    this.forgetPassword()
                    // simpleAlert('Email has been sent to reset password.');
                } else {
                    var m = res.errors;
                    var keys = [];
                    for (var k in m) keys.push(k);

                    var mforuser = 'Email does not exist.';
                    // console.log('key 0 ' + (m[keys[0]]))

                    for (var k in keys)
                        mforuser =  m[keys[k]] + '\n'

                    errorAlert(mforuser);
                }
            })
            .catch(err => {
                this.setState({isLoading: false,})
            })

    }
    forgetPassword = () => {
        if (this.state.email == '') {
            simpleAlert('Enter your email to reset password.')
            return;
        }
        this.setState({isLoading: true})
        let formdata = new FormData();
        formdata.append("email", this.state.email)

        getDataWithoutToken(forget_password_url, 'POST', formdata)
            .then(res => {
                this.setState({isLoading: false,})
                console.log(JSON.stringify(res))
                if (res.status) {
                    simpleAlert('Email has been sent to reset password.');
                } else {
                    simpleAlert('Email not found in system.');
                }
            })
            .catch(err => {
                this.setState({isLoading: false,})
            })

    }

    render() {
        return (
            <View style={{flex: 1,}}>
                <Image source={require('../Asset/main_bg2.png')}
                       style={{flex: 1, position: 'absolute', resizeMode: 'repeat'}}/>
                <KeyboardAvoidingView style={{flex: 1}} behavior="padding" enabled={true}>
                    <View style={{
                        flex: 1,
                        padding: dimen.app_padding,
                        flexDirection: "column",
                        justifyContent: 'space-evenly',
                        alignItems: 'stretch'
                    }}>
                        <View style={{flex: 1, justifyContent: 'center', alignItems: 'stretch'}}>
                            <Image source={require('../Asset/login_logo.png')}
                                   style={[{width: 200, height: 106, alignSelf: 'center', resizeMode: "contain",}]}/>
                            <View style={{marginTop: dimen.app_padding * 2}}>
                                <Text style={{alignSelf: "center"}}>Enter your email to reset your password</Text>
                            </View>
                        </View>
                        <View style={{flex: 1, justifyContent: 'space-between', alignItems: 'stretch'}}>
                            <View style={{marginTop: dimen.app_padding}}>
                                <TextInput onChangeText={(email) => this.setState({email})}
                                           value={this.state.email}
                                           placeholder="email"
                                           style={[TextStyles.inputstyle]}/>
                            </View>
                            <View>
                                <TouchableHighlight style={[{marginBottom: dimen.app_padding}, TextStyles.buttonStyle]}
                                                    onPress={this.checkEmail}
                                >
                                    <Text style={[TextStyles.buttonTextStyle]}>Reset</Text>
                                </TouchableHighlight>
                            </View>
                        </View>
                    </View>
                    {this.state.isLoading && <Loader/>}
                </KeyboardAvoidingView>
                <TouchableOpacity
                    style={{top: 40, left: 20, width: 40, height: 40, justifyContent: 'center', position: 'absolute'}}
                    onPress={this.goback}>
                    <Image source={require('../Asset/back_arrow.png')}
                           style={{margin: 20, width: 20, height: 20, alignSelf: 'center', resizeMode: 'contain'}}/>
                </TouchableOpacity>
            </View>
        )
    }
}


