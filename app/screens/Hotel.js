import React from "react";
import {
    View, Text, Button, StyleSheet, Alert,
    ScrollView, Image, TextInput, ActivityIndicator, TouchableOpacity, TouchableWithoutFeedback,
    NetInfo, RefreshControl,
} from "react-native";

import {Header} from '../Components/CustomHeader';
import BackHeaderButton, {
    HomeHeaderQiblaMain,
    AllTripsHeaderButton,
    LogoTitle
} from '../Components/HeaderComponents';
import {FlatList} from "react-native-gesture-handler";
import Colors from "../Styles/Colors";
import {
    errorAlert,
    getData,
    getSavedObject,
    getUserObject,
    isNetConnected,
    saveMainObject,
    simpleAlert
} from "../Classes/auth";
import {Loader} from "../Components/Loader";
import dimen from "../Styles/Dimen";
import AppText from "../Components/AppText";
import {HotelCard} from "../Components/HotelCard";
import {internetError, single_trip_url, trip_key} from "../Classes/UrlConstant";
import {Linking} from 'react-native';
import {FlightCard} from "../Components/FlightCard";


export class Hotel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tripsteps: [],
            tripdata: {},
            userdata: {},
            trip_hotels: [],
            isLoading: false

        };
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerLeftContainerStyle: {},
            // headerTitle: <LogoTitle/>,

            headerStyle: {
                backgroundColor: Colors.primary_color,
            },
            headerTitleStyle: {
                justifyContent: 'center',
                alignItems: 'center',
            },
            headerTitleContainerStyle: {
                left: 120,
                right: 120,
                justifyContent: 'center',
                alignItems: 'center'
            },
            headerBackTitleVisible: false,
            headerLeft: (
                <AllTripsHeaderButton onPressItem={() => navigation.navigate('AllTripsStack')}/>
            ),
        }


    };

    componentDidMount() {
        this.loadTripData()
    }

    loadTripData() {
        getSavedObject(trip_key)
            .then((data) => {
                this.updateState(data)

            }).catch(() => {
        })

        getUserObject().then((user) => {
            this.setState({
                userdata: JSON.parse(user),
            })
        })
    }

    updateState = (data) => {
        // console.log("data update");

        this.setState({
            tripdata: JSON.parse(data),
            trip_hotels: JSON.parse(data).trip_hotels != null ? JSON.parse(data).trip_hotels : []

        }, function () {
            // console.log("data update" + this.state.trip_hotels.length);
        })
    }
    refreshTripData = () => {
        isNetConnected().then(() => {
            this.setState({isLoading: true})
            getData(single_trip_url + this.state.tripdata.trip_id, 'GET', '')
                .then((res) => {
                    this.setState({isLoading: false})
                    this.updateState(JSON.stringify(res))
                    saveMainObject(trip_key, JSON.stringify(res)).then((res) => {
                    }).catch((res) => {
                    })
                })
                .catch(err => {
                    this.setState({isLoading: false})
                });
        }).catch(() => {
            errorAlert(internetError)
        })
    }

    linkPressed(item) {
        if (item.triphoteldetail.hotel.map_url !== null) {
            Linking.openURL(item.triphoteldetail.hotel.map_url);
        } else {
            simpleAlert("No Location found.");
        }
    }

    render() {
        return (
            <View style={styles.mainContainer}>
                <Image source={require('../Asset/bg.png')}
                       style={styles.backgroundImage}/>
                <View style={styles.titleContainer}>
                    <AppText style={styles.titleText}>
                        Hotel
                    </AppText>
                </View>

                <View style={{flex: 1,}}>
                    {this.state.trip_hotels.length > 0 &&
                    <FlatList
                        bounces={true}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.isLoading}
                                onRefresh={this.refreshTripData}
                            />
                        }
                        data={this.state.trip_hotels}
                        keyExtractor={(item, index) => item.id.toString()}
                        numColumns={1}
                        renderItem={({item}) => (
                            <HotelCard
                                onPressItem={this.linkPressed.bind(this)}
                                imagePress={(path)=>this.props.navigation.navigate('ImageScreen',{path:path})}
                                item={item}/>)}
                    />
                    }
                    {this.state.trip_hotels.length == 0 &&
                    <View style={styles.emptyListContainer}>
                        <AppText style={styles.emptyListText}>
                            No hotel has been assigned to yet. Please
                            contact your agent.
                        </AppText>
                        <TouchableOpacity onPress={this.refreshTripData}>
                            <Image source={require('../Asset/reload.png')} tintColor={Colors.primary_color}
                                   style={{width: 30, height: 30, resizeMode: 'contain'}}/>
                        </TouchableOpacity>

                    </View>
                    }

                </View>
                {/*{this.state.isLoading && <Loader/>}*/}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainContainer: {flex: 1, marginBottom: dimen.bottom_margin_for_bottom_menu},
    backgroundImage: {
        bottom: 0,
        left: 0,
        right: 0,
        top: 0,
        width: '100%',
        height: '100%',
        position: 'absolute',
        resizeMode: 'cover'
    },
    titleContainer: {
        marginTop: dimen.app_padding,
        marginLeft: dimen.app_padding,
        marginRight: dimen.app_padding,
        marginBottom: dimen.app_padding
    },
    titleText: {fontFamily: 'roboto-bold', fontSize: dimen.textinputfontsize},
    emptyListContainer: {flex: 1, justifyContent: 'center', alignItems: 'center'},
    emptyListText: {fontSize: 18, margin: dimen.app_padding * 2, textAlign: 'center', marginBottom: dimen.app_padding}


})
